import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import {
  clearRender,
  click,
  fillIn,
  render,
  select,
  settled,
  tab,
  triggerKeyEvent,
  typeIn,
} from '@ember/test-helpers';
import InputDateComponent from '@getflights/ember-mist-components/components/input/date';
import { tracked } from 'tracked-built-ins';
import { isEqual, isValid, startOfDay } from 'date-fns';
import { setupIntl } from 'ember-intl/test-support';
import { hash } from '@ember/helper';

module('Integration | Component | input/date', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  test('Using the input to manually insert date', async function (assert) {
    const state = tracked({
      value: undefined as Date | undefined,
    });

    const valueChanged = (newValue: Date | undefined) => {
      state.value = newValue;
    };

    await render(<template>
      <InputDateComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        data-test-input
      />
    </template>);

    assert.dom('[data-test-input]').hasValue('', 'Value is empty at first.');
    assert.dom('[data-test-input]').hasAttribute('placeholder', 'YYYY-MM-DD');
    // Using TAB to blur input
    await fillIn('[data-test-input]', '2030-05-01');
    await tab();

    assert
      .dom('[data-test-input]')
      .hasValue('2030-05-01', 'Value is "2030-05-01" after filling in.');
    assert.ok(
      state.value && isEqual(state.value, startOfDay(new Date('2030-05-01'))),
      'Variable has been updated by valueChanged',
    );

    // Using ENTER to blur input
    await fillIn('[data-test-input]', '2030-05-02');
    await triggerKeyEvent('[data-test-input]', 'keydown', 'Enter');

    assert
      .dom('[data-test-input]')
      .hasValue('2030-05-02', 'Value is "2030-05-02" after filling in.');
    assert.ok(
      state.value && isEqual(state.value, startOfDay(new Date('2030-05-02'))),
      'Variable has been updated by valueChanged',
    );

    // Clearing input
    await fillIn('[data-test-input]', '');
    await tab();

    assert
      .dom('[data-test-input]')
      .hasValue('', 'Value is empty after filling in nothing.');
    assert.strictEqual(
      state.value,
      undefined,
      'Variable has been updated to undefined by valueChanged',
    );
  });

  test('Navigating months in the calendar', async function (assert) {
    const undefinedValue = undefined;

    await render(<template>
      <InputDateComponent @value={{undefinedValue}} data-test-input />
    </template>);

    await click('[data-test-input]');

    const thisMonth = new Date().getMonth();

    const monthSelect = document.querySelector(
      `.flatpickr-monthDropdown-months`,
    )!;
    assert
      .dom(`.flatpickr-monthDropdown-month[value="${thisMonth}"]`, monthSelect)
      .matchesSelector(':checked', 'This month is selected by default.');

    const nextMonthSelector = document.querySelector('.flatpickr-next-month')!;
    await click(nextMonthSelector);
    assert
      .dom(
        `.flatpickr-monthDropdown-month[value="${thisMonth + 1}"]`,
        monthSelect,
      )
      .matchesSelector(
        ':checked',
        'Next month is selected after clicking next arrow.',
      );

    const previousMonthSelector = document.querySelector(
      '.flatpickr-prev-month',
    )!;
    await click(previousMonthSelector);
    await click(previousMonthSelector);
    assert
      .dom(
        `.flatpickr-monthDropdown-month[value="${thisMonth - 1}"]`,
        monthSelect,
      )
      .matchesSelector(
        ':checked',
        'Previous month is selected after clicking previous arrow twice.',
      );
  });

  test('Using calendar to select a date', async function (assert) {
    const state = tracked({
      value: undefined as Date | undefined,
    });

    const valueChanged = (newValue: Date | undefined) => {
      state.value = newValue;
    };

    await render(<template>
      <InputDateComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        data-test-input
      />
    </template>);

    assert.dom('[data-test-input]').hasValue('', 'Value is empty at first.');

    await click('[data-test-input]');

    // We have to use document.querySelector because the calendar is rendered outside of the qunit fixture.
    const monthSelect = document.querySelector(
      '.flatpickr-calendar select.flatpickr-monthDropdown-months',
    )!;
    await select(monthSelect, '4');

    const yearInput = document.querySelector(
      '.flatpickr-calendar input.cur-year',
    )!;
    // Just fillIn didnt do the trick for some reason
    await fillIn(yearInput, '');
    await typeIn(yearInput, '2030');

    const flatpickrDays = document.querySelectorAll('.flatpickr-day');
    const desiredDay = Array.from(flatpickrDays).find(
      (day) => day.textContent === '2',
    )!;
    await click(desiredDay);

    assert
      .dom('[data-test-input]')
      .hasValue('2030-05-02', 'Value is "2030-05-02" after filling in.');
    assert.ok(
      state.value && isEqual(state.value, startOfDay(new Date('2030-05-02'))),
      'Variable has been updated by valueChanged',
    );
  });

  test('Invalid date and removing invalid date', async function (assert) {
    const state = tracked({
      // @ts-expect-error string is not a date
      value: 'Not a date' as Date | undefined,
    });

    const valueChanged = (newValue: Date | undefined) => {
      state.value = newValue;
    };

    await render(<template>
      <InputDateComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        data-test-input
      />
    </template>);

    assert.strictEqual(
      state.value,
      // @ts-expect-error string is not a date
      'Not a date',
      'Variable is "Not a date" at first.',
    );
    assert
      .dom('[data-test-input]')
      .hasValue(
        'Invalid date! ("Not a date")',
        'Value is "Invalid date! ("Not a date")" for an invalid date (string).',
      );
    assert
      .dom('[data-test-input]')
      .hasClass('input--invalid', 'Input has invalid class.');

    await click('[data-test-input]');
    assert.strictEqual(
      state.value,
      undefined,
      'Variable has been updated by valueChanged on click.',
    );
    assert.dom('[data-test-input]').hasValue('', 'Value is empty after focus.');
    assert
      .dom('[data-test-input]')
      .doesNotHaveClass(
        '',
        'Value is empty, because it does not render not-a-number values.',
      );

    await clearRender();

    // Invalid date
    state.value = new Date(NaN);

    await render(<template>
      <InputDateComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        data-test-input
      />
    </template>);

    assert.notOk(isValid(state.value), 'Variable is invalid at first.');
    assert
      .dom('[data-test-input]')
      .hasValue(
        'Invalid date!',
        'Value is "Invalid date!" for an invalid date.',
      );
    assert
      .dom('[data-test-input]')
      .hasClass('input--invalid', 'Input has invalid class.');

    await click('[data-test-input]');
    assert.strictEqual(
      state.value,
      undefined,
      'Variable has been updated by valueChanged on click.',
    );
    assert.dom('[data-test-input]').hasValue('', 'Value is empty after focus.');
    assert
      .dom('[data-test-input]')
      .doesNotHaveClass(
        '',
        'Value is empty, because it does not render not-a-number values.',
      );
  });

  test('@required', async function (assert) {
    const state = tracked<{ required?: boolean }>({});

    await render(<template>
      <InputDateComponent
        @value={{undefined}}
        @required={{state.required}}
        data-test-input
      />
    </template>);

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is undefined => not required.');

    state.required = true;
    await settled();

    assert
      .dom('[data-test-input]')
      .isRequired('@required is true => required.');

    state.required = false;
    await settled();

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is false => not required.');
  });

  /**
   * Range
   */

  test('RANGE: Using calendar to select two dates', async function (assert) {
    const state = tracked({
      value: undefined as [Date, Date] | undefined,
    });

    const valueChanged = (newValue: [Date, Date] | undefined) => {
      state.value = newValue;
    };

    await render(<template>
      <InputDateComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        @options={{hash range=true}}
        data-test-input
      />
    </template>);

    assert.dom('[data-test-input]').hasValue('', 'Value is empty at first.');
    assert.dom('[data-test-input]').hasAttribute('placeholder', 'Select dates');

    await click('[data-test-input]');

    // DATE 1: 2 MAY 2030
    // We have to use document.querySelector because the calendar is rendered outside of the qunit fixture.
    const monthSelect = document.querySelector(
      '.flatpickr-calendar select.flatpickr-monthDropdown-months',
    )!;
    await select(monthSelect, '4');

    const yearInput = document.querySelector(
      '.flatpickr-calendar input.cur-year',
    )!;
    // Just fillIn didnt do the trick for some reason
    await fillIn(yearInput, '');
    await typeIn(yearInput, '2030');

    let flatpickrDays = document.querySelectorAll('.flatpickr-day');
    let desiredDay = Array.from(flatpickrDays).find(
      (day) => day.textContent === '2',
    )!;
    await click(desiredDay);

    // DATE 1: 10 NOVEMBER 2031
    await select(monthSelect, '10');

    // Just fillIn didnt do the trick for some reason
    await fillIn(yearInput, '');
    await typeIn(yearInput, '2031');

    flatpickrDays = document.querySelectorAll('.flatpickr-day');
    desiredDay = Array.from(flatpickrDays).find(
      (day) => day.textContent === '10',
    )!;
    await click(desiredDay);

    assert
      .dom('[data-test-input]')
      .hasValue(
        '2030-05-02 \u2192 2031-11-10',
        'Value is "2030-05-02 \u2192 2031-11-10" after filling in.',
      );
    const valuesOk =
      state.value &&
      isEqual(state.value[0]!, startOfDay(new Date('2030-05-02'))) &&
      isEqual(state.value[1]!, startOfDay(new Date('2031-11-10')));
    assert.ok(valuesOk, 'Variable has been updated by valueChanged');
  });

  test('RANGE: Invalid date(s) and removing invalid date(s)', async function (assert) {
    const state = tracked({
      // @ts-expect-error string is not a date array
      value: 'Not a date' as [Date, Date] | undefined,
    });

    const valueChanged = (newValue: [Date, Date] | undefined) => {
      state.value = newValue;
    };

    await render(<template>
      <InputDateComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        @options={{hash range=true}}
        data-test-input
      />
    </template>);

    assert.strictEqual(
      state.value,
      // @ts-expect-error string is not a date array
      'Not a date',
      'Variable is "Not a date" at first.',
    );
    assert
      .dom('[data-test-input]')
      .hasValue(
        'Invalid dates! ("Not a date")',
        'Value is "Invalid date! ("Not a date")" for an invalid date (string).',
      );
    assert
      .dom('[data-test-input]')
      .hasClass('input--invalid', 'Input has invalid class.');

    await click('[data-test-input]');
    assert.strictEqual(
      state.value,
      undefined,
      'Variable has been updated by valueChanged on click.',
    );
    assert.dom('[data-test-input]').hasValue('', 'Value is empty after focus.');
    assert
      .dom('[data-test-input]')
      .doesNotHaveClass(
        '',
        'Value is empty, because it does not render not-a-number values.',
      );

    await clearRender();

    // Invalid date + date
    state.value = [new Date(NaN), new Date()];

    await render(<template>
      <InputDateComponent
        @value={{state.value}}
        @valueChanged={{valueChanged}}
        @options={{hash range=true}}
        data-test-input
      />
    </template>);

    assert.notOk(
      isValid(state.value[0]),
      'First date of variable is invalid at first.',
    );
    assert.ok(
      isValid(state.value[1]),
      'Second date of variable is valid at first.',
    );
    assert
      .dom('[data-test-input]')
      .hasValue(
        'Invalid dates!',
        'Value is "Invalid date!" for an invalid date.',
      );
    assert
      .dom('[data-test-input]')
      .hasClass('input--invalid', 'Input has invalid class.');

    await click('[data-test-input]');
    assert.strictEqual(
      state.value,
      undefined,
      'Variable has been updated by valueChanged on click.',
    );
    assert.dom('[data-test-input]').hasValue('', 'Value is empty after focus.');
    assert
      .dom('[data-test-input]')
      .doesNotHaveClass(
        '',
        'Value is empty, because it does not render not-a-number values.',
      );
  });

  test('RANGE: @required', async function (assert) {
    const state = tracked<{ required?: boolean }>({});

    await render(<template>
      <InputDateComponent
        @value={{undefined}}
        @required={{state.required}}
        @options={{hash range=true}}
        data-test-input
      />
    </template>);

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is undefined => not required.');

    state.required = true;
    await settled();

    assert
      .dom('[data-test-input]')
      .isRequired('@required is true => required.');

    state.required = false;
    await settled();

    assert
      .dom('[data-test-input]')
      .isNotRequired('@required is false => not required.');
  });
});
