import { module, skip, test } from 'qunit';
import { setupRenderingTest } from 'test-app/tests/helpers';
import { setupIntl } from 'ember-intl/test-support';
import { render, settled } from '@ember/test-helpers';
import FloatingComponent from '@getflights/ember-mist-components/components/floating';
import { click } from '@ember/test-helpers';
import userEvent from '@testing-library/user-event';
import { focus } from '@ember/test-helpers';
import { blur } from '@ember/test-helpers';

/**
 * This test is probably over-complicated with the adapter and everything but it works!
 */
module('Integration | Component | Floating', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-001');

  module('MVP', function () {
    test('popover (default)', async function (assert) {
      /* prettier-ignore */
      await render(<template>
        <FloatingComponent as |ref FloatingElement|>
          <button type='button' {{ref}} data-test-ref>
            Click me to open
          </button>
          <FloatingElement>
            I'm floating
          </FloatingElement>
        </FloatingComponent>
        <div data-test-outside>
          I'm an outside div
        </div>
      </template>);

      // No floating element at first
      assert.dom('[data-test-content]').doesNotExist();
      assert
        .dom('[data-test-ref]')
        .hasClass('floating__reference', '"floating__reference" was added');

      /**
       * Clicking opens it
       */
      // Click ref: open
      await click('[data-test-ref]');

      assert
        .dom('[data-test-content]')
        .exists('FloatingElement is rendered after opening it.')
        .hasClass(
          'floating__content',
          'FloatingElement has class "floating__content"',
        );

      /**
       * Clicking again closes it
       */
      // Click ref: close
      await click('[data-test-ref]');

      assert
        .dom('[data-test-content]')
        .doesNotExist(
          'FloatingElement is not rendered after closing it (using the reference button).',
        );

      /**
       * Clicking content has no effect
       */
      // Click ref: open
      await click('[data-test-ref]');
      // Click content
      await click('[data-test-content]');

      assert
        .dom('[data-test-content]')
        .exists(
          "FloatingElement is still rendered after clicking it's content.",
        );

      // Click outside: close
      await click('[data-test-outside]');

      assert
        .dom('[data-test-content]')
        .doesNotExist(
          'FloatingElement is not rendered after closing it (by clicking outside).',
        );
    });

    test('tooltip', async function (assert) {
      /* prettier-ignore */
      await render(<template>
      <FloatingComponent @type='tooltip' as |ref FloatingElement|>
          <button type='button' {{ref}} data-test-ref>
            Click me to open
          </button>
          <FloatingElement>
            I'm floating
          </FloatingElement>
        </FloatingComponent>
        <div data-test-outside>
          I'm an outside div
        </div>
      </template>);

      const user = userEvent.setup();

      // No floating element at first
      assert.dom('[data-test-content]').doesNotExist();
      assert
        .dom('[data-test-ref]')
        .hasClass('floating__reference', '"floating__reference" was added');

      // Hover opens it
      const ref = document.querySelector('[data-test-ref]');
      if (!ref) {
        throw new Error('Could not find [data-test-ref]');
      }

      await user.hover(ref);
      await settled();

      assert
        .dom('[data-test-content]')
        .exists(
          'FloatingElement is rendered when hovering over the reference button.',
        )
        .hasClass(
          'floating__content',
          'FloatingElement has class "floating__content"',
        );

      // "unhover" closes it
      await user.unhover(ref);
      await settled();

      assert
        .dom('[data-test-content]')
        .doesNotExist('FloatingElement is not rendered after stopping hover.');

      // "focus" opens it
      // "pointerenter" opens it
      await focus('[data-test-ref]');

      assert
        .dom('[data-test-content]')
        .exists(
          'FloatingElement is rendered when focussing the reference button.',
        );

      // "blur" closes it
      // "pointerleave" closes it
      await blur('[data-test-ref]');

      assert
        .dom('[data-test-content]')
        .doesNotExist(
          'FloatingElement is not rendered after stopping focus (blur).',
        );
    });
  });

  module('FloatingUiOptions / middleware', function () {
    skip('middleware was manually tested', async function () {
      // todo
    });
  });
});
