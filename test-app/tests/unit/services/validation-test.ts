import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import Model, { attr } from '@ember-data/model';
import type Address from '@getflights/ember-mist-components/interfaces/address';
import Store from '@ember-data/store';
import type { TestContext } from '@ember/test-helpers';
import MistValidationService from '@getflights/ember-mist-components/services/validation';
import { maxLength } from '@getflights/ember-attribute-validations/validations/max-length';
import { number } from '@getflights/ember-attribute-validations/validations/number';
import { address } from '@getflights/ember-mist-components/validations/address';
// import field from '@getflights/ember-field-components/decorators/field';

interface ValidationServiceTestContext extends TestContext {
  store: Store;
  validation: MistValidationService;
}

module('Validation Service', function (hooks) {
  setupTest(hooks);

  hooks.beforeEach(function (this: ValidationServiceTestContext) {
    this.store = this.owner.lookup('service:store') as Store;
    // Register the mist-components validation service
    this.owner.register('service:validation', MistValidationService);
    this.validation = this.owner.lookup(
      'service:validation',
    ) as MistValidationService;
  });

  test('validationsForAttribute', function (this: ValidationServiceTestContext, assert) {
    class TestModel extends Model {
      // Add maxLength(255) validation
      @attr('string')
      declare stringProperty: string;

      // Add number() validation
      @attr('number')
      declare numberProperty: number;

      @attr('address')
      declare addressProperty: Address;
    }

    this.owner.register('model:test', TestModel);

    // Create instance of the model
    const testInstance = this.store.createRecord('test');

    TestModel.eachAttribute((key, attribute) => {
      const validationFunctions = this.validation.validationsForAttribute(
        testInstance,
        // @ts-ignore
        attribute,
      );

      const stringValidationFunctions = validationFunctions.map((func) => {
        return func.toString();
      });

      const maxLength255Validation = maxLength(255).toString();
      const numberValidation = number().toString();
      const addressValidation = address().toString();

      switch (key) {
        case 'stringProperty':
          assert.ok(
            stringValidationFunctions.includes(maxLength255Validation),
            'string type has maxLength(255) validation',
          );
          break;
        case 'numberProperty':
          assert.ok(
            stringValidationFunctions.includes(numberValidation),
            'number type has number validation',
          );
          break;
        case 'addressProperty':
          assert.ok(
            stringValidationFunctions.includes(addressValidation),
            'address type has address validation',
          );
          break;
        default:
          break;
      }
    });

    // const validated = await this.validation.validateModel(testInstance);
  });
});
