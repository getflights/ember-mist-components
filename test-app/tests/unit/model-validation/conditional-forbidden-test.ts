import { attr, belongsTo } from '@ember-data/model';
import Store from '@ember-data/store';
import Service from '@ember/service';
import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import Model from 'ember-data/model';
import type { TestContext } from '@ember/test-helpers';
import ValidationService from '@getflights/ember-attribute-validations/services/validation';
import validation from '@getflights/ember-attribute-validations/decorators/validation';
import { conditionalRequired } from '@getflights/ember-mist-components/validations/conditional-required';
import sinon from 'sinon';

interface RequiredTestContext extends TestContext {
  validation: ValidationService;
  store: Store;
}

module('Model Validation | Conditional Required', function (hooks) {
  setupTest(hooks);

  hooks.beforeEach(function (this: RequiredTestContext) {
    this.validation = this.owner.lookup(
      'service:validation',
    ) as ValidationService;

    this.owner.register(
      'service:intl',
      class IntlMockService extends Service {
        t(key: string, _: { [key: string]: string }): string {
          return key;
        }

        exists(key: string): boolean {
          return key === `ember-attribute-validations.conditionalRequired`;
        }
      },
    );

    this.store = this.owner.lookup('service:store') as Store;
  });

  module('using a field', function (hooks) {
    class DummyModel extends Model {
      @attr('string')
      @validation(conditionalRequired('isRequired'))
      declare name: string;

      get isRequired() {
        return false;
      }

      @attr('string')
      @validation(conditionalRequired('nested.isRequired'))
      declare name2: string;

      nested = {
        isRequired: false,
      };
    }

    class Dummy2Model extends Model {
      @belongsTo('dummy', { async: false, inverse: null })
      @validation(conditionalRequired('isRequired', 'belongsTo'))
      declare parent: DummyModel;

      get isRequired() {
        return false;
      }
    }

    hooks.beforeEach(function (this: RequiredTestContext) {
      this.owner.register('model:dummy', DummyModel);
      this.owner.register('model:dummy2', Dummy2Model);
    });

    test('validate property', async function (this: RequiredTestContext, assert) {
      const dummy = this.store.createRecord('dummy') as unknown as DummyModel;
      const dummyIsRequiredStub = sinon.stub(dummy, 'isRequired');

      const attributes = DummyModel.attributes as unknown as Map<string, any>;
      assert.ok(
        attributes.has('name'),
        '[0] DummyModel has an attribute "name"',
      );
      assert.strictEqual(
        dummy.constructor,
        DummyModel,
        '[0] The constructor of the dummy instance is the DummyModel class',
      );
      assert.strictEqual(
        dummy.name,
        undefined,
        '[0] dummy.name is undefined at first',
      );
      assert.strictEqual(
        dummy.errors.get('length'),
        0,
        '[0] There are no errors on dummy yet',
      );

      dummyIsRequiredStub.get(() => false);
      assert.ok(
        await this.validation.validateModel(dummy),
        '[1] Validation of dummy succeeds, because isRequired is false',
      );

      dummyIsRequiredStub.get(() => true);
      assert.notOk(
        await this.validation.validateModel(dummy),
        '[2] Validation of dummy fails because name is required',
      );
      assert.strictEqual(
        dummy.errors.get('length'),
        1,
        '[2] There is one error on dummy',
      );
      assert.ok(
        dummy.errors.has('name'),
        '[2] dummy has an error on the "name" attribute',
      );
      assert.deepEqual(
        dummy.errors.errorsFor('name'),
        [
          {
            attribute: 'name',
            message: 'ember-attribute-validations.conditionalRequired',
          },
        ],
        '[2] The error on dummy is the "conditionalRequired" error',
      );

      dummy.errors.remove('name');
      assert.strictEqual(
        dummy.errors.get('length'),
        0,
        '[3] There are no errors on dummy anymore',
      );
      dummy.name = '';
      assert.notOk(
        await this.validation.validateModel(dummy),
        '[3] Validation of dummy fails because name is required (and it is an empty string)',
      );
      assert.strictEqual(
        dummy.errors.get('length'),
        1,
        '[3] There is one error on dummy',
      );
      assert.ok(
        dummy.errors.has('name'),
        '[3] dummy has an error on the "name" attribute',
      );
      assert.deepEqual(
        dummy.errors.errorsFor('name'),
        [
          {
            attribute: 'name',
            message: 'ember-attribute-validations.conditionalRequired',
          },
        ],
        '[3] The error on dummy is the "conditionalRequired" error',
      );

      dummy.errors.remove('name');
      assert.strictEqual(
        dummy.errors.get('length'),
        0,
        '[4] There are no errors on dummy anymore',
      );
      dummy.name = 'Dummy';
      assert.ok(
        await this.validation.validateModel(dummy),
        '[4] Validation of dummy succeeds because name is not required anymore',
      );
      assert.strictEqual(
        dummy.errors.get('length'),
        0,
        '[4] There are no errors on dummy anymore',
      );
    });

    test('validate nested property', async function (this: RequiredTestContext, assert) {
      const dummy = this.store.createRecord('dummy') as unknown as DummyModel;

      const attributes = DummyModel.attributes as unknown as Map<string, any>;
      assert.ok(
        attributes.has('name2'),
        '[0] DummyModel has an attribute "name2"',
      );
      assert.strictEqual(
        dummy.constructor,
        DummyModel,
        '[0] The constructor of the dummy instance is the DummyModel class',
      );
      assert.strictEqual(
        dummy.name2,
        undefined,
        '[0] dummy.name2 is undefined at first',
      );
      assert.strictEqual(
        dummy.errors.get('length'),
        0,
        '[0] There are no errors on dummy yet',
      );

      dummy.nested.isRequired = false;
      assert.ok(
        await this.validation.validateModel(dummy),
        '[1] Validation of dummy succeeds, because nested.isRequired is false',
      );

      dummy.nested.isRequired = true;
      assert.notOk(
        await this.validation.validateModel(dummy),
        '[2] Validation of dummy fails because name2 is required',
      );
      assert.strictEqual(
        dummy.errors.get('length'),
        1,
        '[2] There is one error on dummy',
      );
      assert.ok(
        dummy.errors.has('name2'),
        '[2] dummy has an error on the "name2" attribute',
      );
      assert.deepEqual(
        dummy.errors.errorsFor('name2'),
        [
          {
            attribute: 'name2',
            message: 'ember-attribute-validations.conditionalRequired',
          },
        ],
        '[2] The error on dummy is the "conditionalRequired" error',
      );

      dummy.errors.remove('name2');
      assert.strictEqual(
        dummy.errors.get('length'),
        0,
        '[3] There are no errors on dummy anymore',
      );
      dummy.name2 = '';
      assert.notOk(
        await this.validation.validateModel(dummy),
        '[3] Validation of dummy fails because name2 is required (and it is an empty string)',
      );
      assert.strictEqual(
        dummy.errors.get('length'),
        1,
        '[3] There is one error on dummy',
      );
      assert.ok(
        dummy.errors.has('name2'),
        '[3] dummy has an error on the "name2" attribute',
      );
      assert.deepEqual(
        dummy.errors.errorsFor('name2'),
        [
          {
            attribute: 'name2',
            message: 'ember-attribute-validations.conditionalRequired',
          },
        ],
        '[3] The error on dummy is the "conditionalRequired" error',
      );

      dummy.errors.remove('name2');
      assert.strictEqual(
        dummy.errors.get('length'),
        0,
        '[4] There are no errors on dummy anymore',
      );
      dummy.name2 = 'Dummy';
      assert.ok(
        await this.validation.validateModel(dummy),
        '[4] Validation of dummy succeeds because name2 is not required anymore',
      );
      assert.strictEqual(
        dummy.errors.get('length'),
        0,
        '[4] There are no errors on dummy anymore',
      );
    });

    test('validate relationship', async function (this: RequiredTestContext, assert) {
      const dummy = this.store.createRecord('dummy') as unknown as DummyModel;
      // dummy.id = '3';
      const dummy2 = this.store.createRecord('dummy2') as Dummy2Model;
      const dummy2IsRequiredStub = sinon.stub(dummy2, 'isRequired');

      const attributes = Dummy2Model.attributes as unknown as Map<string, any>;
      const relationships = Dummy2Model.relationships as unknown as Map<
        string,
        any
      >;

      assert.notOk(
        attributes.has('name'),
        '[0] Dummy2Model has no attribute "name"',
      );
      assert.notOk(
        attributes.has('dummy'),
        '[0] Dummy2Model has no attribute "dummy"',
      );
      assert.ok(
        relationships.has('dummy'),
        '[0] Dummy2Model has a relationship "dummy"',
      );
      assert.strictEqual(
        dummy2.constructor,
        Dummy2Model,
        '[0] The constructor of the dummy2 instance is the Dummy2Model class',
      );
      assert.strictEqual(
        dummy2.parent,
        null,
        '[0] dummy2.parent is null at first',
      );
      assert.strictEqual(
        dummy2.errors.get('length'),
        0,
        '[0] There are no errors on dummy2 yet',
      );

      dummy2IsRequiredStub.get(() => false);
      assert.ok(
        await this.validation.validateModel(dummy2),
        '[1] Validation of dummy2 succeeds, because isRequired is false',
      );

      dummy2IsRequiredStub.get(() => true);
      assert.notOk(
        await this.validation.validateModel(dummy2),
        '[2] Validation of dummy2 fails because parent is required',
      );
      assert.strictEqual(
        dummy2.errors.get('length'),
        1,
        '[2] There is one error on dummy2',
      );
      assert.ok(
        dummy2.errors.has('parent'),
        '[2] dummy2 has an error on the "parent" attribute',
      );
      assert.deepEqual(
        dummy2.errors.errorsFor('parent'),
        [
          {
            attribute: 'parent',
            message: 'ember-attribute-validations.conditionalRequired',
          },
        ],
        '[2] The error on dummy2 is the "conditionalRequired" error',
      );

      dummy2.errors.remove('parent');
      assert.strictEqual(
        dummy2.errors.get('length'),
        0,
        '[3] There are no errors on dummy2 anymore',
      );
      dummy2.parent = dummy;
      assert.notOk(
        await this.validation.validateModel(dummy2),
        '[3] Validation of dummy2 fails because parent has no id',
      );
      assert.strictEqual(
        dummy2.errors.get('length'),
        1,
        '[3] There is one error on dummy2',
      );
      assert.deepEqual(
        dummy2.errors.errorsFor('parent'),
        [
          {
            attribute: 'parent',
            message: 'ember-attribute-validations.conditionalRequired',
          },
        ],
        '[3] The error on dummy2 is the "conditionalRequired" error',
      );

      dummy2.errors.remove('parent');
      assert.strictEqual(
        dummy2.errors.get('length'),
        0,
        '[4] There are no errors on dummy2 anymore',
      );
      dummy.id = '3';
      assert.ok(
        await this.validation.validateModel(dummy2),
        '[4] Validation of dummy2 succeeds because parent is set and has an id',
      );
      assert.strictEqual(
        dummy2.errors.get('length'),
        0,
        '[4] There are no errors on dummy2 anymore',
      );
    });

    test('validate relationship (set but not loaded)', async function (this: RequiredTestContext, assert) {
      this.store.push({
        data: {
          id: 'dummy2id',
          type: 'dummy2',
          attributes: {},
        },
      });
      const dummy2 = this.store.peekRecord('dummy2', 'dummy2id') as Dummy2Model;
      const dummy2IsRequiredStub = sinon.stub(dummy2, 'isRequired');

      assert.notOk(
        dummy2.belongsTo('parent').id(),
        'No id for parent yet, because it is not set.',
      );

      dummy2IsRequiredStub.get(() => false);
      assert.ok(
        await this.validation.validateModel(dummy2),
        '[1] Validation of dummy2 succeeds, because isRequired is false',
      );

      dummy2IsRequiredStub.get(() => true);
      assert.notOk(
        await this.validation.validateModel(dummy2),
        '[2] Validation of dummy2 fails because parent is required',
      );
      assert.strictEqual(
        dummy2.errors.get('length'),
        1,
        '[2] There is one error on dummy2',
      );
      assert.ok(
        dummy2.errors.has('parent'),
        '[2] dummy2 has an error on the "parent" attribute',
      );

      dummy2.errors.remove('parent');
      assert.strictEqual(
        dummy2.errors.get('length'),
        0,
        '[3] There are no errors on dummy2 anymore',
      );

      this.store.push({
        data: {
          id: 'dummy2id',
          type: 'dummy2',
          attributes: {},
          relationships: {
            parent: {
              data: {
                id: 'parentId',
                type: 'dummy',
              },
            },
          },
        },
      });

      assert.strictEqual(
        dummy2.belongsTo('parent').id(),
        'parentId',
        'There is an id for the parent relation (store.push has worked).',
      );

      assert.ok(
        await this.validation.validateModel(dummy2),
        '[4] Validation of dummy2 succeeds because parent is set and has an id',
      );
      assert.strictEqual(
        dummy2.errors.get('length'),
        0,
        '[4] There are no errors on dummy2 anymore',
      );
    });
  });

  module('using an assertion function', function (hooks) {
    let assertionResult = false;

    function dummyIsRequired(_dummy: DummyModel | Dummy2Model) {
      return assertionResult;
    }

    class DummyModel extends Model {
      @attr('string')
      @validation(conditionalRequired(dummyIsRequired))
      declare name: string;
    }

    class Dummy2Model extends Model {
      @belongsTo('dummy', { async: false, inverse: null })
      @validation(conditionalRequired(dummyIsRequired, 'belongsTo'))
      declare parent: DummyModel;
    }

    hooks.beforeEach(function (this: RequiredTestContext) {
      this.owner.register('model:dummy', DummyModel);
      this.owner.register('model:dummy2', Dummy2Model);
    });

    test('object is passed to the assertion function', async function (this: RequiredTestContext, assert) {
      const asserted = assert.async();
      function assertionFunction(model: SomeModel) {
        asserted();
        assert.strictEqual(
          model,
          instance,
          'the object (model instance) is passed to the assertion function',
        );
        return true;
      }

      class SomeModel extends Model {
        @attr()
        @validation(conditionalRequired(assertionFunction))
        declare name: string;
      }
      this.owner.register('model:some-model', SomeModel);

      const instance = this.store.createRecord('some-model');

      await this.validation.validateModel(instance);
    });

    test('validate property', async function (this: RequiredTestContext, assert) {
      const dummy = this.store.createRecord('dummy') as unknown as DummyModel;

      const attributes = DummyModel.attributes as unknown as Map<string, any>;
      assert.ok(
        attributes.has('name'),
        '[0] DummyModel has an attribute "name"',
      );
      assert.strictEqual(
        dummy.constructor,
        DummyModel,
        '[0] The constructor of the dummy instance is the DummyModel class',
      );
      assert.strictEqual(
        dummy.name,
        undefined,
        '[0] dummy.name is undefined at first',
      );
      assert.strictEqual(
        dummy.errors.get('length'),
        0,
        '[0] There are no errors on dummy yet',
      );

      assertionResult = false;
      assert.ok(
        await this.validation.validateModel(dummy),
        '[1] Validation of dummy succeeds, because assertion function returns false',
      );

      assertionResult = true;
      assert.notOk(
        await this.validation.validateModel(dummy),
        '[2] Validation of dummy fails because name is required',
      );
      assert.strictEqual(
        dummy.errors.get('length'),
        1,
        '[2] There is one error on dummy',
      );
      assert.ok(
        dummy.errors.has('name'),
        '[2] dummy has an error on the "name" attribute',
      );
      assert.deepEqual(
        dummy.errors.errorsFor('name'),
        [
          {
            attribute: 'name',
            message: 'ember-attribute-validations.conditionalRequired',
          },
        ],
        '[2] The error on dummy is the "conditionalRequired" error',
      );

      dummy.errors.remove('name');
      assert.strictEqual(
        dummy.errors.get('length'),
        0,
        '[3] There are no errors on dummy anymore',
      );
      dummy.name = '';
      assert.notOk(
        await this.validation.validateModel(dummy),
        '[3] Validation of dummy fails because name is required (and it is an empty string)',
      );
      assert.strictEqual(
        dummy.errors.get('length'),
        1,
        '[3] There is one error on dummy',
      );
      assert.ok(
        dummy.errors.has('name'),
        '[3] dummy has an error on the "name" attribute',
      );
      assert.deepEqual(
        dummy.errors.errorsFor('name'),
        [
          {
            attribute: 'name',
            message: 'ember-attribute-validations.conditionalRequired',
          },
        ],
        '[3] The error on dummy is the "conditionalRequired" error',
      );

      dummy.errors.remove('name');
      assert.strictEqual(
        dummy.errors.get('length'),
        0,
        '[4] There are no errors on dummy anymore',
      );
      dummy.name = 'Dummy';
      assert.ok(
        await this.validation.validateModel(dummy),
        '[4] Validation of dummy succeeds because name is not required anymore',
      );
      assert.strictEqual(
        dummy.errors.get('length'),
        0,
        '[4] There are no errors on dummy anymore',
      );
    });

    test('validate relationship', async function (this: RequiredTestContext, assert) {
      const dummy = this.store.createRecord('dummy') as unknown as DummyModel;
      // dummy.id = '3';
      const dummy2 = this.store.createRecord('dummy2') as Dummy2Model;

      const attributes = Dummy2Model.attributes as unknown as Map<string, any>;
      const relationships = Dummy2Model.relationships as unknown as Map<
        string,
        any
      >;

      assert.notOk(
        attributes.has('name'),
        '[0] Dummy2Model has no attribute "name"',
      );
      assert.notOk(
        attributes.has('dummy'),
        '[0] Dummy2Model has no attribute "dummy"',
      );
      assert.ok(
        relationships.has('dummy'),
        '[0] Dummy2Model has a relationship "dummy"',
      );
      assert.strictEqual(
        dummy2.constructor,
        Dummy2Model,
        '[0] The constructor of the dummy2 instance is the Dummy2Model class',
      );
      assert.strictEqual(
        dummy2.parent,
        null,
        '[0] dummy2.parent is null at first',
      );
      assert.strictEqual(
        dummy2.errors.get('length'),
        0,
        '[0] There are no errors on dummy2 yet',
      );

      assertionResult = false;
      assert.ok(
        await this.validation.validateModel(dummy2),
        '[1] Validation of dummy2 succeeds, because assertion function returns false',
      );

      assertionResult = true;
      assert.notOk(
        await this.validation.validateModel(dummy2),
        '[2] Validation of dummy2 fails because parent is required',
      );
      assert.strictEqual(
        dummy2.errors.get('length'),
        1,
        '[2] There is one error on dummy2',
      );
      assert.ok(
        dummy2.errors.has('parent'),
        '[2] dummy2 has an error on the "parent" attribute',
      );
      assert.deepEqual(
        dummy2.errors.errorsFor('parent'),
        [
          {
            attribute: 'parent',
            message: 'ember-attribute-validations.conditionalRequired',
          },
        ],
        '[2] The error on dummy2 is the "conditionalRequired" error',
      );

      dummy2.errors.remove('parent');
      assert.strictEqual(
        dummy2.errors.get('length'),
        0,
        '[3] There are no errors on dummy2 anymore',
      );
      dummy2.parent = dummy;
      assert.notOk(
        await this.validation.validateModel(dummy2),
        '[3] Validation of dummy2 fails because parent has no id',
      );
      assert.strictEqual(
        dummy2.errors.get('length'),
        1,
        '[3] There is one error on dummy2',
      );
      assert.deepEqual(
        dummy2.errors.errorsFor('parent'),
        [
          {
            attribute: 'parent',
            message: 'ember-attribute-validations.conditionalRequired',
          },
        ],
        '[3] The error on dummy2 is the "conditionalRequired" error',
      );

      dummy2.errors.remove('parent');
      assert.strictEqual(
        dummy2.errors.get('length'),
        0,
        '[4] There are no errors on dummy2 anymore',
      );
      dummy.id = '3';
      assert.ok(
        await this.validation.validateModel(dummy2),
        '[4] Validation of dummy2 succeeds because parent is set and has an id',
      );
      assert.strictEqual(
        dummy2.errors.get('length'),
        0,
        '[4] There are no errors on dummy2 anymore',
      );
    });

    test('validate relationship (set but not loaded)', async function (this: RequiredTestContext, assert) {
      this.store.push({
        data: {
          id: 'dummy2id',
          type: 'dummy2',
          attributes: {},
        },
      });
      const dummy2 = this.store.peekRecord('dummy2', 'dummy2id') as Dummy2Model;

      assert.notOk(
        dummy2.belongsTo('parent').id(),
        'No id for parent yet, because it is not set.',
      );

      assertionResult = false;
      assert.ok(
        await this.validation.validateModel(dummy2),
        '[1] Validation of dummy2 succeeds, because assertion function returns false',
      );

      assertionResult = true;
      assert.notOk(
        await this.validation.validateModel(dummy2),
        '[2] Validation of dummy2 fails because parent is required',
      );
      assert.strictEqual(
        dummy2.errors.get('length'),
        1,
        '[2] There is one error on dummy2',
      );
      assert.ok(
        dummy2.errors.has('parent'),
        '[2] dummy2 has an error on the "parent" attribute',
      );

      dummy2.errors.remove('parent');
      assert.strictEqual(
        dummy2.errors.get('length'),
        0,
        '[3] There are no errors on dummy2 anymore',
      );

      this.store.push({
        data: {
          id: 'dummy2id',
          type: 'dummy2',
          attributes: {},
          relationships: {
            parent: {
              data: {
                id: 'parentId',
                type: 'dummy',
              },
            },
          },
        },
      });

      assert.strictEqual(
        dummy2.belongsTo('parent').id(),
        'parentId',
        'There is an id for the parent relation (store.push has worked).',
      );

      assert.ok(
        await this.validation.validateModel(dummy2),
        '[4] Validation of dummy2 succeeds because parent is set and has an id',
      );
      assert.strictEqual(
        dummy2.errors.get('length'),
        0,
        '[4] There are no errors on dummy2 anymore',
      );
    });
  });
});
