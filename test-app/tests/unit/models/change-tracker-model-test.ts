import type { TestContext } from '@ember/test-helpers';
import { setupTest } from 'ember-qunit';
import { module, skip, test } from 'qunit';
import Model, {
  type SyncHasMany,
  attr,
  belongsTo,
  hasMany,
} from '@ember-data/model';
import Store from '@ember-data/store';
import ChangeTrackerModel from '@getflights/ember-mist-components/models/change-tracker-model';
import EmberObject from '@ember/object';

interface ChangeTrackerTestContext extends TestContext {
  store: Store;
}

module('Unit | Model | ChangeTrackerModel', function (hooks) {
  setupTest(hooks);

  hooks.beforeEach(function (this: ChangeTrackerTestContext) {
    this.store = this.owner.lookup('service:store');

    this.owner.register(
      'adapter:test',
      class TestAdapter extends EmberObject {
        createRecord() {
          return Promise.resolve();
        }

        updateRecord() {
          return Promise.resolve();
        }
      },
    );
  });

  hooks.afterEach(function (this: ChangeTrackerTestContext) {
    this.store.unloadAll();
  });

  /**
   * When doing an Ember Data upgrade, we want to make sure the following public API's still work as we expect.
   */
  test('Ember Data assumptions', async function (this: ChangeTrackerTestContext, assert) {
    class ParentModel extends Model {
      @hasMany('test', {
        async: false,
        inverse: 'parent',
      })
      declare tests: SyncHasMany<TestModel>;
    }

    class TestModel extends Model {
      @attr()
      declare someProperty: string;

      @belongsTo('parent', {
        async: false,
        inverse: 'tests',
        other: 'other',
      })
      declare parent: ParentModel;

      @hasMany('child', {
        async: false,
        inverse: 'test',
        other: 'other',
      })
      declare children: SyncHasMany<ChildModel>;
    }

    class ChildModel extends Model {
      @belongsTo('test', {
        async: false,
        inverse: 'children',
      })
      declare test: TestModel;
    }

    this.owner.register('model:parent', ParentModel);
    this.owner.register('model:test', TestModel);
    this.owner.register('model:child', ChildModel);

    const instance = this.store.push({
      data: { id: 'test_model', type: 'test' },
    }) as TestModel;
    /**
     * hasDirtyAttributes / changedAttributes()
     */
    assert.notOk(
      instance.hasDirtyAttributes,
      '[hasDirtyAttributes] No dirty attributes yet',
    );
    assert.deepEqual(
      instance.changedAttributes(),
      {},
      '[changedAttributes] No changes yet',
    );

    instance.someProperty = 'now it is dirty';

    assert.ok(
      instance.hasDirtyAttributes,
      '[hasDirtyAttributes] There are dirty attributes after changing one',
    );
    assert.propContains(
      instance.changedAttributes(),
      { someProperty: [undefined, instance.someProperty] },
      `[changedAttributes] Contains the made changes to 'someProperty'`,
    );

    /**
     * eachRelationship
     */
    assert.ok(instance.eachRelationship, `[eachRelationship] exists`);

    type eachRelationshipCallback = Parameters<
      typeof TestModel.eachRelationship<TestModel>
    >[0];
    type RelationshipKey = Parameters<eachRelationshipCallback>[0];
    type RelationshipMeta = Parameters<eachRelationshipCallback>[1];

    const relationshipInfo = new Map<RelationshipKey, RelationshipMeta>();

    instance.eachRelationship((name, details) => {
      relationshipInfo.set(name, details);
    });

    assert.strictEqual(
      relationshipInfo.size,
      2,
      `[eachRelationship] it gives us info about the two relationships`,
    );

    assert.ok(
      relationshipInfo.has('children'),
      `[eachRelationship] 'children' has meta info`,
    );
    const childrenMeta = relationshipInfo.get('children')!;
    assert.strictEqual(
      childrenMeta.kind,
      'hasMany',
      `[eachRelationship] 'children' is of kind 'hasMany'`,
    );
    assert.strictEqual(
      childrenMeta.type,
      'child',
      `[eachRelationship] 'children' is of type 'child'`,
    );
    assert.strictEqual(
      childrenMeta.key,
      'children',
      `[eachRelationship] 'children' key is 'children'`,
    );
    assert.false(
      childrenMeta.options.async,
      `[eachRelationship] 'children' has option 'async'`,
    );
    assert.strictEqual(
      childrenMeta.options['other'],
      'other',
      `[eachRelationship] 'children' has option 'other'`,
    );

    assert.ok(
      relationshipInfo.has('parent'),
      `[eachRelationship] 'parent' has meta info`,
    );
    const parentMeta = relationshipInfo.get('parent')!;
    assert.strictEqual(
      parentMeta.kind,
      'belongsTo',
      `[eachRelationship] 'parent' is of kind 'belongsTo'`,
    );
    assert.strictEqual(
      parentMeta.type,
      'parent',
      `[eachRelationship] 'parent' is of type 'parent'`,
    );
    assert.strictEqual(
      parentMeta.key,
      'parent',
      `[eachRelationship] 'parent' key is 'parent'`,
    );
    assert.false(
      parentMeta.options.async,
      `[eachRelationship] 'parent' has option 'async'`,
    );
    assert.strictEqual(
      parentMeta.options['other'],
      'other',
      `[eachRelationship] 'parent' has option 'other'`,
    );

    /**
     * hasMany() + hasMany().ids()
     */
    assert.ok(instance.hasMany, `[hasMany] exists`);

    instance.children.clear();
    const child = this.store.push({
      data: { id: '1', type: 'child' },
    }) as ChildModel;
    instance.children.pushObject(child);

    const childHasManyReference = instance.hasMany('children');
    assert.ok(
      childHasManyReference,
      `[hasMany] HasMany reference for 'children' exists`,
    );
    assert.deepEqual(
      childHasManyReference.ids(),
      ['1'],
      `[hasMany] HasMany reference for 'children' has the ids of the children`,
    );

    /**
     * belongsTo() + belongsTo().id()
     */
    assert.ok(instance.belongsTo, `[belongsTo] exists`);

    instance.parent = this.store.push({
      data: { id: 'A', type: 'parent' },
    }) as ParentModel;
    const parentBelongsToReference = instance.belongsTo('parent');
    assert.ok(
      parentBelongsToReference,
      `[belongsTo] BelongsTo reference for 'parent' exists`,
    );
    assert.strictEqual(
      parentBelongsToReference.id(),
      'A',
      `[belongsTo] BelongsTo reference for 'parent' has the id of the parent`,
    );
  });

  test('isDirty / hasDirtyRelations / changes()', async function (this: ChangeTrackerTestContext, assert) {
    class ParentModel extends Model {
      @hasMany('test', {
        async: false,
        inverse: 'parent',
      })
      declare tests: SyncHasMany<TestModel>;
    }

    class TestModel extends ChangeTrackerModel {
      changeTracker = { trackHasMany: true, only: ['parent', 'children'] };

      @attr()
      declare someProperty: string;

      @belongsTo('parent', {
        async: false,
        inverse: 'tests',
      })
      declare parent: ParentModel;

      @hasMany('children', {
        async: false,
        inverse: 'test',
      })
      declare children: SyncHasMany<ChildModel>;
    }

    class ChildModel extends Model {
      @belongsTo('test', {
        async: false,
        inverse: 'children',
      })
      declare test: TestModel;
    }

    this.owner.register('model:parent', ParentModel);
    this.owner.register('model:test', TestModel);
    this.owner.register('model:child', ChildModel);

    const instance = this.store.push({
      data: {
        id: 'tid',
        type: 'test',
      },
    }) as TestModel;

    /**
     * Attributes
     */
    assert.notOk(instance.isDirty, '[Attributes] isDirty is false initially');

    instance.someProperty = 'Test';

    assert.ok(
      instance.isDirty,
      '[Attributes] isDirty is true after changing an attribute',
    );
    assert.propContains(
      instance.changes(),
      { someProperty: [undefined, instance.someProperty] },
      `[Attributes] changes() contains the made changes to 'someProperty'`,
    );

    await instance.save();

    assert.notOk(instance.isDirty, '[Attributes] isDirty is false after save');

    /**
     * Relationships (belongsTo)
     */
    assert.notOk(
      instance.hasDirtyRelations,
      '[Relationships] hasDirtyRelations is false',
    );

    const parent1 = this.store.push({
      data: { id: 'p1', type: 'parent' },
    }) as ParentModel;
    instance.parent = parent1;

    assert.ok(
      instance.hasDirtyRelations,
      '[Relationships] hasDirtyRelations is true after setting parent',
    );
    assert.ok(
      instance.isDirty,
      '[Relationships] isDirty is true after setting parent',
    );

    const parentChanges1 = instance.changes()['parent']!;
    assert.strictEqual(
      parentChanges1[0],
      undefined,
      `[Relationships] changes() contains the initial value of 'parent' (undefined)`,
    );
    assert.deepEqual(
      parentChanges1[1],
      parent1,
      `[Relationships] changes() contains the current value of 'parent' (parent1)`,
    );

    await instance.save();

    assert.notOk(
      instance.hasDirtyRelations,
      '[Relationships] hasDirtyRelations is false after save',
    );
    assert.notOk(
      instance.isDirty,
      '[Relationships] isDirty is false after save',
    );

    const parent2 = this.store.push({
      data: { id: 'p2', type: 'parent' },
    }) as ParentModel;
    instance.parent = parent2;

    assert.ok(
      instance.hasDirtyRelations,
      '[Relationships] hasDirtyRelations is true after changing parent',
    );
    assert.ok(
      instance.isDirty,
      '[Relationships] isDirty is true after changing parent',
    );

    const parentChanges2 = instance.changes()['parent']!;
    assert.strictEqual(
      parentChanges2[0],
      parent1,
      `[Relationships] changes() contains the initial value of 'parent' (parent1)`,
    );
    assert.deepEqual(
      parentChanges2[1],
      parent2,
      `[Relationships] changes() contains the current value of 'parent' (parent2)`,
    );

    await instance.save();

    assert.notOk(
      instance.hasDirtyRelations,
      '[Relationships] hasDirtyRelations is false after save',
    );
    assert.notOk(
      instance.isDirty,
      '[Relationships] isDirty is false after save',
    );

    /**
     * Relationships (belongsTo) AND attributes
     */
    const parent3 = this.store.push({
      data: { id: 'p3', type: 'parent' },
    }) as ParentModel;
    instance.parent = parent3;

    instance.someProperty = 'Test both relationships and attributes';

    assert.ok(instance.hasDirtyRelations, '[Both] hasDirtyRelations is true');
    assert.ok(instance.isDirty, '[Both] isDirty is true');

    const changes = instance.changes();
    assert.strictEqual(
      Object.keys(changes).length,
      2,
      '[Both] changes() contains 2 keys',
    );
    assert.ok(
      Object.keys(changes).includes('someProperty'),
      `[Both] changes() contains 'someProperty'`,
    );
    assert.ok(
      Object.keys(changes).includes('parent'),
      `[Both] changes() contains 'parent'`,
    );

    await instance.save();

    assert.notOk(
      instance.hasDirtyRelations,
      '[Both] hasDirtyRelations is false after save',
    );
    assert.notOk(instance.isDirty, '[Both] isDirty is false after save');

    /**
     * Relationships (hasMany)
     */
    instance.children.clear();
    const child1 = this.store.push({
      data: { id: 'c1', type: 'child' },
    }) as ChildModel;
    instance.children.pushObject(child1);

    assert.ok(
      instance.hasDirtyRelations,
      '[HasMany] hasDirtyRelations is true',
    );
    assert.ok(instance.isDirty, '[HasMany] isDirty is true');

    const childrenChanges1 = instance.changes()['children']!;
    assert.strictEqual(
      childrenChanges1[0].length,
      0,
      `[HasMany] changes() contains the initial value of 'children' ([])`,
    );
    assert.strictEqual(
      childrenChanges1[1][0],
      child1,
      `[HasMany] changes() contains the current value of 'children' ([child1])`,
    );

    await instance.save();

    assert.notOk(
      instance.hasDirtyRelations,
      '[HasMany] hasDirtyRelations is false after save',
    );
    assert.notOk(instance.isDirty, '[HasMany] isDirty is false after save');

    instance.children.removeObject(child1);

    assert.ok(
      instance.hasDirtyRelations,
      '[HasMany] hasDirtyRelations is true after removing child',
    );
    assert.ok(
      instance.isDirty,
      '[HasMany] isDirty is true after removing child',
    );

    const childrenChanges2 = instance.changes()['children']!;
    assert.strictEqual(
      childrenChanges2[0][0],
      child1,
      `[HasMany] changes() contains the initial value of 'children' ([child1])`,
    );
    assert.strictEqual(
      childrenChanges2[1].length,
      0,
      `[HasMany] changes() contains the current value of 'children' ([])`,
    );

    await instance.save();

    assert.notOk(
      instance.hasDirtyRelations,
      '[HasMany] hasDirtyRelations is false after save',
    );
    assert.notOk(instance.isDirty, '[HasMany] isDirty is false after save');
  });

  test('POLYMORPHIC BELONGSTO isDirty / hasDirtyRelations / changes()', async function (this: ChangeTrackerTestContext, assert) {
    class ParentModel extends Model {
      @hasMany('test', {
        async: false,
        inverse: 'polyParent',
      })
      declare tests: SyncHasMany<TestModel>;
    }

    class FirstParentModel extends ParentModel {
      readonly name = 'first-parent';
    }

    class SecondParentModel extends ParentModel {
      readonly name = 'second-parent';
    }

    class TestModel extends ChangeTrackerModel {
      changeTracker = {
        trackHasMany: true,
        only: ['polyParent', 'polyChildren'],
      };

      @attr()
      declare someProperty: string;

      @belongsTo('parent', {
        polymorphic: true,
        allowedModelNames: ['first-parent', 'second-parent'],
        async: false,
        inverse: 'tests',
      })
      declare polyParent: FirstParentModel | SecondParentModel;
    }

    this.owner.register('model:parent', ParentModel);
    this.owner.register('model:first-parent', FirstParentModel);
    this.owner.register('model:second-parent', SecondParentModel);
    this.owner.register('model:test', TestModel);

    const instance = this.store.push({
      data: {
        id: 'tid',
        type: 'test',
      },
    }) as TestModel;

    /**
     * Relationships (polymorphic belongsTo)
     */
    assert.notOk(
      instance.hasDirtyRelations,
      '[Relationships] hasDirtyRelations is false',
    );

    const parent1 = this.store.push({
      data: { id: 'p1', type: 'first-parent' },
    }) as FirstParentModel;
    instance.polyParent = parent1;

    assert.ok(
      instance.hasDirtyRelations,
      '[Relationships] hasDirtyRelations is true after setting polyParent',
    );
    assert.ok(
      instance.isDirty,
      '[Relationships] isDirty is true after setting polyParent',
    );

    const parentChanges1 = instance.changes()['polyParent']!;
    assert.strictEqual(
      parentChanges1[0],
      undefined,
      `[Relationships] changes() contains the initial value of 'polyParent' (undefined)`,
    );
    assert.deepEqual(
      parentChanges1[1],
      parent1,
      `[Relationships] changes() contains the current value of 'polyParent' (parent1)`,
    );

    await instance.save();

    assert.notOk(
      instance.hasDirtyRelations,
      '[Relationships] hasDirtyRelations is false after save',
    );
    assert.notOk(
      instance.isDirty,
      '[Relationships] isDirty is false after save',
    );

    const parent2 = this.store.push({
      data: { id: 'p2', type: 'second-parent' },
    }) as SecondParentModel;
    instance.polyParent = parent2;

    assert.ok(
      instance.hasDirtyRelations,
      '[Relationships] hasDirtyRelations is true after changing polyParent',
    );
    assert.ok(
      instance.isDirty,
      '[Relationships] isDirty is true after changing polyParent',
    );

    const parentChanges2 = instance.changes()['polyParent']!;
    assert.strictEqual(
      parentChanges2[0],
      parent1,
      `[Relationships] changes() contains the initial value of 'polyParent' (parent1)`,
    );
    assert.deepEqual(
      parentChanges2[1],
      parent2,
      `[Relationships] changes() contains the current value of 'polyParent' (parent2)`,
    );

    await instance.save();

    assert.notOk(
      instance.hasDirtyRelations,
      '[Relationships] hasDirtyRelations is false after save',
    );
    assert.notOk(
      instance.isDirty,
      '[Relationships] isDirty is false after save',
    );
  });

  skip('Are values updated when pushed into store again (idk refetch or something)', async function (this: ChangeTrackerTestContext, assert) {
    class ParentModel extends Model {
      changeTracker = { trackHasMany: true, only: ['tests'] };

      @hasMany('test', { async: false, inverse: 'parent' })
      declare tests: SyncHasMany<TestModel>;
    }

    class TestModel extends ChangeTrackerModel {
      @belongsTo('parent', { async: false, inverse: 'tests' })
      declare parent: ParentModel;

      @attr()
      declare someProperty?: string;
    }

    class TestAdapter extends EmberObject {
      findRecord() {
        return Promise.resolve({
          data: {
            id: 'idx',
            type: 'test',
            relationships: {
              parent: {
                data: {
                  id: 'p1',
                  type: 'parent',
                },
              },
            },
          },
        });
      }
    }

    this.owner.register('model:parent', ParentModel);
    this.owner.register('model:test', TestModel);
    this.owner.register('adapter:test', TestAdapter);

    const parent = this.store.push({
      data: { id: 'p1', type: 'parent' },
    }) as ParentModel;

    // initially set instance
    const instance = this.store.push({
      data: { id: 'idx', type: 'test' },
    }) as TestModel;

    assert.strictEqual(instance.parent, null, 'Parent is initially null');
    // assert.strictEqual(instance['-relationship-values']['parent'], null, '-relationship-values has null for parent')

    // Find the record (changes the parent to p1)
    await this.store.findRecord('test', instance.id, {
      include: 'parent',
      reload: true,
    });

    assert.strictEqual(instance.parent, parent, 'Parent is set');

    assert.strictEqual(
      Object.keys(instance.changes()).length,
      0,
      'No changes because parent is not new',
    );
  });
});
