import ConditionalForbiddenValidator from '@getflights/ember-mist-components/validators/conditional-forbidden';
import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';

module('Validator | Conditional Forbidden', function (hooks) {
  setupTest(hooks);

  module('using a field', function () {
    test('validate', async function (assert) {
      const propertyKey = 'conditionalForbiddenProperty';
      const validator = new ConditionalForbiddenValidator<typeof target>(
        propertyKey,
        {
          assertionOrField: 'condition',
        },
      );

      /**
       * condition = true
       * Validate as usual
       */
      const target = {
        condition: true,
      };

      const matchError = (err: any) => {
        return (
          err.validator === 'conditionalForbidden' &&
          err.propertyKey === propertyKey
        );
      };

      assert.rejects(validator.validate(true, target), matchError, 'rejected');
      assert.rejects(
        validator.validate('true', target),
        matchError,
        'rejected',
      );
      assert.rejects(validator.validate(false, target), matchError, 'rejected');
      assert.rejects(
        validator.validate('some value', target),
        matchError,
        'rejected',
      );
      assert.rejects(validator.validate(0, target), matchError, 'rejected');

      assert.ok(await validator.validate('', target));
      assert.ok(await validator.validate(null, target));
      assert.ok(await validator.validate(undefined, target));

      /**
       * condition = false
       * Always valid, because the conditional field is false
       */
      target.condition = false;

      // Should normally return an error, but now it's valid
      assert.ok(await validator.validate(true, target));
      assert.ok(await validator.validate('true', target));
      assert.ok(await validator.validate(false, target));
      assert.ok(await validator.validate('some value', target));
      assert.ok(await validator.validate(0, target));
    });

    test('validate nested condition', async function (assert) {
      const propertyKey = 'conditionalForbiddenProperty';
      const validator = new ConditionalForbiddenValidator<typeof target>(
        propertyKey,
        {
          assertionOrField: 'nested.condition',
        },
      );

      /**
       * mested.condition = true
       * Validate as usual
       */
      const target = {
        nested: {
          condition: true,
        },
      };

      const matchError = (err: any) => {
        return (
          err.validator === 'conditionalForbidden' &&
          err.propertyKey === propertyKey
        );
      };

      assert.rejects(validator.validate(true, target), matchError, 'rejected');
      assert.rejects(
        validator.validate('true', target),
        matchError,
        'rejected',
      );
      assert.rejects(validator.validate(false, target), matchError, 'rejected');
      assert.rejects(
        validator.validate('some value', target),
        matchError,
        'rejected',
      );
      assert.rejects(validator.validate(0, target), matchError, 'rejected');

      assert.ok(await validator.validate('', target));
      assert.ok(await validator.validate(null, target));
      assert.ok(await validator.validate(undefined, target));

      /**
       * nested.condition = false
       * Always valid, because the conditional field is false
       */
      target.nested.condition = false;

      // Should normally return an error, but now it's valid
      assert.ok(await validator.validate(true, target));
      assert.ok(await validator.validate('true', target));
      assert.ok(await validator.validate(false, target));
      assert.ok(await validator.validate('some value', target));
      assert.ok(await validator.validate(0, target));
    });
  });

  module('using an assertion function', function () {
    test('validate', async function (assert) {
      interface Target {
        condition: boolean;
      }

      function conditionIsTrue(target: Target) {
        return !!target['condition'];
      }

      const propertyKey = 'conditionalForbiddenProperty';
      const validator = new ConditionalForbiddenValidator(propertyKey, {
        assertionOrField: conditionIsTrue,
      });

      /**
       * condition = true
       * Validate as usual
       */
      const target: Target = {
        condition: true,
      };

      const matchError = (err: any) => {
        return (
          err.validator === 'conditionalForbidden' &&
          err.propertyKey === propertyKey
        );
      };

      assert.rejects(validator.validate(true, target), matchError, 'rejected');
      assert.rejects(
        validator.validate('true', target),
        matchError,
        'rejected',
      );
      assert.rejects(validator.validate(false, target), matchError, 'rejected');
      assert.rejects(
        validator.validate('some value', target),
        matchError,
        'rejected',
      );
      assert.rejects(validator.validate(0, target), matchError, 'rejected');

      assert.ok(await validator.validate('', target));
      assert.ok(await validator.validate(null, target));
      assert.ok(await validator.validate(undefined, target));

      /**
       * condition = false
       * Always valid, because the conditional field is false
       */
      target.condition = false;

      // Should normally return an error, but now it's valid
      assert.ok(await validator.validate(true, target));
      assert.ok(await validator.validate('true', target));
      assert.ok(await validator.validate(false, target));
      assert.ok(await validator.validate('some value', target));
      assert.ok(await validator.validate(0, target));
    });
  });
});
