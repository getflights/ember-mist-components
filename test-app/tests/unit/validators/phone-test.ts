import { setOwner } from '@ember/application';
import type { TestContext } from '@ember/test-helpers';
import PhoneValidator from '@getflights/ember-mist-components/validators/phone';
import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';

module('Validator | Phone', function (hooks) {
  setupTest(hooks);

  test('validate', async function (this: TestContext, assert) {
    const propertyKey = 'phoneProperty';

    const validator = new PhoneValidator(propertyKey);
    setOwner(validator, this.owner);

    const target = {};

    assert.rejects(validator.validate('mijngsm', target)); // not a phonen umber
    assert.rejects(validator.validate('+3249136', target)); // too short
    assert.ok(await validator.validate('+3250661616', target));
    assert.ok(await validator.validate('+32474123456', target));
    assert.ok(await validator.validate('+32474123456', target));
  });
});
