import { setOwner } from '@ember/application';
import type { TestContext } from '@ember/test-helpers';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';
import AddressService from '@getflights/ember-mist-components/services/address';
import ValidationService from '@getflights/ember-attribute-validations/services/validation';
import CountryValidator from '@getflights/ember-mist-components/validators/country';
import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import Model, { attr } from '@ember-data/model';
import Store from '@ember-data/store';
import validation from '@getflights/ember-attribute-validations/decorators/validation';
import { country } from '@getflights/ember-mist-components/validations/country';
import { trackedFunction } from 'reactiveweb/function';

interface CountryTestContext extends TestContext {
  validation: ValidationService;
  address: AddressService;
  store: Store;
}

module('Validator | Country', function (hooks) {
  setupTest(hooks);

  hooks.beforeEach(function (this: CountryTestContext) {
    /**
     * Setup
     */
    // Stub address service
    class AddressServiceStub extends AddressService {
      // @ts-ignore AddressServiceStub !== AddressService
      countrySelectOptionsRequest = trackedFunction(this, async () => {
        const value = await new Promise<SelectOption[]>((resolve) => {
          setTimeout(() => {
            resolve([
              { value: 'BE', label: 'Belgium' },
              { value: 'FR', label: 'France' },
            ]);
          }, 50);
        });

        return value;
      });
    }

    this.owner.register('service:address', AddressServiceStub);

    // Lookup address and store services
    this.address = this.owner.lookup('service:address') as AddressService;
    this.store = this.owner.lookup('service:store') as Store;
    this.validation = this.owner.lookup(
      'service:validation',
    ) as ValidationService;
  });

  test('validate using validator', async function (this: CountryTestContext, assert) {
    /**
     * Validation
     */
    const propertyKey = 'countryProperty';

    const validator = new CountryValidator(propertyKey);
    setOwner(validator, this.owner);

    const target = {};

    // Valid country
    assert.ok(await validator.validate('BE', target));
    assert.ok(await validator.validate('FR', target));

    // Invalid country
    const matchError = (err: any) => {
      return err.validator === 'country' && err.propertyKey === propertyKey;
    };

    assert.rejects(validator.validate('XD', target), matchError, 'rejected');
  });

  test('validate on a model', async function (this: CountryTestContext, assert) {
    class DummyCountryModel extends Model {
      @attr('string')
      @validation(country())
      someCountry!: string;
    }

    this.owner.register('model:dummy-country', DummyCountryModel);
    const instance = this.store.createRecord(
      'dummy-country',
    ) as DummyCountryModel;

    // Valid
    // - No country code
    assert.ok(await this.validation.validateModel(instance));
    // - Valid country code
    instance.someCountry = 'BE';
    assert.ok(await this.validation.validateModel(instance));

    // Invalid
    instance.someCountry = 'XD';
    assert.notOk(await this.validation.validateModel(instance));
  });
});
