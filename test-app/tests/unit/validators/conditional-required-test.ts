import Validator from '@getflights/ember-mist-components/validators/conditional-required';
import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';

module('Validator | Conditional Required', function (hooks) {
  setupTest(hooks);

  module('using a field', function () {
    test('validate', async function (assert) {
      const propertyKey = 'conditionalRequiredProperty';
      const validator = new Validator<typeof target>(propertyKey, {
        assertionOrField: 'condition',
      });

      /**
       * condition = true
       * Validate as usual
       */
      const target = {
        condition: true,
      };

      assert.ok(await validator.validate(true, target));
      assert.ok(await validator.validate('true', target));
      assert.ok(await validator.validate(false, target));
      assert.ok(await validator.validate('some value', target));
      assert.ok(await validator.validate(0, target));

      const matchError = (err: any) => {
        return (
          err.validator === 'conditionalRequired' &&
          err.propertyKey === propertyKey
        );
      };

      assert.rejects(validator.validate('', target), matchError, 'rejected');
      assert.rejects(validator.validate(null, target), matchError, 'rejected');
      assert.rejects(
        validator.validate(undefined, target),
        matchError,
        'rejected',
      );

      /**
       * condition = false
       * Always valid, because the conditional field is false
       */
      target.condition = false;

      // Should normally return an error, but now it's valid
      assert.ok(await validator.validate('', target));
      assert.ok(await validator.validate(null, target));
      assert.ok(await validator.validate(undefined, target));
    });

    test('validate nested condition', async function (assert) {
      const propertyKey = 'conditionalRequiredProperty';
      const validator = new Validator<typeof target>(propertyKey, {
        assertionOrField: 'nested.condition',
      });

      /**
       * mested.condition = true
       * Validate as usual
       */
      const target = {
        nested: {
          condition: true,
        },
      };

      assert.ok(await validator.validate(true, target));
      assert.ok(await validator.validate('true', target));
      assert.ok(await validator.validate(false, target));
      assert.ok(await validator.validate('some value', target));
      assert.ok(await validator.validate(0, target));

      const matchError = (err: any) => {
        return (
          err.validator === 'conditionalRequired' &&
          err.propertyKey === propertyKey
        );
      };

      assert.rejects(validator.validate('', target), matchError, 'rejected');
      assert.rejects(validator.validate(null, target), matchError, 'rejected');
      assert.rejects(
        validator.validate(undefined, target),
        matchError,
        'rejected',
      );

      /**
       * nested.condition = false
       * Always valid, because the conditional field is false
       */
      target.nested.condition = false;

      // Should normally return an error, but now it's valid
      assert.ok(await validator.validate('', target));
      assert.ok(await validator.validate(null, target));
      assert.ok(await validator.validate(undefined, target));
    });
  });

  module('using an assertion function', function () {
    test('validate', async function (assert) {
      interface Target {
        condition: boolean;
      }

      function conditionIsTrue(target: Target) {
        return !!target['condition'];
      }

      const propertyKey = 'conditionalRequiredProperty';
      const validator = new Validator(propertyKey, {
        assertionOrField: conditionIsTrue,
      });

      /**
       * condition = true
       * Validate as usual
       */
      const target: Target = {
        condition: true,
      };

      assert.ok(await validator.validate(true, target));
      assert.ok(await validator.validate('true', target));
      assert.ok(await validator.validate(false, target));
      assert.ok(await validator.validate('some value', target));
      assert.ok(await validator.validate(0, target));

      const matchError = (err: any) => {
        return (
          err.validator === 'conditionalRequired' &&
          err.propertyKey === propertyKey
        );
      };

      assert.rejects(validator.validate('', target), matchError, 'rejected');
      assert.rejects(validator.validate(null, target), matchError, 'rejected');
      assert.rejects(
        validator.validate(undefined, target),
        matchError,
        'rejected',
      );

      /**
       * condition = false
       * Always valid, because the conditional field is false
       */
      target.condition = false;

      // Should normally return an error, but now it's valid
      assert.ok(await validator.validate('', target));
      assert.ok(await validator.validate(null, target));
      assert.ok(await validator.validate(undefined, target));
    });

    test('validate nested condition', async function (assert) {
      const propertyKey = 'conditionalRequiredProperty';
      const validator = new Validator<typeof target>(propertyKey, {
        assertionOrField: 'nested.condition',
      });

      /**
       * mested.condition = true
       * Validate as usual
       */
      const target = {
        nested: {
          condition: true,
        },
      };

      assert.ok(await validator.validate(true, target));
      assert.ok(await validator.validate('true', target));
      assert.ok(await validator.validate(false, target));
      assert.ok(await validator.validate('some value', target));
      assert.ok(await validator.validate(0, target));

      const matchError = (err: any) => {
        return (
          err.validator === 'conditionalRequired' &&
          err.propertyKey === propertyKey
        );
      };

      assert.rejects(validator.validate('', target), matchError, 'rejected');
      assert.rejects(validator.validate(null, target), matchError, 'rejected');
      assert.rejects(
        validator.validate(undefined, target),
        matchError,
        'rejected',
      );

      /**
       * nested.condition = false
       * Always valid, because the conditional field is false
       */
      target.nested.condition = false;

      // Should normally return an error, but now it's valid
      assert.ok(await validator.validate('', target));
      assert.ok(await validator.validate(null, target));
      assert.ok(await validator.validate(undefined, target));
    });
  });
});
