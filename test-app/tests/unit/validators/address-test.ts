import AddressValidator from '@getflights/ember-mist-components/validators/address';
import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import Store from '@ember-data/store';
import type Address from '@getflights/ember-mist-components/interfaces/address';
import AddressService from '@getflights/ember-mist-components/services/address';
import { setOwner } from '@ember/application';

module('Validator | Address', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const addressService = this.owner.lookup(
      'service:address',
    ) as AddressService;
    addressService.shouldCache = false;

    const store = this.owner.lookup('service:store') as Store;

    store.createRecord('address-format', {
      id: 'BE',
      requiredFields: ['addressLine1', 'postalCode'],
    });

    const propertyKey = 'addressProperty';

    const validator = new AddressValidator(propertyKey);
    setOwner(validator, this.owner);

    const target = {};

    const matchError = (err: any) => {
      return err.validator === 'address' && err.propertyKey === propertyKey;
    };

    await assert.rejects(
      validator.validate({}, target),
      matchError,
      'rejected because not an address',
    );

    await assert.rejects(
      validator.validate('notAnAddress', target),
      matchError,
      'rejected because not an address',
    );

    // Address, not blank anymore, but also not valid
    const addr: Address = { countryCode: 'BE' };

    await assert.rejects(
      validator.validate(addr, target),
      matchError,
      'rejected because required fields are missing',
    );

    addr.addressLine1 = 'Teststraat 1';
    addr.postalCode = '8310';

    assert.ok(await validator.validate(addr, target));
    // Address, valid
    // addr.countryCode = 'BE';
    // assert.deepEqual(validator.validate(addr, target), matchError);
  });
});
