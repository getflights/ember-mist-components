/**
 * GLINT
 */
import '@glint/environment-ember-loose';
import '@glint/environment-ember-template-imports';

import type FieldComponentsRegistry from '@getflights/ember-field-components/template-registry';
import type MistComponentsRegistry from '@getflights/ember-mist-components/template-registry';

declare module '@glint/environment-ember-loose/registry' {
  export default interface Registry
    extends FieldComponentsRegistry,
      MistComponentsRegistry /* + other addon registries */ {
    // local entries
  }
}

/**
 * EMBER DATA
 */
import type MistModelRegistry from '@getflights/ember-mist-components/models/registry';
import type DummyModel from 'test-app/models/dummy';
import type FieldModel from 'test-app/models/field';

declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry extends MistModelRegistry {
    dummy: DummyModel;
    field: FieldModel;
    // Catch-all for ember-data
    [key: string]: any;
  }
}

import type MistTransformRegistry from '@getflights/ember-mist-components/transforms/registry';

// Transform Registry
declare module 'ember-data/types/registries/transform' {
  export default interface TransformRegistry extends MistTransformRegistry {}
}
