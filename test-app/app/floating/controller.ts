import Controller from '@ember/controller';
import type { FloatingSignature } from '@getflights/ember-mist-components/components/floating';
import { tracked } from 'tracked-built-ins';

export default class FloatingController extends Controller {
  set = <K extends keyof this>(key: K, value: this[K]) => {
    this[key] = value;
  };

  @tracked floatingType: FloatingSignature['Args']['type'] = 'tooltip';
  @tracked floatingOptions: NonNullable<FloatingSignature['Args']['options']> =
    {};
}
