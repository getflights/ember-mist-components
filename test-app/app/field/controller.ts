import Controller from '@ember/controller';
import field from '@getflights/ember-field-components/decorators/field';
import { tracked } from 'tracked-built-ins';
import { service } from '@ember/service';
import type Store from '@ember-data/store';
import select from '@getflights/ember-mist-components/fields/select';

export default class FieldController extends Controller {
  @service declare store: Store;

  constructor(...args: never[]) {
    super(...args);

    this.dummy = this.store.createRecord('dummy');
  }

  dummy;

  @field(select({ selectOptions: [{ label: 'test', value: 'testt' }] }))
  @tracked
  select: string = 'name';

  @field(select())
  @tracked
  select2: string = 'name';
}
