export const dummySelectSelectOptions = {
  data: {
    id: 'dummy.select',
    type: 'field',
    attributes: {
      'select-options': {
        Confirmed: 'Confirmed',
        Cancelled: 'Cancelled',
      },
    },
    relationships: {
      model: {
        data: {
          id: 'sales-order',
          type: 'meta-model',
        },
      },
    },
  },
};
