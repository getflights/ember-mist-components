import { HttpResponse, http } from 'msw';
import { addressFormatBE, addressFormatCL } from './data/address-format';
import {
  subdivisionSelectOptionsCL,
  subdivisionSelectOptionsCLAntofagasta,
  subdivisionSelectOptionsCLAtacama,
} from './data/address-subdivision';
import { dummySelectSelectOptions } from './data/selectoptions';

export const handlers = [
  http.get('*/api/address/countries/selectoptions', () => {
    return HttpResponse.json([
      { value: 'BE', label: 'Belgium' },
      { value: 'CL', label: 'Chile' },
    ]);
  }),

  http.get('*/api/address/format/:id', ({ params }) => {
    const { id } = params;

    if (id === 'BE') {
      return HttpResponse.json(addressFormatBE);
    }

    if (id === 'CL') {
      return HttpResponse.json(addressFormatCL);
    }

    return new HttpResponse(null, { status: 404 });
  }),

  http.get('*/api/address/subdivisions/:name', ({ params }) => {
    const { name } = params;

    if (name === 'CL') {
      return HttpResponse.json(subdivisionSelectOptionsCL);
    }

    if (name === 'CL,Antofagasta') {
      return HttpResponse.json(subdivisionSelectOptionsCLAntofagasta);
    }

    if (name === 'CL,Atacama') {
      return HttpResponse.json(subdivisionSelectOptionsCLAtacama);
    }

    return new HttpResponse(null, { status: 404 });
  }),

  http.get('*/fields/dummy.select', () => {
    return HttpResponse.json(dummySelectSelectOptions);
  }),
];
