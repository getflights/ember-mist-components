import Controller from '@ember/controller';
import { tracked } from 'tracked-built-ins';
import { action } from '@ember/object';
import type Address from '@getflights/ember-mist-components/interfaces/address';
import AddressValidator from '@getflights/ember-mist-components/validators/address';

export default class IndexController extends Controller {
  // declare model: ExampleModel;

  toggle = (key: keyof this) => {
    // @ts-ignore
    this[key] = !this[key];
  };

  @tracked showCountryCode = false;
  @tracked showAddress = false;
  @tracked showPhone = false;

  @tracked countryCode = 'BE';

  @action
  countryCodeChanged(newCountry: string) {
    this.countryCode = newCountry;
  }

  @tracked address: Address | undefined = undefined;

  @action
  addressChanged(newAddress: Address | undefined) {
    this.address = newAddress;
  }

  @tracked validationResult?: string;

  @action
  validateAddress() {
    const addressValidator = new AddressValidator('address');

    if (this.address) {
      addressValidator
        .validate(this.address, this)
        .then(() => {
          this.validationResult = 'VALID';
        })
        .catch(() => {
          this.validationResult = 'INVALID';
        });
    }
  }

  @tracked phoneNumber?: string;

  phoneNumberChanged = (newPhone: string | undefined) => {
    this.phoneNumber = newPhone;
  };
}
