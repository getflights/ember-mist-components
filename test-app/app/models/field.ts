import Model, { attr } from '@ember-data/model';
import modelListView from '@getflights/ember-mist-components/decorators/model-list-view';

@modelListView('default', {
  columns: ['label'],
  rows: 10,
  sortOrder: {
    field: 'created',
    dir: 'DESC',
  },
})
export default class FieldModel extends Model {
  @attr()
  declare selectOptions?: any;
}
