import Model, { attr } from '@ember-data/model';
import field from '@getflights/ember-field-components/decorators/field';
import select from '@getflights/ember-mist-components/fields/select';

export default class DummyModel extends Model {
  @attr()
  @field(select())
  declare select?: string;
}

declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    dummy: DummyModel;
  }
}
