import field from '@getflights/ember-field-components/decorators/field';
import MistModel from './mist-model.ts';
import { attr } from '@ember-data/model';
import datetime from '../fields/datetime.ts';

// @ts-ignore
export default abstract class DrupalModel extends MistModel {
  @attr('datetime')
  @field(datetime())
  created?: Date;

  @attr('datetime')
  @field(datetime())
  changed?: Date;
}
