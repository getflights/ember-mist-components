/* eslint-disable ember/classic-decorator-hooks */
/* eslint-disable ember/classic-decorator-no-classic-methods */
/* eslint-disable ember/no-observers */
import ValidationModel from '@getflights/ember-attribute-validations/models/validation-model';
import { inject as service } from '@ember/service';
import type { Promise } from 'rsvp';
import type { ChangedAttributes } from 'ember-data';
import type ModelChangeTrackerService from '../services/model-change-tracker.ts';
import { or } from '@ember/object/computed';
import type { SyncHasMany } from '@ember-data/model';

// @important Change Tracking hasMany is tricky
// Default it is disabled, so you have to explicitly turn on tracking for hasMany, but keep in mind the following gotchas
// - If you have a multi-entity-reference field on an entity, that field is a hasMany relationship in Ember
//   because it is not tracked by default, if you navigate away from an edit, any dirty changes on that field will not be rolled back
// - If you enable hasMany tracking, all hasMany relationships will be tracked on that model, and will thus be rolled back if you navigate away
//   this also means that if for example you have a modelTableRelated, on a object, and you click on the "new" button, because you navigate to
//   the new edit page, the hasMany of that modelTableRelated will also be rolled back, and the new relationship will not be filled in,
// - There is another final option in changeTracker, "only", with only you must explicitly define which relationships to track,
//   that way you can define which hasMany relationship to keep track of, but very important, you must also include all the belongsTo relationships
//   on that object, because belongsTo is tracked by default, but when you use "only", it only tracks the belongsTo relationships defined in "only"
export default abstract class ChangeTrackerModel extends ValidationModel {
  @service declare modelChangeTracker: ModelChangeTrackerService;

  // These declares have no effect on the JS, but are for typing
  // Set by Change Tracker Service on initialization
  @or('hasDirtyAttributes', 'hasDirtyRelations')
  declare isDirty: boolean;
  // declare hasDirtyAttributes: ComputedProperty<boolean, boolean>;
  declare hasDirtyRelations: boolean;

  init() {
    // @ts-ignore
    // eslint-disable-next-line prefer-rest-params
    super.init(...arguments);

    if (this.modelChangeTracker.isAutoSaveEnabled(this)) {
      this.initTracking();
    }

    this.modelChangeTracker.initializeDirtiness(this);

    this.setupTrackerMetaData();
    this.setupUnknownRelationshipLoadObservers();
  }

  /**
   * Did an attribute/association change?
   *
   * @param key the attribute/association name
   * @param changed optional ember-data changedAttribute object
   * @returns true if value changed
   */
  didChange(
    key: string,
    changed: ChangedAttributes | null = null,
    options?: any,
  ): boolean {
    return this.modelChangeTracker.didChange(this, key, changed, options);
  }

  /**
   * Did any attribute/association change?
   *
   * returns object with:
   *  {key: value} = {attribute: [old, new]}
   *
   * If the the attribute changed, it will be included in this object
   */
  changes(): { [key: string]: [any, any] } | ChangedAttributes {
    const changed = Object.assign({}, this.changedAttributes());
    const trackerInfo = this.modelChangeTracker.metaInfo(this);

    for (const key in trackerInfo) {
      if (
        !changed[key] &&
        Object.prototype.hasOwnProperty.call(trackerInfo, key)
      ) {
        if (this.didChange(key, changed)) {
          changed[key] = this.getRelationshipValues(key);
        }
      }
    }

    return changed;
  }

  private getRelationshipValues(key: string): [any, any] {
    type RelationObject = { id: string; type: string };

    // A polymorphic hasMany is not possible (yet?), so no RelationObject[]
    const lastValue = this.modelChangeTracker.lastValue(this, key) as
      | string
      | string[]
      | RelationObject; /* | RelationObject[]*/

    const modelFromId = (
      idOrObject: string | RelationObject | null | undefined,
    ) => {
      if (!idOrObject) {
        return undefined;
      }

      // @ts-ignore
      const modelClass = this.store.modelFor(this.constructor.modelName);

      if (modelClass && modelClass.relationshipsByName) {
        const relationship = modelClass.relationshipsByName.get(key);

        if (relationship) {
          const id =
            typeof idOrObject === 'string' ? idOrObject : idOrObject.id;
          const type =
            typeof idOrObject === 'string'
              ? relationship.type
              : idOrObject.type;
          return this.store.peekRecord(type, id);
        }
      }
    };

    if (Array.isArray(lastValue)) {
      // hasMany
      const lastValues = lastValue.map((value) => {
        return modelFromId(value);
      });
      return [lastValues, (this.get(<any>key) as SyncHasMany<any>).toArray()];
    } else {
      // belongsTo
      return [
        modelFromId(lastValue) ?? undefined,
        // @ts-ignore
        this.get(key),
      ];
    }
  }

  /**
   * Rollback all the changes on this model, for the keys you are
   * tracking.
   *
   * NOTE: Be sure you understand what keys you are tracking.
   * By default, tracker will save all keys, but if you set up
   * a model to 'only' track a limited set of keys, then the rollback
   * will only be limited to those keys
   *
   */
  rollback(): void {
    const isNew = this.isNew as unknown as boolean;
    this.rollbackAttributes();
    if (isNew) {
      return;
    }
    const trackerInfo = this.modelChangeTracker.metaInfo(this);
    this.modelChangeTracker.rollbackHasMany(this, trackerInfo);
    const rollbackData = this.modelChangeTracker.rollbackData(
      this,
      trackerInfo,
    );
    const normalized = this.modelChangeTracker.normalize(this, rollbackData);
    this.store.push(normalized);
  }

  // alias for saveChanges method
  startTrack(): void {
    this.initTracking();
    this.saveChanges();
  }

  // Ember Data DS.Model events
  // http://api.emberjs.com/ember-data/3.10/classes/DS.Model/events
  //
  // Replaces deprecated Ember.Evented usage:
  // https://github.com/emberjs/rfcs/blob/master/text/0329-deprecated-ember-evented-in-ember-data.md
  // Related: https://github.com/emberjs/rfcs/pull/329

  onIsNewChanged() {
    if ((this.isNew as unknown as boolean) === false) {
      // this.modelChangeTracker.saveChanges(this);
      this.saveOnCreate();
      this.removeObserver('isNew', this.onIsNewChanged);
    }
  }

  onIsDeletedChanged(): void {
    if ((this.isDeleted as unknown as boolean) === true) {
      this.clearSavedAttributes();
      this.removeObserver('isDeleted', this.onIsDeletedChanged);
    }
  }

  // onIsLoadedChanged() {
  //   this.setupTrackerMetaData();
  //   this.setupUnknownRelationshipLoadObservers();
  //   this.removeObserver('isLoaded', this.onIsLoadedChanged);
  // }

  initTracking(): void {
    // sync tracker with model on events like create/update/delete/load
    if (this.isNew as unknown as boolean) {
      this.addObserver('isNew', this.onIsNewChanged);
    }
    this.addObserver('isDeleted', this.onIsDeletedChanged);

    // if (!this.isLoaded) {
    //   this.addObserver('isLoaded', this, this.onIsLoadedChanged);
    // }

    // there is no didUpdate hook anymore and no appropriate model props to base on
    // saveOnUpdate should be called after model has been saved
    // right after model
    this.modelChangeTracker.setupTracking(this);
  }

  save(...args: any[]) {
    return super.save(...args).then((result) => {
      this.saveOnUpdate();
      return result;
    });
  }

  /**
   * Save the current state of the model
   *
   * NOTE: This is needed when manually pushing data
   * to the store and using Ember < 2.10
   *
   * options like => {except: 'company'}
   *
   * @param {Object} options
   */
  saveChanges(options?: any): void {
    this.modelChangeTracker.setupTracking(this);
    this.modelChangeTracker.saveChanges(this, options);
    this.modelChangeTracker.triggerIsDirtyReset(this);
  }

  saveTrackerChanges(options?: any): void {
    this.saveChanges(options);
  }

  /**
   * Get value of the last known value tracker is saving for this key
   *
   * @param key attribute/association name
   * @returns {*}
   */
  savedTrackerValue(key: string): any {
    return this.modelChangeTracker.lastValue(this, key);
  }

  // save state when model is loaded or created if using auto save
  setupTrackerMetaData(): void {
    // this is experimental
    this.modelChangeTracker.initializeDirtiness(this);

    if (this.modelChangeTracker.isAutoSaveEnabled(this)) {
      this.saveChanges();
    }
  }

  // watch for relationships loaded with data via links
  setupUnknownRelationshipLoadObservers(): void {
    // @ts-ignore
    this.eachRelationship((key) => {
      // @ts-ignore
      this.addObserver(key, this, 'observeUnknownRelationshipLoaded');
    });
  }

  saveOnUpdate() {
    // if (this.modelChangeTracker.isAutoSaveEnabled(this) || this.modelChangeTracker.isIsDirtyEnabled(this)) {
    this.saveChanges();
    // }
  }

  saveOnCreate() {
    // if (this.modelChangeTracker.isAutoSaveEnabled(this) || this.modelChangeTracker.isIsDirtyEnabled(this)) {
    this.saveChanges();
    // }
  }

  // There is no didReload callback on models, so have to override reload
  reload(...args: any): Promise<this> {
    const promise = super.reload(...args);
    promise.then(() => {
      if (this.modelChangeTracker.isAutoSaveEnabled(this)) {
        this.saveChanges();
      }
    });
    return promise;
  }

  // when model deletes, remove any tracked state
  clearSavedAttributes(): void {
    this.modelChangeTracker.clear(this);
  }

  observeUnknownRelationshipLoaded(_: any, key: string /*, value, rev*/): void {
    if (
      this.modelChangeTracker.trackingIsSetup(this) &&
      this.modelChangeTracker.isTracking(this, key)
    ) {
      const saved = this.modelChangeTracker.saveLoadedRelationship(this, key);
      if (saved) {
        // @ts-ignore
        this.removeObserver(key, this, 'observeUnknownRelationshipLoaded');
      }
    }
  }
}
