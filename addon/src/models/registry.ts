import type AddressFormat from './address-format';
import type UserModel from './user';

export default interface MistModelRegistry {
  'address-format': AddressFormat;
  user: UserModel;
}
