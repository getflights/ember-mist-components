import Model, { attr } from '@ember-data/model';
import type Address from '../interfaces/address';
import { isBlank } from '@ember/utils';
import { action, get } from '@ember/object';
import type { FieldOfAddress } from '../interfaces/address';

export const IGNORED_ADDRESS_FIELDS = [
  'organization',
  'givenName',
  'additionalName',
  'familyName',
];

export default class AddressFormat extends Model {
  @attr
  declare countryCode: string; // = id

  @attr
  declare format: string;

  // @attr
  // declare localFormat

  @attr
  declare usedFields: FieldOfAddress[];

  // declare usedSubdivisionFields: string[];

  @attr
  declare requiredFields: string[];

  // declare uppercaseFields: string[];

  // declare administrativeAreaType: any;

  // declare localityType: any;

  // declare dependentLocalityType: any;

  // declare postalcodeType: any;

  // declare postalcodePattern: any;

  // declare postalcodePrefix: any;

  @attr
  declare subdivisionDepth: number;

  @attr
  declare labels: {
    [key: string]: string | null;
    // "givenName": "First name",
    // "additionalName": "Middle name",
    // "familyName": "Last name",
    // "organization": "Company",
    // "addressLine1": "Street address",
    // "addressLine2": "Street address line 2",
    // "postalCode": null,
    // "sortingCode": "Cedex",
    // "dependentLocality": null,
    // "locality": "City",
    // "administrativeArea": null
  };

  @action
  validate(value: Address) {
    const requiredFields = this.requiredFields.filter(
      (field) => !IGNORED_ADDRESS_FIELDS.includes(field),
    );

    const someFieldMissing = requiredFields.some((requiredField: string) => {
      return isBlank(get(value, requiredField));
    });

    return !someFieldMissing;
  }
}
