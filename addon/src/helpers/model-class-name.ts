import Helper from '@ember/component/helper';
import { service } from '@ember/service';
import type ModelInformationService from '../services/model-information.ts';
import type MistModel from '../models/mist-model.ts';

export default class ModelClassNameHelper extends Helper {
  @service declare modelInformation: ModelInformationService;

  compute([model]: [MistModel]) {
    return this.modelInformation.getModelName(model);
  }
}
