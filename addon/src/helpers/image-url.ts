import { helper } from '@ember/component/helper';
import { htmlSafe, type SafeString } from '@ember/template';

interface ImageLike {
  url: string;
}

export function imageUrl([value, style]: [
  value: ImageLike | undefined,
  style?: string,
]): SafeString | undefined {
  if (!value) {
    return;
  }

  if (!value.url) {
    return;
  }

  const url = new URL(value.url);

  if (style) {
    url.searchParams.set('style', style);
  }

  return htmlSafe(url.toString());
}

export default helper(imageUrl);
