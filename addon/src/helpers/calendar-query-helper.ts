import Helper from '@ember/component/helper';
import { service } from '@ember/service';
import { dasherize } from '@ember/string';
import type ModelInformationService from '../services/model-information.ts';
import Condition from '../query/Condition.ts';
import Query from '../query/Query.ts';
import { tracked } from 'tracked-built-ins';
import {
  endOfDay,
  endOfISOWeek,
  endOfMonth,
  format,
  isSameMonth,
  startOfDay,
  startOfISOWeek,
  startOfMonth,
} from 'date-fns';
import type { ModelKey } from '../components/-base-collection';
import type ModelRegistry from 'ember-data/types/registries/model';

type Positional<K extends ModelKey, Q extends Query<K>> = [
  query: Q,
  modelName: K,
  dateField: keyof ModelRegistry[K] & string,
];

type Return<Q extends Query> = { query: Q; setCenter: (center: Date) => void };

interface CalendarQuerySignature<K extends ModelKey, Q extends Query<K>> {
  Args: {
    Positional: Positional<K, Q>;
  };
  Return: Return<Query<K>>;
}

/**
 * Create a copy of a query and add two conditions, so that it only returns the results from the current month.
 * Returns an object with the updated query + a setCenter function to update the month selection.
 */
export default class CalendarQueryHelper<
  K extends ModelKey,
  Q extends Query<K>,
> extends Helper<CalendarQuerySignature<K, Q>> {
  @service declare modelInformation: ModelInformationService;

  @tracked center = new Date();

  setCenter = (center: Date) => {
    if (!isSameMonth(center, this.center)) {
      this.center = center;
    }
  };

  appliedConditions: Condition[] = [];

  compute([originalQuery, modelName, dateField]: Positional<K, Q>): Return<
    Query<K>
  > {
    const query = new Query(modelName);
    query.copyFrom(originalQuery);

    const transformType = this.modelInformation.getTransformType(
      modelName,
      dateField,
    );

    const firstVisibleDay = startOfISOWeek(
      startOfMonth(startOfDay(this.center)),
    );
    const lastVisibleDay = endOfISOWeek(endOfMonth(endOfDay(this.center)));

    const dateFormat =
      transformType === 'datetime' ? "yyyy-MM-dd'T'HH:mm:ss" : 'yyyy-MM-dd';

    this.appliedConditions.forEach((condition) => {
      query.removeCondition(condition);
    });

    const afterCondition = new Condition(
      dasherize(dateField),
      '>=',
      format(firstVisibleDay, dateFormat),
    );

    const beforeCondition = new Condition(
      dasherize(dateField),
      '<=',
      format(lastVisibleDay, dateFormat),
    );

    query.addCondition(afterCondition);
    query.addCondition(beforeCondition);

    this.appliedConditions = [beforeCondition, afterCondition];

    return tracked({ query, setCenter: this.setCenter });
  }
}
