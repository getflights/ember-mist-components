import Helper from '@ember/component/helper';
import Model from '@ember-data/model';
import { isBlank } from '@ember/utils';

type Positional = [Model, string];

type Return = string | undefined;

export interface PDFURLHelperSignature {
  Args: {
    Positional: Positional;
  };
  Return: Return;
}

export default class BelongsToHelper extends Helper {
  compute([model, belongsToRelationshipName]: Positional): Return {
    if (isBlank(model)) {
      return undefined;
    }

    // @ts-ignore
    return model.belongsTo(belongsToRelationshipName).id();
  }
}
