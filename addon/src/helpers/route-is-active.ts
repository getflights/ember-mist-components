import Helper from '@ember/component/helper';
import { service } from '@ember/service';
import type RouterService from '@ember/routing/router-service';

type Positional = [string | string[]];
type Named = { model?: string };

type Return = boolean;

export interface PDFURLHelperSignature {
  Args: {
    Positional: Positional;
  };
  Return: Return;
}

export default class RouteIsActiveHelper extends Helper {
  @service declare router: RouterService;

  compute([routeNames]: Positional, { model }: Named): Return {
    if (!Array.isArray(routeNames)) {
      routeNames = [routeNames];
    }

    return routeNames.some((route) => {
      if (model) {
        return this.router.isActive(route, model);
      }

      return this.router.isActive(route);
    });
  }
}
