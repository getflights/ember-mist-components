import type ListViewInterface from '../interfaces/list-view.ts';
import type { ListViewWithoutModel } from '../interfaces/list-view.ts';

/**
 * Add a new list view to the model
 * @param key Key of the list view
 * @param listView The list view
 */
export default function modelListView(
  key: string,
  listView: ListViewWithoutModel,
): ClassDecorator {
  return (target: any) => {
    if (!('settings' in target)) {
      target['settings'] = {};
    }

    if (!('listViews' in target.settings)) {
      target['settings']['listViews'] = {};
    }

    target['settings']['listViews'][key] = {
      model: target.constructor.modelName,
      ...listView,
    } as ListViewInterface;
  };
}
