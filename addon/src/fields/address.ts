import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import InputFieldAddressComponent from '../components/input-field/address.gts';
import OutputFieldAddressComponent from '../components/output-field/address.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';

type FieldAddressOptions = BaseInputFieldOptions;

const address = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldAddressOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
  };

  return {
    type: 'address',
    inputField: InputFieldAddressComponent,
    inputFieldOptions,
    outputField: OutputFieldAddressComponent,
    outputFieldOptions,
  };
};

export default address;
