import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import InputFieldTextComponent from '@getflights/ember-field-components/components/input-field/text';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import type { OutputTextMaskOptions } from '../components/output/text-mask.gts';
import OutputFieldTextMaskComponent from '../components/output-field/text-mask.gts';

type FieldTextMaskOptions = BaseInputFieldOptions & {
  mask: string;
  regex: RegExp;
};

const textMask = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldTextMaskOptions,
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions: BaseOutputFieldOptions<OutputTextMaskOptions> = {
    ...baseOutputFieldOptions,
    outputOptions: {
      ...baseOutputFieldOptions.outputOptions,
      mask: options.mask,
      regex: options.regex,
    },
  };

  return {
    type: 'text-mask',
    inputField: InputFieldTextComponent,
    inputFieldOptions,
    // @ts-ignore
    outputField: OutputFieldTextMaskComponent,
    outputFieldOptions,
  };
};

export default textMask;
