import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import InputFieldPhoneComponent from '../components/input-field/phone.gts';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputFieldPhoneComponent from '../components/output-field/phone.gts';

type FieldPhoneOptions = BaseInputFieldOptions;

const phone = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldPhoneOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
  };

  return {
    type: 'phone',
    inputField: InputFieldPhoneComponent,
    inputFieldOptions,
    outputField: OutputFieldPhoneComponent,
    outputFieldOptions,
  };
};

export default phone;
