import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import InputFieldImagesComponent from '../components/input-field/images.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputFieldImagesComponent from '../components/output-field/images.gts';

type FieldImagesOptions = BaseInputFieldOptions & {
  endpoint?: string;
  style?: string;
};

const images = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldImagesOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
    inputOptions: {
      ...baseInputFieldOptions,
      endpoint: options.endpoint,
    },
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
    outputOptions: {
      ...baseOutputFieldOptions,
      style: options.style,
    },
  };

  return {
    type: 'images',
    inputField: InputFieldImagesComponent,
    inputFieldOptions,
    outputField: OutputFieldImagesComponent,
    outputFieldOptions,
  };
};

export default images;
