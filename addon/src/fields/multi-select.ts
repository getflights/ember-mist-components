import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';
import type SelectOptionGroup from '@getflights/ember-field-components/interfaces/select-option-group';
import InputFieldMultiSelectComponent from '../components/input-field/multi-select.gts';
import OutputFieldMultiSelectComponent from '../components/output-field/multi-select.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';

type FieldMultiSelectOptions = BaseInputFieldOptions & {
  selectOptions?: (SelectOption | SelectOptionGroup)[];
};

const multiSelect = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldMultiSelectOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
    inputOptions: {
      ...baseInputFieldOptions.inputOptions,
      selectOptions: options.selectOptions,
    },
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
  };

  return {
    type: 'multi-select',
    // @ts-ignore
    inputField: InputFieldMultiSelectComponent,
    inputFieldOptions,
    // @ts-ignore
    outputField: OutputFieldMultiSelectComponent,
    outputFieldOptions,
  };
};

export default multiSelect;
