import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import InputFieldStringsComponent from '../components/input-field/strings.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputFieldStringsComponent from '../components/output-field/strings.gts';

type FieldStringsOptions = BaseInputFieldOptions;

const strings = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldStringsOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
  };

  return {
    type: 'strings',
    inputField: InputFieldStringsComponent,
    inputFieldOptions,
    outputField: OutputFieldStringsComponent,
    outputFieldOptions,
  };
};

export default strings;
