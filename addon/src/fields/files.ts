import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import InputFieldFilesComponent from '../components/input-field/files.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputFieldFilesComponent from '../components/output-field/files.gts';

type FieldFilesOptions = BaseInputFieldOptions & {
  endpoint?: string;
};

const files = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldFilesOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
    inputOptions: {
      ...baseInputFieldOptions,
      endpoint: options.endpoint,
    },
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
  };

  return {
    type: 'files',
    // @ts-ignore
    inputField: InputFieldFilesComponent,
    inputFieldOptions,
    outputField: OutputFieldFilesComponent,
    outputFieldOptions,
  };
};

export default files;
