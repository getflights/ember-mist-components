import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import InputFieldImageComponent from '../components/input-field/image.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputFieldImageComponent from '../components/output-field/image.gts';

type FieldImageOptions = BaseInputFieldOptions & {
  endpoint?: string;
  style?: string;
};

const image = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldImageOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions: BaseInputFieldOptions = {
    ...baseInputFieldOptions,
    inputOptions: {
      ...baseInputFieldOptions,
      endpoint: options.endpoint,
    },
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
    outputOptions: {
      ...baseOutputFieldOptions,
      style: options.style,
    },
  };

  return {
    type: 'image',
    // @ts-ignore
    inputField: InputFieldImageComponent,
    inputFieldOptions,
    outputField: OutputFieldImageComponent,
    outputFieldOptions,
  };
};

export default image;
