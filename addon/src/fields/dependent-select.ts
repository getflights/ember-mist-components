import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';
import type SelectOptionGroup from '@getflights/ember-field-components/interfaces/select-option-group';
import InputFieldDependentSelectComponent, {
  type InputDependentSelectOptions,
} from '../components/input-field/dependent-select.gts';
import OutputFieldSelectComponent from '../components/output-field/select.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';

type FieldDependentSelectOptions = BaseInputFieldOptions & {
  selectOptions: (SelectOption | SelectOptionGroup)[];
  dependentField: string;
  dependencies: Map<string, string[]>;
};

const dependentSelect = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldDependentSelectOptions,
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions: BaseInputFieldOptions<InputDependentSelectOptions> =
    {
      // Base input field options
      ...baseInputFieldOptions,
      // Dependent select options
      inputOptions: {
        ...baseInputFieldOptions.inputOptions,
        dependentField: options.dependentField,
        dependencies: options.dependencies,
        selectOptions: options.selectOptions,
      },
    };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
    outputOptions: {
      ...baseOutputFieldOptions.outputOptions,
      selectOptions: options.selectOptions,
    },
  };

  return {
    type: 'dependent-select',
    // @ts-ignore
    inputField: InputFieldDependentSelectComponent,
    inputFieldOptions,
    // @ts-ignore
    outputField: OutputFieldSelectComponent,
    outputFieldOptions,
  };
};

export default dependentSelect;
