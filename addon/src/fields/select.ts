import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';
import type SelectOptionGroup from '@getflights/ember-field-components/interfaces/select-option-group';
import InputFieldSelectComponent from '../components/input-field/select.gts';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputFieldSelectComponent from '../components/output-field/select.gts';

type FieldSelectOptions = BaseInputFieldOptions & {
  selectOptions?: (SelectOption | SelectOptionGroup)[];
};

const select = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldSelectOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
    inputOptions: {
      ...baseInputFieldOptions.inputOptions,
      selectOptions: options.selectOptions,
    },
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
    outputOptions: {
      ...baseInputFieldOptions.inputOptions,
      selectOptions: options.selectOptions,
    },
  };

  return {
    type: 'select',
    // @ts-ignore
    inputField: InputFieldSelectComponent,
    inputFieldOptions,
    // @ts-ignore
    outputField: OutputFieldSelectComponent,
    outputFieldOptions,
  };
};

export default select;
