import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import {
  filterBaseInputFieldOptions,
  type BaseInputFieldOptions,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldDecoratorOptions } from '@getflights/ember-field-components/decorators/field';
import InputFieldUrlComponent from '@getflights/ember-field-components/components/input-field/url';
import {
  filterBaseOutputFieldOptions,
  type BaseOutputFieldOptions,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputFieldUrlComponent from '../components/output-field/url.gts';

type FieldUrlOptions = BaseInputFieldOptions;

const url = <O extends SomeModel, F extends FieldOf<O>>(
  options: FieldUrlOptions = {},
): FieldDecoratorOptions<O, F> => {
  const baseInputFieldOptions: BaseInputFieldOptions =
    filterBaseInputFieldOptions(options);

  const inputFieldOptions = {
    ...baseInputFieldOptions,
  };

  const baseOutputFieldOptions: BaseOutputFieldOptions =
    filterBaseOutputFieldOptions(options);

  const outputFieldOptions = {
    ...baseOutputFieldOptions,
  };

  return {
    type: 'url',
    inputField: InputFieldUrlComponent,
    inputFieldOptions,
    outputField: OutputFieldUrlComponent,
    outputFieldOptions,
  };
};

export default url;
