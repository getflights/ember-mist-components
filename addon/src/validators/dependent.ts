import type ValidatorInterface from '@getflights/ember-attribute-validations/interfaces/validator';
import { isPresent } from '@ember/utils';
import { assert } from '@ember/debug';
import { hasValue } from '@getflights/ember-attribute-validations/utils';
import fail from '@getflights/ember-attribute-validations/-private/fail';
import getNested, { type NestedKey } from '../utils/get-nested.ts';

export interface DependentValidatorOptions<
  O extends object,
  DF extends keyof O | NestedKey<O>,
> {
  dependentField: DF;
  dependencies: Map<string, string[]>;
}

/**
 * Check whether the value has a value (if another value is truthy)
 */
export default class DependentValidator<
  O extends object,
  DF extends keyof O | NestedKey<O>,
> implements ValidatorInterface
{
  readonly name = 'dependent';

  constructor(propertyKey: any, options: DependentValidatorOptions<O, DF>) {
    this.propertyKey = propertyKey;

    assert(
      'You must define a `dependentField` for DependentValidator',
      isPresent(options?.dependentField),
    );
    this.dependentField = options.dependentField!;

    assert(
      'You must define a map of `dependencies` for DependentValidator',
      isPresent(options?.dependencies),
    );

    assert(
      'Invalid map of dependencies, provided to DependentValidator',
      options.dependencies instanceof Map,
    );
    this.dependencies = options.dependencies!;
  }

  readonly propertyKey;

  private dependentField: DF;
  private dependencies: Map<string, string[]>;

  async validate(value: any, target: O): Promise<true> {
    if (hasValue(value)) {
      const dependentFieldValue = getNested(
        target,
        this.dependentField,
      ) as unknown as string;
      const possibleValues = this.dependencies.get(dependentFieldValue) ?? [];

      if (!possibleValues.includes(value)) {
        return Promise.reject(fail.call(this));
      }
    }

    return true;
  }
}
