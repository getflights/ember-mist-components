import type ValidatorInterface from '@getflights/ember-attribute-validations/interfaces/validator';
import { service } from '@ember/service';
import { hasValue } from '@getflights/ember-attribute-validations/utils';
import PhoneIntlService from '../services/phone-intl.ts';
import fail from '@getflights/ember-attribute-validations/-private/fail';

export default class PhoneValidator implements ValidatorInterface {
  readonly name = 'phone';

  @service declare phoneIntl: PhoneIntlService;

  constructor(propertyKey: any) {
    this.propertyKey = propertyKey;

    // const owner = getOwner(this);
    // assert('PhoneValidator needs to be initialized with an owner.', isPresent(owner));
  }

  readonly propertyKey;

  async validate(value: any, _model: object): Promise<true> {
    if (hasValue(value) && !(await this.phoneIntl.isValidNumber(value))) {
      return Promise.reject(fail.call(this));
    }

    return true;
  }
}
