import type ValidatorInterface from '@getflights/ember-attribute-validations/interfaces/validator';
import { service } from '@ember/service';
import { hasValue } from '@getflights/ember-attribute-validations/utils';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';
import AddressService from '../services/address.ts';
import fail from '@getflights/ember-attribute-validations/-private/fail';

export default class CountryValidator implements ValidatorInterface {
  readonly name = 'country';

  @service declare address: AddressService;

  constructor(propertyKey: any) {
    this.propertyKey = propertyKey;
  }

  readonly propertyKey;

  async validate(value: any, _model: object): Promise<true> {
    // Early return, no unnecessary requests for country select options.
    if (!hasValue(value)) {
      return true;
    }

    const selectOptions =
      await this.address.countrySelectOptionsRequest.promise;

    if (
      !selectOptions?.some(
        (selectOption: SelectOption) => value === selectOption.value,
      )
    ) {
      return Promise.reject(fail.call(this));
    }

    return true;
  }
}
