import type ValidatorInterface from '@getflights/ember-attribute-validations/interfaces/validator';
import { service } from '@ember/service';
import type AddressService from '../services/address.ts';
import { isAddress } from '../interfaces/address.ts';
// import { assert } from '@ember/debug';
import fail from '@getflights/ember-attribute-validations/-private/fail';

export default class AddressValidator implements ValidatorInterface {
  readonly name = 'address';

  @service declare address: AddressService;

  constructor(propertyKey: any) {
    this.propertyKey = propertyKey;
  }

  readonly propertyKey;

  async validate(value: any | undefined, _model: object): Promise<true> {
    // assert(
    //   'The value to validate must be an address, other types not allowed',
    //   value instanceof Address || isBlank(value)
    // );

    if (value) {
      // Not an address and not blank: not a valid address
      if (!isAddress(value)) {
        return Promise.reject(fail.call(this));
      }

      const format = await this.address.loadAddressFormat(value.countryCode);

      if (!format || !format.validate(value)) {
        return Promise.reject(fail.call(this));
      }
    }

    return true;
  }
}
