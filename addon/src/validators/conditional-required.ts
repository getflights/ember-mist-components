import RequiredValidator from '@getflights/ember-attribute-validations/validators/required';
import { isPresent } from '@ember/utils';
import { assert } from '@ember/debug';
import getNested, { type NestedKey } from '../utils/get-nested.ts';

function isAssertionFn<O extends object>(
  assertionFnOrField: AssertionFn<O> | keyof O | NestedKey<O>,
): assertionFnOrField is AssertionFn<O> {
  return typeof assertionFnOrField === 'function';
}

export type AssertionFn<O> = (target: O) => boolean;

export interface ConditionalRequiredValidatorOptions<O extends object> {
  assertionOrField: AssertionFn<O> | keyof O | NestedKey<O>;
}

/**
 * Check whether the value has a value (if another value is truthy)
 */
export default class ConditionalRequiredValidator<
  O extends object,
> extends RequiredValidator {
  readonly name = 'conditionalRequired';

  constructor(
    propertyKey: any,
    options: ConditionalRequiredValidatorOptions<O>,
  ) {
    super(propertyKey);

    assert(
      'You must define a `conditionalField` for ConditionalRequiredValidator',
      isPresent(options?.assertionOrField),
    );
    this.assertionOrField = options!.assertionOrField!;
  }

  private assertionOrField: AssertionFn<O> | keyof O | NestedKey<O>;

  async validate(value: any, target: O): Promise<true> {
    if (isAssertionFn(this.assertionOrField)) {
      // if this assertion is truthy
      if (this.assertionOrField(target)) {
        // Run the required validator
        return await super
          .validate(value, target)
          .catch((e) => Promise.reject(e));
      }
    } else {
      const conditionalFieldValue = getNested(target, this.assertionOrField);

      // if this required field has a value
      if (conditionalFieldValue) {
        // Run the required validator
        return await super
          .validate(value, target)
          .catch((e) => Promise.reject(e));
      }
    }

    return true;
  }
}
