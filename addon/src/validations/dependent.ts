import type { ValidationFunction } from '@getflights/ember-attribute-validations/decorators/validation';
import DependentValidator from '../validators/dependent.ts';
import type { NestedKey } from '../utils/get-nested.ts';

export function dependent<
  O extends object,
  F extends keyof O,
  DF extends keyof O | NestedKey<O>,
>(
  dependentField: DF,
  dependencies: Map<string, string[]>,
): ValidationFunction<O, F> {
  return (propertyKey: F, target: O) => {
    // @ts-ignore
    const value = target[propertyKey];

    const validator = new DependentValidator<O, DF>(propertyKey, {
      dependentField,
      dependencies,
    });
    return validator.validate(value, target);
  };
}
