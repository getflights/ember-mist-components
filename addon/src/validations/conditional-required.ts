import type { ValidationFunction } from '@getflights/ember-attribute-validations/decorators/validation';
import ConditionalRequiredValidator, {
  type AssertionFn,
} from '../validators/conditional-required.ts';
import type { NestedKey } from '../utils/get-nested.ts';
import type Model from '@ember-data/model';
import { assert } from '@ember/debug';

function isModel(o: object): o is Model {
  return 'belongsTo' in o;
}

function conditionalRequired<O extends object, F extends keyof O>(
  assertionOrField: AssertionFn<NoInfer<O>> | keyof O | NestedKey<O>,
  relationshipKind?: O extends Model ? 'belongsTo' : never,
): ValidationFunction<O, F> {
  return (propertyKey: F, target: O) => {
    let value;
    if (relationshipKind === 'belongsTo') {
      assert(
        `You can only check conditionalRequired of type "belongsTo" on a Model: belongsTo method does not exist on target.`,
        isModel(target),
      );
      value = target
        .belongsTo(
          // @ts-ignore
          propertyKey,
        )
        .id();
    } else {
      value = target[propertyKey];
    }

    const validator = new ConditionalRequiredValidator<O>(propertyKey, {
      assertionOrField,
    });
    return validator.validate(value, target);
  };
}

export { conditionalRequired };
