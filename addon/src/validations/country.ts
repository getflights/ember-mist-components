import { getOwner, setOwner } from '@ember/application';
import type { ValidationFunction } from '@getflights/ember-attribute-validations/decorators/validation';
import CountryValidator from '../validators/country.ts';

export function country<
  O extends object,
  F extends keyof O,
>(): ValidationFunction<O, F> {
  return (propertyKey: F, target: O) => {
    const value = target[propertyKey];

    const owner = getOwner(target);
    const validator = new CountryValidator(propertyKey);
    // @ts-ignore
    setOwner(validator, owner);
    return validator.validate(value, target);
  };
}
