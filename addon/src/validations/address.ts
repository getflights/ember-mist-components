import type { ValidationFunction } from '@getflights/ember-attribute-validations/decorators/validation';
import AddressValidator from '../validators/address.ts';
import { getOwner, setOwner } from '@ember/application';

export function address<
  O extends object,
  F extends keyof O,
>(): ValidationFunction<O, F> {
  return (propertyKey: F, target: O) => {
    const value = target[propertyKey];

    const owner = getOwner(target);
    const validator = new AddressValidator(propertyKey);
    // @ts-ignore
    setOwner(validator, owner);
    return validator.validate(value, target);
  };
}
