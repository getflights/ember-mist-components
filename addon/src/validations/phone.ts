import { getOwner, setOwner } from '@ember/application';
import type { ValidationFunction } from '@getflights/ember-attribute-validations/decorators/validation';
import PhoneValidator from '../validators/phone.ts';

export function phone<
  O extends object,
  F extends keyof O,
>(): ValidationFunction<O, F> {
  return (propertyKey: F, target: O) => {
    const value = target[propertyKey];

    const owner = getOwner(target);
    const validator = new PhoneValidator(propertyKey);
    // @ts-ignore
    setOwner(validator, owner);
    return validator.validate(value, target);
  };
}
