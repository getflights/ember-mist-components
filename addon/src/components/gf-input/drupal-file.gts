import { task } from 'ember-concurrency';
import { service } from '@ember/service';
import { dasherize } from '@ember/string';
import { htmlSafe } from '@ember/template';
import type DrupalFile from '../../interfaces/drupal-file';
import type HttpService from '../../services/http';
import type ToastService from '../../services/toast';
import { assert } from '@ember/debug';
import { cached } from '@glimmer/tracking';
import {
  type FileQueueService,
  FileSource,
  UploadFile,
} from 'ember-file-upload';
import type { BaseInputArguments } from '@getflights/ember-field-components/components/input/-base';
import Component from '@glimmer/component';
import LoadingComponent from '../loading.gts';
import t from 'ember-intl/helpers/t';
import { not, or } from 'ember-truth-helpers';
import GfInputFileComponent from '../gf-input/file.gts';
import { fn, hash } from '@ember/helper';
import { on } from '@ember/modifier';
import OutputDrupalFileComponent from '../output/drupal-file.gts';
import { guidFor } from '@ember/object/internals';
import { tracked } from 'tracked-built-ins';

export interface GfInputDrupalFileOptions<
  Multiple extends boolean | undefined,
> {
  field: string;
  modelName: string;
  multiple?: Multiple;
  headers?: { [key: string]: string };
  endpoint?: string;
  entity?: string;
}

export interface GfInputDrupalFileArguments<
  Multiple extends boolean | undefined = undefined,
> extends BaseInputArguments {
  value?: Multiple extends true ? DrupalFile[] : DrupalFile;
  valueChanged?: (
    newValue: Multiple extends true
      ? DrupalFile[] | undefined
      : DrupalFile | undefined,
  ) => void;
  options: GfInputDrupalFileOptions<Multiple>;
}

export interface GfInputDrupalFileSignature<
  Multiple extends boolean | undefined,
> {
  Args: GfInputDrupalFileArguments<Multiple>;
  Element: HTMLSpanElement;
}

export default class GfInputDrupalFileComponent<
  Multiple extends boolean | undefined,
> extends Component<GfInputDrupalFileSignature<Multiple>> {
  @service declare http: HttpService;
  @service declare toast: ToastService;
  @service declare fileQueue: FileQueueService;

  @tracked private activeFile?: UploadFile;

  constructor(owner: any, args: GfInputDrupalFileArguments<Multiple>) {
    super(owner, args);
    assert(
      "gf-input/drupal-file.options 'field' and 'modelName' are required",
      args.options && args.options.field && args.options.modelName,
    );

    this.guid = guidFor(this);
  }

  guid;

  get queue() {
    return this.fileQueue.findOrCreate(this.guid);
  }

  get totalFiles(): number {
    if (this.queue) {
      return <number>this.queue.files.length;
    }

    return 0;
  }

  get activeFilePositionInQueue(): number {
    if (this.activeFile && this.queue) {
      const position = this.queue.files.indexOf(this.activeFile);

      if (position !== -1) {
        return position + 1;
      }
    }

    return -1;
  }

  get mistFieldTarget(): string | undefined {
    if (!this.args.options.modelName || !this.args.options.field) {
      return undefined;
    }

    const target = [];
    if (this.args.options.entity) {
      target.push(dasherize(this.args.options.entity));
    }

    target.push(dasherize(this.args.options.modelName));
    target.push(dasherize(this.args.options.field));

    return target.join('.');
  }

  get fieldHeaders(): Map<string, string> {
    const returnValue = new Map();

    if (this.args.options && this.args.options.headers) {
      for (const key in this.args.options.headers) {
        returnValue.set(key, this.args.options.headers[key]);
      }
    }

    return returnValue;
  }

  get uploadEndpoint(): string {
    if (this.args.options && this.args.options.endpoint) {
      return `${this.http.endpoint}${this.args.options.endpoint}`;
    } else {
      return `${this.http.endpoint}file/files`;
    }
  }

  @cached
  get httpHeaders(): Map<string, string> {
    const returnValue = new Map();
    const httpHeaders = this.http.headers;

    if (httpHeaders) {
      for (const key in httpHeaders) {
        returnValue.set(key, httpHeaders[key]);
      }
    }

    return returnValue;
  }

  get headers(): { [s: string]: string } {
    const headers = new Map([...this.fieldHeaders, ...this.httpHeaders]);
    const mistFieldTarget = this.mistFieldTarget;

    if (mistFieldTarget) {
      headers.set('X-Mist-Field-Target', mistFieldTarget);
    }

    const returnValue: { [s: string]: string } = {};
    headers.forEach((value, key) => {
      returnValue[key] = value;
    });

    return returnValue;
  }

  uploadFile = task(
    { enqueue: true, maxConcurrency: 1 },
    async (file: UploadFile) => {
      this.activeFile = file;

      if (file) {
        await file
          .upload(this.uploadEndpoint, { headers: this.headers })
          .then((r: Response) => r.json())
          .then((response: any) => {
            const fileObject: DrupalFile = {
              id: response.data.id,
              filename: response.data.attributes.filename,
              uri: response.data.attributes.uri,
              url: response.data.attributes.url,
              filemime: response.data.attributes.filemime,
              filesize: response.data.attributes.filesize,
              hash: response.data.attributes.hash,
            };

            if (this.args.options.multiple) {
              const currentValue =
                (this.args.value as DrupalFile[] | undefined) ?? [];
              // @ts-expect-error typing of Multiple messes this up
              this.args.valueChanged?.([...currentValue, fileObject]);
            } else {
              // @ts-expect-error typing of Multiple messes this up
              this.args.valueChanged?.(fileObject);
            }
          })
          .catch((error: any) => {
            console.error(error);
            if (file.queue) {
              file.queue.remove(file);
            }

            let errorMessage = 'File upload failed';
            if (
              error?.body?.errors &&
              Array.isArray(error.body.errors) &&
              error.body.errors.length > 0
            ) {
              if ('detail' in error.body.errors[0]) {
                errorMessage = error.body.errors[0].detail;
              } else if ('code' in error.body.errors[0]) {
                errorMessage = error.body.errors[0].code;
              }
            }

            errorMessage = `(${htmlSafe(file.name)}) ${errorMessage}`;
            this.toast.error(errorMessage);
          });
      }
    },
  );

  deleteFile = (file: DrupalFile) => {
    if (this.args.options.multiple) {
      const values = this.args.value as DrupalFile[];
      const indexOfValue = values.indexOf(file);

      if (indexOfValue !== -1) {
        values.splice(indexOfValue, 1);

        if (values.length === 0) {
          this.args.valueChanged?.(undefined);
        } else {
          // @ts-expect-error typing of Multiple messes up this piece of code
          this.args.valueChanged?.([...values]);
        }
      }
    } else {
      this.args.valueChanged?.(undefined);
    }
  };

  fileSelected = (file: File | File[] | undefined) => {
    if (!file) {
      return;
    }

    const files = Array.isArray(file) ? file : [file];

    files
      .map((file) => {
        const uploadFile = new UploadFile(file, FileSource.Browse);
        this.queue.add(uploadFile);
        return uploadFile;
      })
      .forEach(this.uploadFile.perform);
  };

  <template>
    <span
      class='gf-input gf-input-drupal-file{{if
          @options.multiple
          " gf-input-drupal-file--multiple"
          " gf-input-drupal-file--single"
        }}'
      ...attributes
    >
      {{#if @value}}
        {{#if @options.multiple}}
          <ul class='gf-input-drupal-file__list'>
            {{! @glint-ignore }}
            {{#each @value as |file|}}
              <li>
                <OutputDrupalFileComponent @value={{file}} />
                <button
                  class='btn btn-xs btn-default remove'
                  type='button'
                  {{on 'click' (fn this.deleteFile file)}}
                >
                  {{t 'ember-mist-components.labels.remove'}}
                </button>
              </li>
            {{/each}}
          </ul>
        {{else}}
          <ul class='gf-input-drupal-file__list'>
            <li>
              {{! @glint-ignore }}
              <OutputDrupalFileComponent @value={{@value}} />
              <button
                class='btn btn-xs btn-default remove'
                type='button'
                {{! @glint-ignore }}
                {{on 'click' (fn this.deleteFile @value)}}
              >
                {{t 'ember-mist-components.labels.remove'}}
              </button>
            </li>
          </ul>
        {{/if}}
      {{/if}}

      {{#if this.uploadFile.isRunning}}
        <span class='gf-input-drupal-file__uploading'>
          {{t 'ember-mist-components.labels.uploading'}}
          <LoadingComponent />

          <span class='gf-input-drupal-file__progress'>
            ({{this.activeFilePositionInQueue}}/{{this.totalFiles}})
          </span>
        </span>
      {{/if}}

      {{#if this.uploadFile.isIdle}}
        {{#if (or @options.multiple (not @value))}}
          <GfInputFileComponent
            @options={{hash multiple=@options.multiple}}
            @valueChanged={{this.fileSelected}}
            {{!-- id={{@inputId}} --}}
          />
        {{/if}}
      {{/if}}
    </span>
  </template>
}
