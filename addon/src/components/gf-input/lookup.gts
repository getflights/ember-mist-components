import Component from '@glimmer/component';
import type ModelRegistry from 'ember-data/types/registries/model';
import type DrupalModel from '../../models/drupal-model';
import type Query from '../../query/Query';
import GfInputModelAutocompleteComponent, {
  type GfInputModelAutocompleteOptions,
} from './model-autocomplete.gts';
import GfInputLookupWindowComponent, {
  type GfInputLookupWindowOptions,
} from './lookup-window.gts';
import { get } from '@ember/helper';
import { on } from '@ember/modifier';
import { isNone } from '@ember/utils';

/**
 * Options that can be passed in.
 * `hideClear` see this.shouldHideClear
 * `searchQuery` see this.shouldUseSearchQuery
 */
export interface GfInputLookupOptions<K extends keyof ModelRegistry> {
  /**
   * The passed in name of the model
   */
  modelName: K | K[];
  /**
   * Query that can be passed in to limit the results to
   */
  baseQuery?: Query;
  placeholder?: string;
  hideClear?: boolean;
}

export interface GfInputLookupArguments<K extends keyof ModelRegistry> {
  value?: DrupalModel;
  valueChanged: (value: ModelRegistry[K]) => void;
  options: GfInputLookupOptions<K>;
  disabled?: boolean;
}

export interface GfInputLookupSignature<K extends keyof ModelRegistry> {
  Args: GfInputLookupArguments<K>;
  Element: HTMLElement;
}

export default class GfInputLookupComponent<
  K extends keyof ModelRegistry,
> extends Component<GfInputLookupSignature<K>> {
  get modelAutocompleteOptions(): GfInputModelAutocompleteOptions<K> {
    return {
      baseQuery: this.args.options.baseQuery,
      hideClear: this.args.options?.hideClear,
      modelName: this.args.options.modelName as K,
      placeholder: this.args.options.placeholder,
    };
  }

  get lookupWindowOptions(): GfInputLookupWindowOptions<K> {
    return {
      baseQuery: this.args.options.baseQuery,
      modelName: this.args.options.modelName,
    };
  }

  clear = () => {
    this.args.valueChanged(undefined);
  };

  get isMultiModelname() {
    return Array.isArray(this.args.options.modelName);
  }

  <template>
    <div
      class='gf-input gf-input-lookup{{if
          this.isMultiModelname
          " gf-input-lookup--multi"
        }}{{if (isNone @value) " gf-input-lookup--empty"}}'
    >
      {{#if this.isMultiModelname}}
        {{! Output + clear button }}
        <div class='gf-input-lookup__current'>
          {{#if @value}}
            <div class='gf-input-lookup__value'>
              <span class='name'>
                {{! @glint-ignore }}
                {{get @value 'name'}}
              </span>
              <span class='id'>
                ({{@value.id}})
              </span>
            </div>
            <span
              class='gf-input-lookup__clear'
              role='button'
              {{on 'click' this.clear}}
            >
              &#215;
            </span>
          {{/if}}
        </div>
      {{else}}
        <GfInputModelAutocompleteComponent
          @options={{this.modelAutocompleteOptions}}
          @value={{@value}}
          @valueChanged={{@valueChanged}}
          class='gf-input-lookup__autocomplete'
          ...attributes
        />
      {{/if}}
      <GfInputLookupWindowComponent
        @options={{this.lookupWindowOptions}}
        @valueChanged={{@valueChanged}}
        class='gf-input-lookup__button'
      />
    </div>
  </template>
}
