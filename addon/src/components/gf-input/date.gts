import flatpickr from 'flatpickr';
import type { BaseInputArguments } from '@getflights/ember-field-components/components/input/-base';
import Component from '@glimmer/component';
import { service } from '@ember/service';
import type IntlService from 'ember-intl/services/intl';
import { modifier } from 'ember-modifier';
import { isValid } from 'date-fns';
import type FieldInformationService from '@getflights/ember-field-components/services/field-information';

// Some flatpickr options cannot be set by the user of Input/Date because they are overridden by the component.
type PossibleFlatpickrOptions = Omit<
  flatpickr.Options.Options,
  'onChange' | 'mode'
>;
type PossibleRangeFlatpickrOptions = Omit<
  flatpickr.Options.Options,
  'onChange' | 'mode' | 'allowInput' | 'onClose'
>;

export type InputDateOptions<Range extends boolean | undefined> =
  (Range extends true
    ? PossibleRangeFlatpickrOptions
    : PossibleFlatpickrOptions) & {
    range?: Range;
  };

export interface InputDateArguments<
  Range extends boolean | undefined,
  D = Range extends true ? [Date, Date] : Date,
> extends BaseInputArguments {
  value: D | undefined;
  valueChanged?: (newValue: D | undefined) => void;
  options?: InputDateOptions<Range>;
}

export interface InputDateSignature<Range extends boolean | undefined> {
  Args: InputDateArguments<Range>;
  Element: HTMLInputElement;
}

export function unicodeToFlatpickrFormat(unicodeFormat: string): string {
  if (!unicodeFormat) {
    return unicodeFormat;
  }

  let result = unicodeFormat;

  const changes = new Map<string, string>([
    ['HH', 'H'],
    ['H', 'j'],
    ['mm', 'i'],
    ['m', 'i'],
    ['ss', 'S'],
    ['s', 's'],
    ['yyyy', 'Y'],
    ['yy', 'y'],
    ['MM', 'm'],
    ['M', 'n'],
    ['dd', 'd'],
    ['d', 'j'],
  ]);

  // Change <from> to /<index>/
  // This makes sure when you change 'dd' to 'd', this 'd' does not get changes to 'j'
  let index = 0;
  for (const [from, _to] of changes) {
    const searchString = `/${index}/`;
    result = result.replaceAll(from, searchString);
    index++;
  }

  // Change /<index>/ to <to>
  index = 0;
  for (const [_from, to] of changes) {
    const searchString = `/${index}/`;
    result = result.replaceAll(searchString, to);
    index++;
  }

  return result;
}

export default abstract class InputDateComponent<
  Range extends boolean | undefined = undefined,
> extends Component<InputDateSignature<Range>> {
  @service declare fieldInformation: FieldInformationService;
  @service declare intl: IntlService;

  private get hasTime() {
    return !!this.args.options?.enableTime;
  }

  private valueChanged = (
    selectedDates: Date[],
    // _dateStr?: string,
    // _instance?: flatpickr.Instance,
  ) => {
    if (this.args.disabled) {
      return;
    }

    if (selectedDates.length < 1) {
      this.args.valueChanged?.(undefined);
      return;
    }

    if (this.isRange) {
      if (selectedDates.length === 2) {
        const newValue = selectedDates as [Date, Date];
        // @ts-expect-error typing of Range messes up this piece of code
        this.args.valueChanged?.(newValue);
      }
    } else {
      const newValue = selectedDates[0]!;
      // @ts-expect-error typing of Range messes up this piece of code
      this.args.valueChanged?.(newValue);
    }
  };

  private get flatpickrFormat(): string {
    return unicodeToFlatpickrFormat(
      this.args.options?.enableTime
        ? this.fieldInformation.dateTimeFormat
        : this.fieldInformation.dateFormat,
    );
  }

  private validate(value: Date | [Date, Date] | undefined) {
    if (value) {
      return this.isRange
        ? Array.isArray(value) && value.every((v) => v && isValid(v))
        : isValid(value);
    }

    return true;
  }

  private get isValidValue() {
    return this.validate(this.args.value);
  }

  private get invalidTranslation() {
    return this.isRange
      ? this.intl.t('input.date.range.invalid')
      : this.intl.t('input.date.invalid');
  }

  private flatpickrRef?: flatpickr.Instance;

  private flatpickrModifier = modifier((element: HTMLInputElement) => {
    const options: flatpickr.Options.Options = {
      // Default options
      allowInput: true,
      allowInvalidPreload: true,
      enableSeconds: true,
      locale: {
        firstDayOfWeek: 1,
        rangeSeparator: ` ${this.intl.t('input.date.range.separator')} `,
      },
      time_24hr: true,
      dateFormat: this.flatpickrFormat,
      // User provided args
      ...this.args.options,
      // Options that cannot be set by the user
      onChange: this.valueChanged,
      // Overrides
      mode: this.isRange ? 'range' : 'single',
      // Static if inside a dialog (modal)
      static: element.matches('dialog *'),
    };

    if (this.isRange) {
      options['allowInput'] = false;
      options['onClose'] = (
        selectedDates: Date[],
        _dateStr: string,
        instance: flatpickr.Instance,
      ) => {
        // If only one value is filled in, clear it, but don't emit change event (false argument).
        if (selectedDates.length > 0 && selectedDates.length !== 2) {
          instance.clear(false);
        }
      };
    }

    this.flatpickrRef = flatpickr(element, options);

    const removeInvalidValue = () => {
      this.flatpickrRef?.clear(true);
      element.removeEventListener('focus', removeInvalidValue);
    };

    if (this.args.value) {
      if (this.isValidValue) {
        this.flatpickrRef!.setDate(this.args.value);
      } else {
        // Show "invalid" dates in the input, but clear them whenever the user opens the input, because flatpickr can't handle Invalid date values that well.
        element.value = this.invalidTranslation;
        if (typeof this.args.value === 'string') {
          element.value += ` ("${this.args.value}")`;
        }
        element.addEventListener('focus', removeInvalidValue);
      }
    }

    // Cleanup
    return () => {
      element.value = '';
      this.flatpickrRef?.destroy();
      this.flatpickrRef = undefined;
      element.removeEventListener('focus', removeInvalidValue);
    };
  });

  private get placeholder() {
    if (this.isRange) {
      return this.intl.t('input.date.range.placeholder');
    }

    if (this.hasTime) {
      return this.intl.t('input.date.placeholder_datetime');
    }

    return this.intl.t('input.date.placeholder');
  }

  private get inputClass() {
    const classes = ['input'];

    if (this.isRange) {
      classes.push('input-date-range');

      if (this.hasTime) {
        classes.push('input-date-range--time');
      }
    } else {
      classes.push('input-date');

      if (this.hasTime) {
        classes.push('input-date--time');
      }
    }

    if (!this.isValidValue) {
      classes.push('input--invalid');
    }

    return classes.join(' ');
  }

  get isRange() {
    return !!this.args.options?.range;
  }

  <template>
    <div class='gf-input gf-input-date'>
      <input
        autocomplete='off'
        placeholder={{this.placeholder}}
        class={{this.inputClass}}
        ...attributes
        disabled={{@disabled}}
        required={{@required}}
        {{this.flatpickrModifier}}
      />
    </div>
  </template>
}
