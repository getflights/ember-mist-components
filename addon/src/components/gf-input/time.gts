import flatpickr from 'flatpickr';
import Component from '@glimmer/component';
import type { BaseInputArguments } from '@getflights/ember-field-components/components/input/-base';
import { unicodeToFlatpickrFormat } from './date.gts';
import { service } from '@ember/service';
import type IntlService from 'ember-intl/services/intl';
import { format, isValid, parse } from 'date-fns';
import { modifier } from 'ember-modifier';
import type FieldInformationService from '@getflights/ember-field-components/services/field-information';
import t from 'ember-intl/helpers/t';

// Flatpickr CSS
import 'flatpickr/dist/flatpickr.css';

// Some flatpickr options cannot be set by the user of Input/Date because they are overridden by the component.
type PossibleTimeFlatpickrOptions = Omit<
  flatpickr.Options.Options,
  'onChange' | 'mode' | 'enableTime' | 'noCalendar'
>;

export type GfInputTimeOptions = PossibleTimeFlatpickrOptions;

export interface GfInputTimeArguments extends BaseInputArguments {
  value: string | undefined;
  valueChanged?: (newValue: string | undefined) => void;
  options?: GfInputTimeOptions;
}

export interface GfInputTimeSignature {
  Args: GfInputTimeArguments;
  Element: HTMLInputElement;
}

export default class GfInputTimeComponent extends Component<GfInputTimeSignature> {
  @service declare fieldInformation: FieldInformationService;
  @service declare intl: IntlService;

  private valueChanged = (
    selectedDates: Date[],
    // _dateStr?: string,
    // _instance?: flatpickr.Instance,
  ) => {
    if (this.args.disabled) {
      return;
    }

    if (selectedDates.length < 1) {
      this.args.valueChanged?.(undefined);
    } else {
      const newValue = selectedDates[0]!;
      this.args.valueChanged?.(
        format(newValue, this.fieldInformation.timeFormat),
      );
    }
  };

  private get isValidValue() {
    if (this.args.value) {
      const time = parse(
        this.args.value,
        this.fieldInformation.timeFormat,
        new Date(),
      );
      return isValid(time);
    }

    return true;
  }

  private get invalidTranslation() {
    return this.intl.t('input.time.invalid');
  }

  private flatpickrRef?: flatpickr.Instance;

  private flatpickrModifier = modifier((element: HTMLInputElement) => {
    const dateFormat = unicodeToFlatpickrFormat(
      this.fieldInformation.timeFormat,
    );

    const options: flatpickr.Options.Options = {
      // Default options
      allowInput: true,
      allowInvalidPreload: true,
      dateFormat,
      enableSeconds: true,
      time_24hr: true,
      // Args
      ...this.args.options,
      // Overrides
      mode: 'single',
      onChange: this.valueChanged,
      enableTime: true,
      noCalendar: true,
    };

    this.flatpickrRef = flatpickr(element, options);

    const removeInvalidValue = () => {
      this.flatpickrRef?.clear(true);
      element.removeEventListener('focus', removeInvalidValue);
    };

    if (this.args.value) {
      if (this.isValidValue) {
        this.flatpickrRef!.setDate(this.args.value);
      } else {
        // Show "invalid" dates in the input, but clear them whenever the user opens the input, because flatpickr can't handle Invalid date values that well.
        element.value = this.invalidTranslation;
        if (typeof this.args.value === 'string') {
          element.value += ` ("${this.args.value}")`;
        }
        element.addEventListener('focus', removeInvalidValue);
      }
    }

    // Cleanup
    return () => {
      element.value = '';
      this.flatpickrRef?.destroy();
      this.flatpickrRef = undefined;
      element.removeEventListener('focus', removeInvalidValue);
    };
  });

  <template>
    <div class='gf-input gf-input-time'>
      <input
        autocomplete='off'
        placeholder={{t 'input.time.placeholder'}}
        class='input input-time{{unless this.isValidValue " input--invalid"}}'
        ...attributes
        disabled={{@disabled}}
        required={{@required}}
        {{this.flatpickrModifier}}
      />
    </div>
  </template>
}
