import { concat, fn } from '@ember/helper';
import sortableGroup from 'ember-sortable/modifiers/sortable-group';
import sortableItem from 'ember-sortable/modifiers/sortable-item';
import sortableHandle from 'ember-sortable/modifiers/sortable-handle';
import IconComponent from '../icon.gts';
import { on } from '@ember/modifier';
import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';

export interface GfInputMultiArguments<T> {
  value?: T[];
  inputId?: string;
  hideNew?: boolean;

  addNewItem?: () => void;
  removeItem?: (index: number) => void;
  reorderItems: (reorderedItems: T[]) => void;
}

export interface GfInputMultiSignature<T> {
  Args: GfInputMultiArguments<T>;
  Blocks: {
    default: [index: number];
  };
  Element: Element;
}

export default class GfInputMultiComponent<T> extends Component<
  GfInputMultiSignature<T>
> {
  constructor(owner: any, args: any) {
    super(owner, args);

    this.guid = guidFor(this);
  }

  guid;

  /**
   * We need to use the indexes as models for ember-sortable, otherwise when changing the value of an item, it would rerender, making text inputs unusable (defocussing because it's rerendering).
   */
  get indexes() {
    // @ts-ignore
    return Array.apply(null, { length: this.args.value?.length ?? 0 }).map(
      (_, i) => i,
    );
  }

  reorderItems = (newIndexOrder: number[], _draggedIndex: unknown) => {
    this.args.reorderItems(
      newIndexOrder.map((index) => this.args.value![index]!),
    );
  };

  <template>
    <div
      class='gf-input gf-input-multi'
      {{sortableGroup
        groupName=(concat this.guid '-group')
        onChange=this.reorderItems
      }}
    >
      {{#each this.indexes as |index|}}
        <div
          class='gf-input-multi__item'
          {{sortableItem groupName=(concat this.guid '-group') model=index}}
        >
          <div class='sort-handle' sort={{index}} {{sortableHandle}}>
            <IconComponent @name='draggable' />
          </div>
          {{yield index}}
          {{#if @removeItem}}
            <button
              class='btn gf-input-multi__item__remove'
              type='button'
              {{on 'click' (fn @removeItem index)}}
            >
              <IconComponent @name='remove' />
            </button>
          {{/if}}
        </div>
      {{/each}}

      {{#if @addNewItem}}
        <div class='gf-input-multi__button__wrapper'>
          <button
            class='btn gf-input-multi__button'
            type='button'
            {{on 'click' @addNewItem}}
          >
            <IconComponent @name='plus' />
          </button>
        </div>
      {{/if}}
    </div>
  </template>
}
