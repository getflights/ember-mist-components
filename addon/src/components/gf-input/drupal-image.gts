import Component from '@glimmer/component';
import ModalComponent from '../modal.gts';
import { on } from '@ember/modifier';
import OutputDrupalImageComponent from '../output/drupal-image.gts';
import type DrupalImage from '../../interfaces/drupal-image';
import OutputImageLinkComponent from '../output/drupal-image-link.gts';
import t from 'ember-intl/helpers/t';
import GfInputDrupalFileComponent, {
  type GfInputDrupalFileOptions,
} from './drupal-file.gts';

export interface GfInputDrupalImageOptions
  extends GfInputDrupalFileOptions<false> {}

export default class GfInputDrupalImageComponent extends Component<{
  Args: {
    value?: DrupalImage;
    valueChanged?: (newValue: DrupalImage | undefined) => void;
    options: GfInputDrupalImageOptions;
  };
  Blocks: {
    default: [showModal: () => void, value: DrupalImage | undefined];
  };
}> {
  get inputOptions(): GfInputDrupalFileOptions<false> {
    return {
      ...this.args.options,
      endpoint: this.args.options.endpoint ?? 'image/images',
    };
  }

  <template>
    <ModalComponent as |modal|>
      <div class='gf-input gf-input-drupal-image'>
        {{#if (has-block)}}
          {{yield modal.show @value}}
        {{else}}
          {{#if @value.id}}
            <OutputImageLinkComponent
              @value={{@value}}
              class='gf-input-drupal-image__link'
            />

            <button
              class='btn btn-xs gf-input-drupal-image__edit'
              type='button'
              {{on 'click' modal.show}}
            >
              Edit Image
            </button>
          {{else}}
            <button
              class='btn btn-xs gf-input-drupal-image__edit'
              type='button'
              {{on 'click' modal.show}}
            >
              Upload Image
            </button>
          {{/if}}
        {{/if}}
      </div>
      <modal.Dialog as |dialog|>
        <dialog.Header>
          <h4 class='modal-title'>Choose an image</h4>
        </dialog.Header>
        <dialog.Body>
          <div class='gf-input-drupal-image__body'>
            <GfInputDrupalFileComponent
              @value={{@value}}
              @options={{this.inputOptions}}
              {{! @glint-ignore }}
              @valueChanged={{@valueChanged}}
            />
            {{#if @value.id}}
              <OutputDrupalImageComponent
                @value={{@value}}
                class='image-preview'
              />
            {{/if}}
          </div>
        </dialog.Body>
        <dialog.Footer>
          <button type='button' class='btn btn-link' {{on 'click' modal.close}}>
            {{t 'label.close'}}
          </button>
        </dialog.Footer>
      </modal.Dialog>
    </ModalComponent>
  </template>
}
