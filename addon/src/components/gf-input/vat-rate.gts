import { service } from '@ember/service';
import Component from '@glimmer/component';
import type { BaseInputArguments } from '@getflights/ember-field-components/components/input/-base';
import InputSelectComponent, {
  type InputSelectOptions,
} from '@getflights/ember-field-components/components/input/select';
import { trackedFunction } from 'reactiveweb/function';
import type ModelRegistry from 'ember-data/types/registries/model';
import Query from '../../query/Query.ts';
import type Store from '@ember-data/store';
import LoadingComponent from '../loading.gts';
import type SelectOptionGroup from '@getflights/ember-field-components/interfaces/select-option-group';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';

// @ts-ignore TODO: move this input to mist-crm
type VatRateType = ModelRegistry['vat-rate'];

export interface GfInputVatRateOptions {}

export interface GfInputVatRateArguments<
  Required extends boolean | undefined = undefined,
> extends BaseInputArguments {
  value: VatRateType | undefined;
  valueChanged?: (
    newValue: Required extends true ? VatRateType : VatRateType | undefined,
  ) => void;
  required?: Required;
  options: GfInputVatRateOptions;
}

export interface GfInputVatRateSignature<Required extends boolean | undefined> {
  Args: GfInputVatRateArguments<Required>;
  Element: HTMLSpanElement | HTMLSelectElement;
}

export default class GfInputVatRateComponent<
  Required extends boolean | undefined,
> extends Component<GfInputVatRateSignature<Required>> {
  @service declare store: Store;

  private selectOptionsRequest = trackedFunction(this, async () => {
    // @ts-ignore TODO: move this input to mist-crm
    const query = new Query('vat-rate');
    query.setLimit(200);

    const vatRates = await query.fetch(this.store);

    const countryCodes = vatRates
      .uniqBy('countryCode')
      .sortBy('countryCode')
      .mapBy('countryCode');

    return countryCodes
      .map((countryCode) => {
        const vatRatesPerCountry = vatRates
          .filterBy('countryCode', countryCode)
          .sortBy('name');
        const selectOptionGroup: SelectOptionGroup = {
          label: countryCode ? countryCode : 'Unknown',
          selectOptions: vatRatesPerCountry
            .filter(
              (vatRate) =>
                vatRate.active ||
                (this.args.value && vatRate.id == this.args.value?.id),
            )
            .map((vatRate): SelectOption => {
              return {
                value: vatRate.id,
                label: vatRate.name,
              };
            }),
        };

        if (selectOptionGroup.selectOptions.length > 0) {
          return selectOptionGroup;
        }
      })
      .filter(
        (selectOptGroup): selectOptGroup is SelectOptionGroup =>
          !!selectOptGroup,
      );
  });

  get isLoading() {
    return this.selectOptionsRequest.isLoading;
  }

  get isError() {
    return this.selectOptionsRequest.isError;
  }

  get selectOptions() {
    return this.selectOptionsRequest.value ?? [];
  }

  get options(): InputSelectOptions {
    return {
      selectOptions: this.selectOptions ?? [],
    };
  }

  get value() {
    return this.args.value?.id;
  }

  valueChanged = (id: string | undefined) => {
    if (!id) {
      // @ts-ignore typing of Required fucks up these types
      this.args.valueChanged?.(undefined);
      return;
    }
    // id to model
    // @ts-ignore TODO: move this input to mist-crm
    const record = this.store.peekRecord('vat-rate', id)!;
    this.args.valueChanged?.(record);
  };

  <template>
    {{#if this.isLoading}}
      <span class='gf-input gf-input-model-select' ...attributes>
        <div class='gf-input-model-select__loading form-control'>
          <LoadingComponent />
        </div>
      </span>
    {{else}}
      <InputSelectComponent
        class='form-control'
        @value={{this.value}}
        @valueChanged={{this.valueChanged}}
        @required={{@required}}
        @options={{this.options}}
        ...attributes
      />
    {{/if}}
  </template>
}
