import type Store from '@ember-data/store';
import { guidFor } from '@ember/object/internals';
import { service } from '@ember/service';
import type Address from '../../interfaces/address.ts';
import UiService from '../../services/ui.ts';
import LoadingComponent from '../loading.gts';
import GfInputCountryCodeComponent from '../gf-input/country-code.gts';
import { fn, get as getHelper } from '@ember/helper';
import type AddressFormat from '../../models/address-format.ts';
import Component from '@glimmer/component';
import type AddressService from '../../services/address.ts';
import { get } from '@ember/object';
import { trackedFunction } from 'reactiveweb/function';
import { not } from 'ember-truth-helpers';
import InputSelectComponent, {
  type InputSelectOptions,
} from '@getflights/ember-field-components/components/input/select';
import { use } from 'ember-resources';
import AddressHelperResource, {
  AddressHelper,
} from '../../resources/address-helper.ts';
import type { FieldOfAddress } from '../../interfaces/address.ts';
import { on } from '@ember/modifier';

export interface GfInputAddressArguments {
  value: Address | undefined;
  valueChanged: (newValue: Readonly<Address> | undefined) => void;
  required?: boolean;
}

export interface GfInputAddressSignature {
  Args: GfInputAddressArguments;
  Element: HTMLDivElement;
}

export default class GfInputAddressComponent extends Component<GfInputAddressSignature> {
  @service declare store: Store;
  @service declare ui: UiService;

  constructor(owner: any, args: GfInputAddressArguments) {
    super(owner, args);

    this.countryInputId = `${guidFor(this)}-country`;
  }

  private helperResource = use(
    this,
    AddressHelperResource(() => this.args.value?.countryCode),
  );

  get helper(): AddressHelper {
    return this.helperResource.current;
  }

  updateValue = (newValue: Address | undefined) => {
    this.args.valueChanged(Object.freeze(newValue));
  };

  countryCodeChanged = (countryCode: string | undefined) => {
    // New country => new object / No country => undefined
    this.updateValue(countryCode ? { countryCode } : undefined);
  };

  countryInputId;

  addressFieldChanged = (field: string, value?: any) => {
    if (!this.args.value) {
      return;
    }

    const newAddress: Address = {
      ...this.args.value,
    };

    newAddress[field as FieldOfAddress] = value;

    if (this.helper.addressFormat) {
      const { subdivisionDepth, usedFields } = this.helper.addressFormat;

      if (subdivisionDepth > 0) {
        // only need to be reset if there are potential dependent address fields
        const zeroBasedPositionOfField = usedFields.indexOf(
          field as FieldOfAddress,
        );

        if (zeroBasedPositionOfField + 1 < subdivisionDepth) {
          for (
            let i = zeroBasedPositionOfField + 1;
            i < subdivisionDepth;
            i++
          ) {
            newAddress[usedFields[i]!] = undefined;
          }
        }
      }
    }

    this.updateValue(newAddress);
  };

  columnClassName(amountOfColumns: number) {
    const colNumber = Math.max(12 / amountOfColumns, 3);
    return colNumber < 12 ? `col-12 col-sm-${colNumber}` : 'col-12';
  }

  changeEvent = (field: FieldOfAddress, ev: Event) => {
    const newValue = (ev.target as HTMLInputElement).value;

    this.addressFieldChanged(field, newValue);
  };

  <template>
    <div class='gf-input gf-input-address p-0' ...attributes>
      <div class='row'>
        <div class='col-12 country-select'>
          <GfInputCountryCodeComponent
            @value={{@value.countryCode}}
            @valueChanged={{this.countryCodeChanged}}
            @required={{@required}}
            id={{this.countryInputId}}
          />
        </div>
      </div>

      {{#if this.helper.formatError}}
        <div class='alert alert-warning'>
          Error loading address format
        </div>
      {{/if}}

      {{#if @value}}
        {{#if this.helper.addressFormat}}
          {{#each this.helper.rows key='@index' as |row|}}
            <div class='row g-0'>
              {{#each row.columns key='field' as |column|}}
                <div class={{this.columnClassName row.columns.length}}>
                  {{#if column.subdivision}}
                    <GfInputAddressSubdivision
                      @format={{this.helper.addressFormat}}
                      @field={{column.field}}
                      @label={{column.label}}
                      @address={{@value}}
                      @valueChanged={{fn this.addressFieldChanged column.field}}
                      class='form-control'
                    />
                  {{else}}
                    {{! template-lint-disable require-input-label }}
                    <input
                      class='form-control'
                      {{! @glint-ignore }}
                      value={{getHelper @value column.field}}
                      placeholder={{column.label}}
                      {{on 'change' (fn this.changeEvent column.field)}}
                    />
                  {{/if}}
                </div>
              {{/each}}
            </div>
          {{/each}}
        {{/if}}
      {{/if}}

      {{#if this.helper.formatLoading}}
        <div class='address-spinner'>
          <LoadingComponent />
        </div>
      {{/if}}
    </div>
  </template>
}

interface GfInputAddressSubdivisionSignature {
  Args: {
    format: AddressFormat;
    address: Address;
    field: FieldOfAddress;
    label: string;
    valueChanged: (newValue?: string) => void;
  };
  Element: HTMLDivElement;
}

class GfInputAddressSubdivision extends Component<GfInputAddressSubdivisionSignature> {
  @service declare address: AddressService;

  get none() {
    return `--- ${this.args.label} ---`;
  }

  get value(): string {
    // @ts-ignore
    return get(this.args.address, this.args.field);
  }

  private opts = trackedFunction(this, async () => {
    if (this.parentGrouping) {
      return await this.address.getSubdivisionSelectOptions.perform(
        this.parentGrouping,
      );
    }

    return;
  });

  get parentValues() {
    // usedFields: an array, containing the used fields in the addressFormat, sorted from big to small
    const { usedFields, countryCode } = this.args.format;
    const indexOfField = usedFields.indexOf(this.args.field);

    const parentValues = [countryCode];

    for (let i = 0; i < indexOfField; i++) {
      const parentValue = get(this.args.address, usedFields[i]!) as string;
      parentValues.push(parentValue);
    }
    return parentValues;
  }

  get allParentsSet() {
    return !this.parentValues.some((value) => !value);
  }

  get parentGrouping() {
    return this.allParentsSet ? this.parentValues.join(',') : undefined;
  }

  get selectOptions() {
    return this.opts.value ?? [];
  }

  get isLoading() {
    return this.opts.isLoading;
  }

  valueChanged = (newValue?: string) => {
    this.args.valueChanged(newValue);
  };

  get inputSelectOptions(): InputSelectOptions {
    return {
      selectOptions: this.selectOptions,
      placeholder: this.none,
    };
  }

  <template>
    {{#if (not this.allParentsSet)}}
      {{! @glint-ignore }}
      <InputSelectComponent
        @disabled={{true}}
        @options={{this.inputSelectOptions}}
        {{! @glint-ignore }}
        ...attributes
      />
    {{else if this.isLoading}}
      Loading select options
    {{else if this.selectOptions}}
      <InputSelectComponent
        @value={{this.value}}
        @valueChanged={{this.valueChanged}}
        @options={{this.inputSelectOptions}}
        {{! @glint-ignore }}
        ...attributes
      />
    {{else}}
      Error loading subdivision select options
    {{/if}}
  </template>
}
