import Component from '@glimmer/component';
import type { BaseInputArguments } from '@getflights/ember-field-components/components/input/-base';
import { on } from '@ember/modifier';
import { service } from '@ember/service';
import type IntlService from 'ember-intl/services/intl';
import { modifier } from 'ember-modifier';
import IconComponent from '../icon.gts';
import t from 'ember-intl/helpers/t';
import { tracked } from 'tracked-built-ins';

export interface GfInputFileOptions<Multiple extends boolean | undefined> {
  multiple?: Multiple;
  allowedTypes?: string[];
  maxSize?: number;
  onValidationError?: (error: string) => void;
}

export interface GfInputFileArguments<
  Multiple extends boolean | undefined = undefined,
> extends BaseInputArguments {
  value?: Multiple extends true ? File[] : File;
  valueChanged?: (
    newValue: Multiple extends true ? File[] | undefined : File | undefined,
  ) => void;
  options?: GfInputFileOptions<Multiple>;
}

export interface GfInputFileSignature<Multiple extends boolean | undefined> {
  Args: GfInputFileArguments<Multiple>;
  Element: HTMLButtonElement;
}

export default class GfInputFileComponent<
  Multiple extends boolean | undefined,
> extends Component<GfInputFileSignature<Multiple>> {
  @service declare intl: IntlService;

  validateFile = (file: File) => {
    const { allowedTypes, maxSize } = { ...this.args.options };

    if (allowedTypes !== undefined && !allowedTypes.includes(file.type)) {
      this.args.options?.onValidationError?.(
        this.intl.t('input.file.error.invalid_type'),
      );
      return false;
    }

    if (maxSize !== undefined && file.size > maxSize) {
      this.args.options?.onValidationError?.(
        this.intl.t('input.file.error.too_large'),
      );
      return false;
    }

    return true;
  };

  @tracked selectedFiles?: File | File[];

  get selectedFilesLabel() {
    if (Array.isArray(this.selectedFiles)) {
      return this.selectedFiles[0]!.name;
    }

    return this.selectedFiles?.name;
  }

  valueChanged = (ev: Event) => {
    if (this.args.disabled) {
      return;
    }

    try {
      const { files } = ev.target as HTMLInputElement;

      if (files && files.length > 0) {
        if (this.args.options?.multiple) {
          // If all files are valid
          if (Array.from(files).every(this.validateFile)) {
            // @ts-expect-error typing of Multiple messes up this piece of code
            this.args.valueChanged?.(Array.from(files));
            this.selectedFiles = Array.from(files);
          } else {
            this.args.options?.onValidationError?.('Invalid file(s)');
          }
        } else {
          // If all files are valid
          if (this.validateFile(files[0]!)) {
            // @ts-expect-error typing of Multiple messes up this piece of code
            this.args.valueChanged?.(files[0]!);
            this.selectedFiles = files[0];
          } else {
            this.args.options?.onValidationError?.('Invalid file');
          }
        }
      } else {
        this.args.valueChanged?.(undefined);
        this.selectedFiles = undefined;
      }
    } catch (error) {
      // @ts-expect-error type of event.target is wrong
      ev.target.value = undefined;
    }
  };

  get accept() {
    return this.args.options?.allowedTypes?.join(',');
  }

  inputFile?: HTMLInputElement;

  inputFileModifier = modifier((inputFile: HTMLInputElement) => {
    this.inputFile = inputFile;

    if (this.args.value) {
      this.selectedFiles = this.args.value;
    }
  });

  openInputFile = () => {
    this.inputFile?.click();
  };

  <template>
    <div class='gf-input gf-input-file'>
      <button
        type='button'
        class='form-control gf-input-file__browse'
        ...attributes
        {{on 'click' this.openInputFile}}
      >
        {{#if this.selectedFilesLabel}}
          {{this.selectedFilesLabel}}
        {{else}}
          {{t 'ember-mist-components.labels.browse'}}
          <IconComponent @name='paperclip' />
        {{/if}}
      </button>
      {{! template-lint-disable require-input-label }}
      <input
        type='file'
        accept={{this.accept}}
        disabled={{@disabled}}
        required={{@required}}
        multiple={{@options.multiple}}
        {{on 'change' this.valueChanged}}
        {{this.inputFileModifier}}
      />
    </div>
  </template>
}
