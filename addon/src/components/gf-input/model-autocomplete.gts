import { service } from '@ember/service';
import { isBlank } from '@ember/utils';
import { debug, assert } from '@ember/debug';
import { task, timeout } from 'ember-concurrency';
import type Store from '@ember-data/store';
import Query from '../../query/Query.ts';
import Component from '@glimmer/component';
import type ModelRegistry from 'ember-data/types/registries/model';
import PowerSelectComponent from 'ember-power-select/components/power-select';
import type { SomeModel } from '@getflights/ember-field-components';
import { get } from '@ember/helper';

/**
 * Options that can be passed in.
 * `hideClear` see this.shouldHideClear
 * `searchQuery` see this.shouldUseSearchQuery
 */
export interface GfInputModelAutocompleteOptions<
  K extends keyof ModelRegistry,
> {
  /**
   * The passed in name of the model
   */
  modelName: K;
  /**
   * Query that can be passed in to limit the results to
   */
  baseQuery?: Query;
  hideClear?: boolean;
  searchQuery?: boolean;
  prefixField?: string;
  placeholder?: string;
  inputId?: string;
}

export interface GfInputModelAutocompleteArguments<
  K extends keyof ModelRegistry,
> {
  value?: ModelRegistry[K];
  valueChanged?: (value: ModelRegistry[K] | undefined) => void;
  options: GfInputModelAutocompleteOptions<K>;
  disabled?: boolean;
}

export interface GfInputModelAutocompleteSignature<
  K extends keyof ModelRegistry,
> {
  Args: GfInputModelAutocompleteArguments<K>;
  Element: HTMLElement;
}

export default class GfInputModelAutocompleteComponent<
  K extends keyof ModelRegistry,
> extends Component<GfInputModelAutocompleteSignature<K>> {
  @service declare store: Store;

  constructor(owner: unknown, args: any) {
    super(owner, args);

    assert('You must pass in a modelName', !isBlank(args.options.modelName));
  }

  searchTask = task(async (searchQuery: string) => {
    await timeout(500); // Lets debounce the typing by 500ms
    const query = this.query;

    query.clearSearch();

    if (this.shouldUseSearchQuery) {
      query.setSearch(searchQuery);
    } else {
      query.setSearch(searchQuery, 'name');
    }

    return query
      .fetch(this.store)
      .then((results: any) => {
        return results;
      })
      .catch((error: any) => {
        debug(error);
      });
  });

  /**
   * Decides whether the attribute searchQuery on the Query should be used, or the search should happen on the `name` field
   */
  get shouldUseSearchQuery(): boolean {
    return this.args.options?.searchQuery ?? false;
  }

  /**
   * Whether to show the clear button on the power-select component
   */
  get allowClear(): boolean {
    return !(this.args.options?.hideClear ?? false);
  }

  get query(): Query {
    const query = new Query(this.args.options.modelName);

    if (this.args.options.baseQuery) {
      query.copyFrom(this.args.options.baseQuery);
      query.setModelName(this.args.options.modelName);
    }

    return query;
  }

  /**
   * The EmberPowerSelect implementation of the focus functionality (see documentation)
   * @param select The EmberPowerSelect component
   * @param e EmberPowerSelect Event
   */
  handleFocus = (select: any, e: any) => {
    const blurredEl = e.relatedTarget;
    const focusComesFromOutside =
      !isBlank(blurredEl) &&
      !blurredEl.classList.contains('ember-power-select-search-input');

    if (select && focusComesFromOutside) {
      select.actions.open();
    }
  };

  formValueChanged = (value?: SomeModel) => {
    this.args.valueChanged?.(value);
  };

  <template>
    <div class='gf-input gf-input-model-autocomplete'>
      <PowerSelectComponent
        @triggerClass='form-control'
        @triggerId={{@options.inputId}}
        @placeholder={{@options.placeholder}}
        @search={{this.searchTask.perform}}
        @selected={{@value}}
        @onChange={{this.formValueChanged}}
        @onFocus={{this.handleFocus}}
        @searchEnabled={{true}}
        @disabled={{@disabled}}
        @allowClear={{this.allowClear}}
        @selectedItemComponent={{component
          OptionComponent
          type='selected'
          prefixField=@options.prefixField
        }}
        ...attributes
        as |model|
      >
        <OptionComponent
          @type='option'
          @prefixField={{@options.prefixField}}
          @option={{model}}
        />
      </PowerSelectComponent>
    </div>
  </template>
}

class OptionComponent extends Component<{
  Args: {
    type: 'option' | 'selected';
    prefixField?: string;
    option: SomeModel;
  };
}> {
  get formattedName() {
    let prefix;
    if (this.args.prefixField) {
      // @ts-ignore
      prefix = this.args.option[this.args.prefixField];
    }

    // @ts-ignore
    const name = this.args.option['name'];

    return prefix ? `${prefix} ${name}` : name;
  }

  get class() {
    return this.args.type === 'selected'
      ? 'gf-input-model-autocomplete__value'
      : 'gf-input-model-autocomplete__option';
  }

  <template>
    <div class={{this.class}}>
      <span class='name'>
        {{this.formattedName}}
      </span>
      <span class='id'>
        ({{get @option 'id'}})
      </span>
    </div>
  </template>
}
