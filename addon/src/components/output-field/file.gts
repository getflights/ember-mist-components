import Component from '@glimmer/component';

import OutputFieldTemplate, {
  type OutputFieldArguments,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputDrupalFileComponent, {
  type OutputDrupalFileOptions,
  type OutputDrupalFileSignature,
} from '../output/drupal-file.gts';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';

export default class OutputFieldFileComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputDrupalFileOptions>;
  Element: OutputDrupalFileSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputDrupalFileComponent}}
      @fieldType='file'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
