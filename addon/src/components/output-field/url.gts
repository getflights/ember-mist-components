import Component from '@glimmer/component';

import OutputFieldTemplate, {
  type OutputFieldArguments,
} from '@getflights/ember-field-components/components/output-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import OutputUrlComponent, {
  type OutputUrlOptions,
  type OutputUrlSignature,
} from '../output/url.gts';

export default class OutputFieldUrlComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputUrlOptions>;
  Element: OutputUrlSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      {{! @glint-ignore }}
      @outputComponent={{OutputUrlComponent}}
      @fieldType='select'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
