import Component from '@glimmer/component';
import OutputFieldTemplate, {
  type OutputFieldArguments,
} from '@getflights/ember-field-components/components/output-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';
import { format } from 'date-fns';
import { isNone } from '@ember/utils';

export default class OutputFieldAutonumberComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputAutonumberOptions<O, F>>;
  Element: OutputAutonumberSignature<O, F>['Element'];
}> {
  get outputFieldArgs() {
    return {
      ...this.args,
      outputOptions: {
        ...this.args.outputOptions,
        model: this.args.model,
        field: this.args.field,
      },
    };
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      {{! @glint-ignore }}
      @outputComponent={{OutputAutonumberComponent}}
      @fieldType='text'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}

interface OutputAutonumberOptions<O extends SomeModel, F extends FieldOf<O>> {
  model: O;
  field: F;
  minLength?: number;
  prefixPattern: string;
  dateField?: string;
}

interface OutputAutonumberArguments<O extends SomeModel, F extends FieldOf<O>>
  extends BaseOutputArguments {
  value: string | undefined;
  options: OutputAutonumberOptions<O, F>;
}

interface OutputAutonumberSignature<O extends SomeModel, F extends FieldOf<O>> {
  Args: OutputAutonumberArguments<O, F>;
  Element: HTMLSpanElement;
}

class OutputAutonumberComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<OutputAutonumberSignature<O, F>> {
  calculateAutonumberPattern(date: Date, pattern: string): string {
    pattern = pattern.replaceAll('{{YYYY}}', format(date, 'yyyy'));
    pattern = pattern.replaceAll('{{YY}}', format(date, 'yy').padStart(2, '0'));
    pattern = pattern.replaceAll('{{QQ}}', format(date, 'Q').padStart(2, '0'));
    pattern = pattern.replaceAll('{{MM}}', format(date, 'MM').padStart(2, '0'));
    pattern = pattern.replaceAll('{{DD}}', format(date, 'dd').padStart(2, '0'));
    return pattern;
  }

  get value() {
    if (isNone(this.args.value)) {
      return undefined;
    }

    let value = this.args.value;

    if (this.args.options.minLength && this.args.options.minLength > 0) {
      value = value.padStart(this.args.options.minLength, '0');
    }

    let prefix = '';
    if (this.args.options.prefixPattern) {
      // there is a prefix Pattern
      prefix = this.args.options.prefixPattern;

      const dateField = this.args.options.dateField
        ? this.args.options.dateField
        : 'created';

      // Next we check possible date values to replace
      const date = this.args.options.model[dateField] as Date;

      if (date) {
        prefix = this.calculateAutonumberPattern(date, prefix);
      }
    }

    return `${prefix}${value}`;
  }

  <template>
    <span
      class='output output-autonumber{{if
          (isNone this.value)
          " output--empty"
        }}'
      ...attributes
    >
      {{this.value}}
    </span>
  </template>
}
