import Component from '@glimmer/component';

import OutputFieldTemplate, {
  type OutputFieldArguments,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputSelectComponent, {
  type OutputSelectOptions,
  type OutputSelectSignature,
} from '@getflights/ember-field-components/components/output/select';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import { LoadingSelect } from '../input-field/select.gts';
import { service } from '@ember/service';
import type IntlService from 'ember-intl/services/intl';
import { use } from 'ember-resources';
import FieldSelectHelperResource from '../../resources/field-select-helper.ts';

export default class OutputFieldSelectComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputSelectOptions>;
  Element: OutputSelectSignature['Element'];
}> {
  @service declare intl: IntlService;

  private helperResource = use(
    this,
    FieldSelectHelperResource(() => ({
      model: this.args.model,
      field: this.args.field,
      selectOptions: this.args.outputOptions?.selectOptions,
    })),
  );

  get helper() {
    return this.helperResource.current;
  }

  get outputFieldArgs() {
    const args = {
      ...this.args,
      outputOptions: {
        placeholder: this.helper.placeholder,
        ...this.args.outputOptions,
        selectOptions: this.helper.selectOptions,
      },
    };

    if (!args.outputOptions?.selectOptions) {
      args.outputOptions.selectOptions = this.helper.asyncSelectOptions ?? [];

      if (this.helper.isError) {
        args.outputOptions.placeholder = this.intl.t(
          'input-field.select.could_not_load',
        );
      }
    }

    return args;
  }

  get outputComponent() {
    if (!this.args.outputOptions?.selectOptions && this.helper.isLoading) {
      return LoadingSelect;
    }

    return OutputSelectComponent;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      {{! @glint-ignore }}
      @outputComponent={{this.outputComponent}}
      @fieldType='select'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
