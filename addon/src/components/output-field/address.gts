import Component from '@glimmer/component';

import OutputFieldTemplate, {
  type OutputFieldArguments,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputAddressComponent, {
  type OutputAddressOptions,
  type OutputAddressSignature,
} from '../output/address.gts';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';

export default class OutputFieldAddressComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputAddressOptions>;
  Element: OutputAddressSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputAddressComponent}}
      @fieldType='address'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
