import Component from '@glimmer/component';

import OutputFieldTemplate, {
  type OutputFieldArguments,
} from '@getflights/ember-field-components/components/output-field/-base';
import OutputDrupalImageComponent, {
  type OutputDrupalImageOptions,
  type OutputDrupalImageSignature,
} from '../output/drupal-image.gts';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';

export default class OutputFieldImageComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: OutputFieldArguments<O, F, OutputDrupalImageOptions>;
  Element: OutputDrupalImageSignature['Element'];
}> {
  get outputFieldArgs() {
    return this.args;
  }

  <template>
    <OutputFieldTemplate
      @outputFieldArgs={{this.outputFieldArgs}}
      @outputComponent={{OutputDrupalImageComponent}}
      @fieldType='image'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
