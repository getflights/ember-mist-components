import { action } from '@ember/object';
import Order, { Direction } from '../query/Order.ts';
import ListViewService from '../services/list-view.ts';
import Component from '@glimmer/component';
import { tracked } from 'tracked-built-ins';
import type ModelRegistry from 'ember-data/types/registries/model'; // eslint-disable-line ember/use-ember-data-rfc-395-imports
import { service } from '@ember/service';
import type Store from '@ember-data/store';
import { camelize, dasherize } from '@ember/string';
import { cached } from '@glimmer/tracking';
import { trackedFunction } from 'reactiveweb/function';

export type ModelKey = keyof ModelRegistry & string;
export type SomeModel = ModelRegistry[keyof ModelRegistry];

export interface CollectionPageInfo {
  count: number;
  totalCount: number;
  pageSize: number;
  pageCurrent: number;
  pageCount: number;
  resultRowFirst: number;
  resultRowLast: number;
  setPage: (number: number) => void;
  setPageSize: (number: number) => void;
}

export interface CollectionSortingInfo {
  order?: {
    field: string;
    direction: Direction;
  };
  setSort: (path: string, direction?: -1 | 1) => void;
  fields: string[];
}

export interface CollectionSearchInfo {
  searchString?: string;
  /**
   * Fields that are being searched
   */
  searchFields?: string[];
  /**
   * Fields you can search on
   */
  fields: string[];
  setSearch: (searchString: string, ...searchFields: string[]) => void;
  clearSearch: () => void;
}

export type FieldOf<M extends SomeModel> = keyof M & string;

export interface Collection<M extends SomeModel> {
  // Data
  modelName: keyof ModelRegistry;
  records: Array<M>;
  meta: any;
  refresh: () => void;
  fields: string[];
  // Pagination - sort - search
  pagination: CollectionPageInfo;
  sorting: CollectionSortingInfo;
  search: CollectionSearchInfo;
  // Loading and error state
  isLoading: boolean;
  isError?: boolean;
  errorMessage?: string;
}

export interface BaseCollectionArguments {
  modelListView?: string;
  // List view id (for columns, sorting...)
  listViewId?: string;
}

export interface BaseCollectionSignature<M extends SomeModel> {
  Args: BaseCollectionArguments;
  Blocks: {
    default: [collection: Collection<M>];
  };
}

export default abstract class BaseCollectionComponent<
  M extends SomeModel,
  S extends BaseCollectionSignature<M>,
> extends Component<S> {
  @service declare store: Store;
  // Rename to listViewService (no conflict with listView propr)
  @service('list-view') declare listViewService: ListViewService;

  abstract get modelName(): ModelKey;

  @cached
  get modelListView() {
    return this.listViewService.getModelListView(
      this.modelName,
      this.args.modelListView,
    );
  }

  protected apiListViewRequest = trackedFunction(this, async () => {
    if (this.args.listViewId) {
      return await this.listViewService.getListViewById(this.args.listViewId);
    }
  });

  get apiListView() {
    // We try to avoid adding apiListViewRequest to the tracked chain if there is no listViewId
    if (!this.args.listViewId) {
      return;
    }

    return this.apiListViewRequest.value;
  }

  get listView() {
    return this.apiListView ?? this.modelListView;
  }

  @cached
  get modelClass() {
    return this.store.modelFor(this.modelName);
  }

  // @cached
  // get modelFields() {
  //   const fields = new Map<string, 'field' | 'property' | 'computed'>();

  //   const prototypeProperties = Object.getOwnPropertyNames(
  //     this.modelClass.prototype
  //   );
  //   const instanceProperties = Object.getOwnPropertyNames(
  //     new this.modelClass()
  //   );

  //   this.listView.columns.forEach((column) => {
  //     const keys = column.split('.');

  //     const camelizedKey = camelize(keys[0]!);

  //     const kind = this.modelClass.fields.get(camelizedKey);

  //     if (kind) {
  //       fields.set(column, 'field'); // attribute or relationship
  //       return;
  //     }

  //     // Property or getter
  //     if (prototypeProperties.includes(camelizedKey)) {
  //       fields.set(column, 'computed');
  //       return;
  //     }
  //     if (instanceProperties.includes(camelizedKey)) {
  //       fields.set(column, 'property');
  //       return;
  //     }
  //   });

  //   return fields;
  // }

  /**
   * Fields that are either an attribute or relation and thus exist in the backend.
   */
  @cached
  get existingFields() {
    return this.fields.filter((field) => {
      const keys = field.split('.');

      const checkNestedField = (keys: string[], modelClass: any): boolean => {
        const camelizedKey = camelize(keys[0]!);
        const kind = modelClass.fields.get(camelizedKey);

        if (!kind) {
          return false;
        }

        if (kind === 'attribute') {
          const meta = modelClass.attributes.get(camelizedKey);

          // No search on an address itself
          if (meta.type === 'address' && keys.length === 1) {
            return false;
          }

          return true;
        }

        if (kind === 'belongsTo') {
          if (keys.length === 1) {
            return true;
          }

          const { meta } = modelClass.relationshipsByName.get(camelizedKey);

          if (meta && meta.type) {
            const relationshipModelClass = this.store.modelFor(meta.type);
            return checkNestedField(keys.slice(1), relationshipModelClass);
          }
        }

        return false;
      };

      return checkNestedField(keys, this.modelClass);
    });
  }

  /**
   * A dasherized list of (default) fields that can be used in UI components
   */
  @cached
  get fields() {
    const modelClassProperties: string[] = [];

    // @ts-ignore
    this.modelClass.eachComputedProperty((field: string, _options: any) => {
      modelClassProperties.push(field);
    });

    const instanceProperties = Object.getOwnPropertyNames(
      new this.modelClass(),
    );

    const prototypeProperties: string[] = Object.getOwnPropertyNames(
      this.modelClass.prototype,
    );

    return this.listView.columns
      .filter((column): column is FieldOf<M> => {
        const keyToFind = camelize(column.split('.')[0]!);
        return (
          modelClassProperties.includes(keyToFind) ||
          instanceProperties.includes(keyToFind) ||
          prototypeProperties.includes(keyToFind)
        );
      })
      .map((field) => dasherize(field));
  }

  //#region PAGINATION
  @tracked desiredPage = 1;

  @action
  setPage(page: number) {
    this.desiredPage = page;
  }

  @tracked desiredPageSize?: number;

  @action
  setPageSize(size: number) {
    this.desiredPageSize = size;
    // also reset page to 1
    this.desiredPage = 1;
  }
  //#endregion

  //#region SORTING
  @tracked protected desiredSortOrder?: Order;

  @action
  setSort(path: string, direction?: -1 | 1) {
    this.desiredPage = 1;

    // -1 --> DESC
    // 1 | undefined --> ASC (default)
    this.desiredSortOrder = new Order(
      path,
      direction === -1 ? Direction.DESC : Direction.ASC,
    );
  }
  //#endregion

  //#region SEARCH
  get search(): CollectionSearchInfo {
    return {
      searchString: this.searchString,
      searchFields: this.searchFields,
      fields: this.existingFields,
      setSearch: this.setSearch,
      clearSearch: this.clearSearch,
    };
  }

  @tracked searchString?: string;

  @tracked desiredSearchFields: string[] = tracked([]);

  @cached
  get searchFields() {
    return this.desiredSearchFields.length > 0
      ? this.desiredSearchFields
      : this.existingFields;
  }

  @action
  setSearch(searchString: string, ...searchFields: string[]) {
    this.clearSearch();
    this.searchString = searchString;
    this.desiredSearchFields.push(...searchFields);
  }

  @action
  clearSearch() {
    this.searchString = undefined;
    this.desiredSearchFields.splice(0, this.desiredSearchFields.length);
  }
  //#endregion
}
