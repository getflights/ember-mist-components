import Component from '@glimmer/component';
import Calendar, { type CalendarOptions } from '../objects/calendar.ts';
import {
  format,
  isSameDay,
  isSameMonth,
  isToday,
  isWeekend,
  setISODay,
} from 'date-fns';
import { on } from '@ember/modifier';
import eq from 'ember-truth-helpers/helpers/eq';
import { service } from '@ember/service';
import { type IntlService } from 'ember-intl';
import t from 'ember-intl/helpers/t';
import { concat, fn } from '@ember/helper';
import { resource, resourceFactory, use } from 'ember-resources';
import { action } from '@ember/object';

export interface CalendarSignature {
  Args: {
    center?: Date | undefined;
    selectedDate?: Date | undefined;
    dateRangeChanged?: ((start: Date, end: Date) => void) | undefined;
    /**
     * Initial options to pass on to the calendar.
     * These values are not tracked by the calendar and are only passed once on initialization.
     */
    calendarOptions?: Partial<CalendarOptions> | undefined;
    onDayClick?: ((date: Date) => void) | undefined;
  };
  Element: HTMLElement;
  Blocks: {
    toolbar: [];
    day: [Date];
    default: [];
  };
}

const MONTH_INDEXES = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

const defaultCalendarOptions: Partial<CalendarOptions> = {};

const CalendarResource = resourceFactory(
  (
    centerFn: (() => Date | undefined) | undefined,
    options: Partial<CalendarOptions> | undefined,
  ) => {
    let calendar: Calendar;
    let previousCenter: Date | undefined;

    return resource(() => {
      const center = centerFn?.();

      if (!calendar) {
        calendar = new Calendar(center, options);
        previousCenter = center;
      } else {
        Promise.resolve().then(() => {
          if (center && center !== previousCenter) {
            calendar.center = center;
            previousCenter = center;
          }
        });
      }

      return calendar;
    });
  },
);

export default class CalendarComponent extends Component<CalendarSignature> {
  @service declare intl: IntlService;

  constructor(owner: any, args: any) {
    super(owner, args);

    this.labels = {
      days: [1, 2, 3, 4, 5, 6, 7].map((dayNumber) => {
        const weekIntlKey = `calendar.days.${dayNumber}`;

        return this.intl.exists(weekIntlKey)
          ? this.intl.t(weekIntlKey)
          : format(setISODay(new Date(), dayNumber - 1), 'ccc');
      }),
      months: MONTH_INDEXES.map((monthIndex) => {
        const monthIntlKey = `calendar.months.${monthIndex + 1}`;

        return this.intl.exists(monthIntlKey)
          ? this.intl.t(monthIntlKey)
          : format(new Date().setMonth(monthIndex), 'LLLL');
      }),
    };

    this.monthSelectOptions = this.labels.months.map((month, monthIndex) => {
      return {
        value: monthIndex,
        label: month,
      };
    });
  }

  private CalendarResource = use(
    this,
    CalendarResource(() => this.args.center, this.calendarOptions),
  );

  get calendar() {
    return this.CalendarResource.current;
  }

  @action
  onCenterChange(newCenter: Date) {
    this.args.calendarOptions?.onCenterChange?.(newCenter);
    this.args.dateRangeChanged?.(
      this.calendar.visibleDays[0]!,
      this.calendar.visibleDays[this.calendar.visibleDays.length - 1]!,
    );
  }

  get calendarOptions(): Partial<CalendarOptions> {
    return Object.assign(
      {},
      defaultCalendarOptions,
      this.args.calendarOptions,
      {
        onCenterChange: this.onCenterChange,
      },
    );
  }

  monthSelectOptions: {
    value: number;
    label: string;
  }[];

  get weekSelectOptions() {
    return this.calendar.ISOWeeks.map((weekNumber) => {
      return {
        value: weekNumber,
        label: `${this.intl.t('calendar.week')} ${weekNumber}`,
      };
    });
  }

  get yearSelectOptions() {
    const yearsBefore = 2;
    const yearsAfter = 5;

    const currentYear = new Date().getFullYear();
    return Array.from(
      { length: yearsBefore + 1 + yearsAfter },
      (_, i) => i - yearsBefore,
    ).map((yearModifier) => {
      const value = currentYear + yearModifier;
      const label = format(new Date().setFullYear(value), 'yyyy');

      return {
        value,
        label,
      };
    });
  }

  labels: {
    days: string[];
    months: string[];
  };

  get visibleDateRange() {
    return [
      this.calendar.visibleDays[0]!,
      this.calendar.visibleDays[this.calendar.visibleDays.length - 1]!,
    ]
      .map((date) => format(date, 'd MMMM yyyy'))
      .join(' - ');
  }

  onMonthInput = (ev: Event) => {
    const value = (ev.currentTarget! as HTMLSelectElement).value;
    const valueAsNumber = parseInt(value);
    this.calendar.month = valueAsNumber;
  };

  onWeekInput = (ev: Event) => {
    const value = (ev.currentTarget! as HTMLSelectElement).value;
    const valueAsNumber = parseInt(value);
    this.calendar.week = valueAsNumber;
  };

  onYearInput = (ev: Event) => {
    const value = (ev.currentTarget! as HTMLSelectElement).value;
    const valueAsNumber = parseInt(value);
    this.calendar.year = valueAsNumber;
  };

  onViewInput = (ev: Event) => {
    const value = (ev.currentTarget! as HTMLSelectElement).value as
      | 'month'
      | 'week';
    this.calendar.view = value;
  };

  centerOnToday = () => {
    this.calendar.center = new Date();
  };

  dayClasses = (date: Date) => {
    const classes: string[] = [];

    if (isToday(date)) {
      classes.push('calendar__day--today');
    }

    if (this.args.selectedDate && isSameDay(date, this.args.selectedDate)) {
      classes.push('calendar__day--selected');
    }

    if ('month' === this.calendar.view) {
      if (isSameMonth(date, this.calendar.center)) {
        classes.push('calendar__day--current-month');
      } else {
        classes.push('calendar__day--other-month');
      }
    }

    if (isWeekend(date)) {
      classes.push('calendar__day--weekend');
    }

    return ' ' + classes.join(' ');
  };

  onDayClick = (date: Date) => {
    this.args.onDayClick?.(date);
  };

  <template>
    <div
      class='calendar{{concat " calendar--view-" this.calendar.view}}'
      ...attributes
    >
      <div class='calendar__toolbar'>
        <div class='calendar__toolbar__left'>
          {{#unless this.calendar.todayVisible}}
            <button
              type='button'
              class='calendar__today'
              {{on 'click' this.centerOnToday}}
            >
              {{t 'calendar.today'}}
            </button>
          {{/unless}}

          <select aria-label='View' {{on 'input' this.onViewInput}}>
            <option value='month' selected={{eq this.calendar.view 'month'}}>{{t
                'calendar.view.month'
              }}</option>
            <option value='week' selected={{eq this.calendar.view 'week'}}>{{t
                'calendar.view.week'
              }}</option>
          </select>
        </div>

        <div class='calendar__toolbar__center'>
          {{#if (eq this.calendar.view 'month')}}
            <select aria-label='Month' {{on 'input' this.onMonthInput}}>
              {{#each this.monthSelectOptions as |opt|}}
                <option
                  value={{opt.value}}
                  selected={{eq opt.value this.calendar.month}}
                >{{opt.label}}</option>
              {{/each}}
            </select>
          {{else}}
            <select aria-label='Week' {{on 'input' this.onWeekInput}}>
              {{#each this.weekSelectOptions as |opt|}}
                <option
                  value={{opt.value}}
                  selected={{eq opt.value this.calendar.week}}
                >{{opt.label}}</option>
              {{/each}}
            </select>
          {{/if}}

          <select aria-label='Year' {{on 'input' this.onYearInput}}>
            {{#each this.yearSelectOptions as |opt|}}
              <option
                value={{opt.value}}
                selected={{eq opt.value this.calendar.year}}
              >{{opt.label}}</option>
            {{/each}}
          </select>
        </div>

        <div class='calendar__toolbar__right'>
          {{#if (eq this.calendar.view 'month')}}
            <div class='calendar__nav'>
              <button
                type='button'
                class='calendar__nav__button'
                {{on 'click' this.calendar.previousMonth}}
              >
                <i class='icon-arrow-left' />
              </button>
              <button
                type='button'
                class='calendar__nav__button'
                {{on 'click' this.calendar.nextMonth}}
              >
                <i class='icon-arrow-right' />
              </button>
            </div>
          {{else}}
            <div class='calendar__nav'>
              <button
                type='button'
                class='calendar__nav__button'
                {{on 'click' this.calendar.previousWeek}}
              >
                <i class='icon-arrow-left' />
              </button>
              <button
                type='button'
                class='calendar__nav__button'
                {{on 'click' this.calendar.nextWeek}}
              >
                <i class='icon-arrow-right' />
              </button>
            </div>
          {{/if}}
        </div>
      </div>
      <div class='calendar__labels'>
        {{#if (eq this.calendar.view 'week')}}
          <div class='calendar__label-date-range'>
            {{this.visibleDateRange}}
          </div>
        {{/if}}
        {{#each this.labels.days as |day|}}
          <div class='calendar__label-day'>
            {{day}}
          </div>
        {{/each}}
      </div>
      <div class='calendar__day-grid'>
        {{#each this.calendar.visibleDays as |day|}}
          <div
            role={{if @onDayClick 'button'}}
            class='calendar__day{{this.dayClasses day}}'
            {{! template-lint-disable no-invalid-interactive }}
            {{on 'click' (fn this.onDayClick day)}}
          >
            <div class='calendar__day__number'>
              {{format day 'd'}}
            </div>
            {{yield day to='day'}}
          </div>
        {{/each}}
      </div>
      {{yield}}
    </div>
  </template>
}
