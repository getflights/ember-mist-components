/* global window */
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { getOwner } from '@ember/application';
import type Store from '@ember-data/store';
import { task } from 'ember-concurrency';
import DrupalModel from '../models/drupal-model.ts';
import { alias } from '@ember/object/computed';
import { cached } from '@glimmer/tracking';

interface Arguments {
  model: DrupalModel;
  template: DrupalModel;
  language?: string;
}

export default class TemplateGenerateButtonComponent extends Component<Arguments> {
  @service declare store: Store;

  @cached
  get config(): object {
    // @ts-ignore
    return getOwner(this).resolveRegistration('config:environment');
  }

  @alias('config.apiEndpoint')
  apiEndpoint!: string;

  generatePdf = task({ drop: true }, async () => {
    let digest = null;

    await this.args.template
      // @ts-ignore
      .generateDigest({ id: this.args.model.id })
      .then((response: Response) => {
        return response.json().then((payload) => {
          digest = payload.digest;
        });
      });

    window.open(
      // @ts-ignore
      `${this.apiEndpoint}template/generate/${this.args.template.key}?id=${
        this.args.model.id
      }&digest=${digest}${
        this.args.language ? `&lang=${this.args.language}` : ''
      }`,
      '_blank',
    );
  });
}
