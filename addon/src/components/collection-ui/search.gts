import Component from '@glimmer/component';
import type { Collection, SomeModel } from '../-base-collection.ts';
import { modifier } from 'ember-modifier';
import { tracked } from 'tracked-built-ins';
import { isBlank } from '@ember/utils';
import { service } from '@ember/service';
import type FieldInformationService from '@getflights/ember-field-components/services/field-information';
import { camelize } from '@ember/string';
import InputTextComponent from '@getflights/ember-field-components/components/input/text';
import { on } from '@ember/modifier';
import t from 'ember-intl/helpers/t';
import gt from 'ember-truth-helpers/helpers/gt';
import InputSelectComponent from '@getflights/ember-field-components/components/input/select';
import { hash } from '@ember/helper';
import MenuItemComponent from '../menu-item.ts';
import IconComponent from '../icon.gts';

interface CollectionUiSearchArguments<M extends SomeModel> {
  collection: Collection<M>;
  close?: () => void;
}

interface CollectionUiSearchSignature<M extends SomeModel> {
  Args: CollectionUiSearchArguments<M>;
}

const undefinedOrBlank = (value: string | undefined): value is undefined => {
  return !value || isBlank(value);
};

export default class CollectionUiSearchComponent<
  M extends SomeModel,
> extends Component<CollectionUiSearchSignature<M>> {
  @service declare fieldInformation: FieldInformationService;

  constructor(owner: any, args: any) {
    super(owner, args);

    this.searchString = this.args.collection.search.searchString;
  }

  @tracked searchString?: string;

  searchValueChanged = (searchString: string) => {
    this.searchString = searchString;
  };

  submit = () => {
    if (undefinedOrBlank(this.searchString)) {
      if (!undefinedOrBlank(this.args.collection.search.searchString)) {
        this.args.collection.search.clearSearch();
      }
      return;
    }

    if (this.searchField) {
      this.args.collection.search.setSearch(
        this.searchString,
        this.searchField,
      );
    } else {
      this.args.collection.search.setSearch(this.searchString);
    }
  };

  formSubmitted = (event: SubmitEvent) => {
    if (event) {
      event.preventDefault();
    }

    this.submit();
  };

  inputSearchModifier = modifier((element: HTMLInputElement) => {
    // When rendered in a modal, the element may not be visible yet. Adding this small delay works.
    setTimeout(() => element.focus(), 1);
  });

  @tracked searchField?: string;

  setSearchField = (column?: string) => {
    this.searchField = !isBlank(column) ? column : undefined;
  };

  get fieldSelectOptions() {
    return this.args.collection.search.fields.map((field) => {
      // Camelize the key, but KEEP the path separator (.)
      const camelizedField = field
        .split('.')
        .map((fieldPart) => camelize(fieldPart))
        .join('.');

      const translatedField = this.fieldInformation.getTranslatedFieldLabel(
        this.args.collection.modelName,
        camelizedField,
      );

      return {
        value: field,
        label: translatedField,
      };
    });
  }

  <template>
    <form class='cui-search' {{on 'submit' this.formSubmitted}}>
      <InputTextComponent
        @value={{this.searchString}}
        placeholder={{t 'search.placeholder'}}
        @valueChanged={{this.searchValueChanged}}
        {{this.inputSearchModifier}}
        class='form-control cui-search__input'
        {{!on "keydown" this.keydown}}
      />
      {{#if (gt this.fieldSelectOptions.length 1)}}
        <InputSelectComponent
          @options={{hash
            selectOptions=this.fieldSelectOptions
            placeholder=(t 'collection-ui.search.all')
          }}
          @value={{this.searchField}}
          @required={{false}}
          @valueChanged={{this.setSearchField}}
          class='form-control cui-search__fieldselect'
        />
      {{/if}}
      <span class='input-group-text'>
        <ul class='action-menu cui-search__actions'>
          <MenuItemComponent>
            <button
              type='button'
              class='cui-search__icon'
              {{on 'click' this.submit}}
            >
              <IconComponent @name='search' />
            </button>
          </MenuItemComponent>
          {{#if @close}}
            <MenuItemComponent @itemClicked={{@close}}>
              <IconComponent @name='close' />
            </MenuItemComponent>
          {{/if}}
        </ul>
      </span>
    </form>
  </template>
}
