import Component from '@glimmer/component';
import CalendarComponent from '../calendar.gts';
import type { Collection, SomeModel } from '../-base-collection';
import { cached } from '@glimmer/tracking';
import { format, isValid } from 'date-fns';
import { on } from '@ember/modifier';
import t from 'ember-intl/helpers/t';
import IconComponent from '../icon.gts';
import MenuDropdownComponent from '../menu-dropdown.ts';
import MenuItemComponent from '../menu-item.ts';
import ModalComponent from '../modal.gts';
import CopyTextComponent from '../copy-text.ts';
import LoadingComponent from '../loading.gts';
import { hash } from '@ember/helper';

interface CollectionUiCalendarArguments<M extends SomeModel> {
  collection: Collection<M>;
  dateField: keyof M;
  onCenterChange: (newCenter: Date) => void;
  // Optional setting center outside of calendar
  center?: Date;
  // Optional selection handling outside of calendar
  selectedDate?: Date;
  onSelect?: (date?: Date) => void;
}

export interface CollectionUiCalendarSignature<M extends SomeModel> {
  Element: HTMLDivElement;
  Args: CollectionUiCalendarArguments<M>;
  Blocks: {
    item: [model: M];
    filters: [];
    actions: [];
  };
}

export default class CollectionUiCalendarComponent<
  M extends SomeModel,
> extends Component<CollectionUiCalendarSignature<M>> {
  private mapDayFormat = 'yyyy-MM-dd';

  /**
   * Returns a hashmap with as key the date, and value an array of models for that date
   */
  @cached
  get modelsPerDate() {
    return this.args.collection.records.reduce((map, model, _index) => {
      const dateValue = model[this.args.dateField];

      if (isValid(dateValue)) {
        const key = format(dateValue as Date, this.mapDayFormat);

        if (map.has(key)) {
          map.get(key)!.push(model);
        } else {
          map.set(key, [model]);
        }
      }

      return map;
    }, new Map<string, M[]>());
  }

  modelsForDay = (date: Date) => {
    return this.modelsPerDate.get(format(date, this.mapDayFormat)) ?? [];
  };

  <template>
    <div class='cui-calendar__header'>
      <div class='cui-calendar__header__filters'>
        {{yield to='filters'}}
      </div>
      <div class='cui-calendar__header__actions action-menu'>
        <MenuDropdownComponent>
          <MenuItemComponent @itemClicked={{@collection.refresh}}>
            {{t 'ember-mist-components.labels.refresh'}}
            <IconComponent @name='refresh' />
          </MenuItemComponent>
          {{#if @collection.meta.icalFeed}}
            <ModalComponent as |modal|>
              <MenuItemComponent @itemClicked={{modal.show}}>
                {{t 'ember-mist-components.labels.export'}}
                <i class='icon-calendar-export'></i>
              </MenuItemComponent>
              <modal.Dialog as |dialog|>
                <dialog.Body>
                  {{t 'calendar.export.general' htmlSafe=true}}
                  <CopyTextComponent @text={{@collection.meta.icalFeed}} />
                  {{t 'calendar.export.resources' htmlSafe=true}}
                  <ul>
                    <li>
                      <a
                        href='https://support.google.com/calendar/answer/37100?hl=en&co=GENIE.Platform%3DDesktop#:~:text=Use%20a%20link%20to%20add%20a%20public%20calendar'
                      >{{t 'calendar.export.google'}}</a>
                    </li>
                    <li>
                      <a
                        href='https://support.microsoft.com/en-gb/office/import-or-subscribe-to-a-calendar-in-outlook-com-cff1429c-5af6-41ec-a5b4-74f2c278e98c#:~:text=select%20Import.-,Subscribe%20to%20a%20calendar,-Note%3A%C2%A0When'
                      >{{t 'calendar.export.microsoft'}}</a>
                    </li>
                    <li>
                      <a
                        href='https://support.apple.com/guide/iphone/use-multiple-calendars-iph3d1110d4/ios#:~:text=Subscribe%20to%20an%20external%2C%20read%2Donly%20calendar'
                      >{{t 'calendar.export.apple'}}</a>
                    </li>
                  </ul>
                  {{t 'calendar.export.resources_other' htmlSafe=true}}
                </dialog.Body>
                <dialog.Footer>
                  <button
                    type='button'
                    class='btn btn-link'
                    {{on 'click' modal.close}}
                  >
                    {{t 'label.close'}}
                  </button>
                </dialog.Footer>
              </modal.Dialog>
            </ModalComponent>
          {{/if}}
        </MenuDropdownComponent>
        {{yield to='actions'}}
      </div>
    </div>
    <CalendarComponent
      @center={{@center}}
      @selectedDate={{@selectedDate}}
      @calendarOptions={{hash onCenterChange=@onCenterChange}}
      @onDayClick={{@onSelect}}
      class='cui-calendar__calendar'
    >
      {{! <:toolbar>
        Toolbar ?
      </:toolbar> }}
      <:day as |date|>
        {{#let (this.modelsForDay date) as |models|}}
          {{#if models}}
            <div class='calendar-day__content'>
              {{#if (has-block 'item')}}
                {{#each models as |model|}}
                  {{yield model to='item'}}
                {{/each}}
              {{else}}
                {{#each models as |model|}}
                  <div class='calendar-day__item'>
                    {{! @glint-ignore }}
                    {{model.name}}
                  </div>
                {{/each}}
              {{/if}}
            </div>
          {{/if}}
        {{/let}}
      </:day>
      <:default>
        {{#if @collection.isLoading}}
          <div class='cui-calendar__loading'>
            <LoadingComponent />
          </div>
        {{/if}}
      </:default>
    </CalendarComponent>
  </template>
}
