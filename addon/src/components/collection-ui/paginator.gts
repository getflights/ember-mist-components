import Component from '@glimmer/component';
import type { Collection, SomeModel } from '../-base-collection.ts';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';
import { service } from '@ember/service';
import UiService from '../../services/ui.ts';
import { type IntlService } from 'ember-intl';
import t from 'ember-intl/helpers/t';
import LoadingComponent from '../loading.gts';
import InputSelectComponent from '@getflights/ember-field-components/components/input/select';
import { hash } from '@ember/helper';
import not from 'ember-truth-helpers/helpers/not';
import { on } from '@ember/modifier';
import IconComponent from '../icon.gts';
import InputNumberComponent from '@getflights/ember-field-components/components/input/number';
import { tracked } from 'tracked-built-ins';

interface CollectionUiPaginatorArguments<M extends SomeModel> {
  collection: Collection<M>;
}

interface CollectionUiPaginatorSignature<M extends SomeModel> {
  Element: HTMLElement;
  Args: CollectionUiPaginatorArguments<M>;
}

export default class CollectionUiPaginatorComponent<
  M extends SomeModel,
> extends Component<CollectionUiPaginatorSignature<M>> {
  @service declare ui: UiService;
  @service declare intl: IntlService;

  get hasResults() {
    return this.pagination.totalCount > 0;
  }

  // TOTALS
  get totals() {
    return `${this.pagination.resultRowFirst} - ${
      this.pagination.resultRowLast
    } ${this.intl.t('ember-mist-components.labels.of')} ${
      this.pagination.totalCount
    }`;
  }

  // LIMIT
  get limitSelectOptions() {
    const limits = [10, 25, 50, 100, 200];

    if (!limits.includes(this.args.collection.pagination.pageSize)) {
      limits.push(this.args.collection.pagination.pageSize);
    }

    return limits
      .sort((a, b) => a - b)
      .map((limit) => {
        return {
          value: `${limit}`,
          label: `${limit}`,
        };
      });
  }

  get selectedLimit() {
    return `${this.pagination.pageSize}`;
  }

  selectedLimitChanged = (value: string) => {
    this.pagination.setPageSize(parseInt(value));
  };

  // PAGINATION
  get pagination() {
    return this.args.collection.pagination;
  }

  get pageSelectOptions(): SelectOption[] | undefined {
    const { pageCount } = this.pagination;

    if (this.pagination.pageCount > 30) {
      return undefined;
    }

    if (pageCount && pageCount > 0) {
      return Array.from(Array(pageCount), (_, i) => ({
        value: `${i + 1}`,
        label: `${i + 1}`,
      }));
    }

    return [
      {
        value: `1`,
        label: `1`,
      },
    ];
  }

  @tracked private _desiredPage: number | undefined;

  get desiredPage() {
    if (this._desiredPage !== undefined) {
      return this._desiredPage;
    }

    return this.pagination.pageCurrent;
  }

  desiredPageChanged = (value: number | undefined) => {
    if (value !== undefined && !isNaN(value)) {
      this._desiredPage = Math.min(value, this.pagination.pageCount);
    }
  };

  desiredPageFormSubmitted = (event?: Event) => {
    event?.preventDefault();

    this.updateDesiredPage();
  };

  updateDesiredPage = () => {
    if (!this.hasResults) {
      return;
    }

    if (this.desiredPage !== this.pagination.pageCurrent) {
      this.pagination.setPage(this.desiredPage);
    }
  };

  previousPage = () => {
    if (this.hasPreviousPage) {
      this.pagination.setPage(this.pagination.pageCurrent - 1);
    }
  };

  nextPage = () => {
    if (this.hasNextPage) {
      this.pagination.setPage(this.pagination.pageCurrent + 1);
    }
  };

  get hasNextPage() {
    return this.pagination.pageCurrent < this.pagination.pageCount;
  }

  get hasPreviousPage() {
    return this.pagination.pageCurrent > 1;
  }

  <template>
    <div class='cui-paginator'>
      {{#if this.hasResults}}
        <div class='cui-paginator__totals'>
          {{#if @collection.isLoading}}
            <LoadingComponent />
          {{else}}
            {{this.totals}}
          {{/if}}
        </div>
        <div class='cui-paginator__limit'>
          {{t 'collection-ui.paginator.rows'}}
          <InputSelectComponent
            class='form-control collection__limit-selector'
            @options={{hash selectOptions=this.limitSelectOptions}}
            @value={{this.selectedLimit}}
            @valueChanged={{this.selectedLimitChanged}}
            @required={{true}}
          />
        </div>
      {{/if}}
      <div class='cui-paginator__pagination'>
        {{! PAGINATION }}
        <nav class='pagination__wrapper' ...attributes>
          <ul class='pagination'>
            <li class='pagination__item'>
              <button
                class='btn'
                type='button'
                disabled={{not this.hasPreviousPage}}
                aria-label={{t 'ember-mist-components.labels.previous'}}
                {{on 'click' this.previousPage}}
              >
                <IconComponent @name='chevron-left' />
              </button>
            </li>
            <li class='pagination__item'>
              <form {{on 'submit' this.desiredPageFormSubmitted}}>
                <InputNumberComponent
                  class='form-control pagination__selector'
                  @value={{this.desiredPage}}
                  @valueChanged={{this.desiredPageChanged}}
                  @required={{true}}
                  @disabled={{not this.hasResults}}
                  {{on 'blur' this.updateDesiredPage}}
                />
              </form>
            </li>
            <li class='pagination__item'>
              <button
                class='btn'
                type='button'
                disabled={{not this.hasNextPage}}
                aria-label={{t 'ember-mist-components.labels.next'}}
                {{on 'click' this.nextPage}}
              >
                <IconComponent @name='chevron-right' />
              </button>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </template>
}
