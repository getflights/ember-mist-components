import Component from '@glimmer/component';
import type UiService from '../services/ui';
import { service } from '@ember/service';

export interface IconSignature {
  Args: {
    name: string;
  };
  Element: HTMLElement;
}

export default class IconComponent extends Component<IconSignature> {
  @service declare ui: UiService;

  get iconComponent() {
    return this.ui.iconFor(this.args.name);
  }

  <template><this.iconComponent ...attributes /></template>
}
