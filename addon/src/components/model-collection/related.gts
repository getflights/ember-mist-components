import type { SyncHasMany } from '@ember-data/model';
import { service } from '@ember/service';
import { dasherize } from '@ember/string';
import type ModelInformationService from '../../services/model-information.ts';
import Condition, { Operator } from '../../query/Condition.ts';
import Query from '../../query/Query.ts';
import Component from '@glimmer/component';
import type ModelRegistry from 'ember-data/types/registries/model';
import type { Collection, ModelKey, SomeModel } from '../-base-collection.ts';
import ModelCollection from '../model-collection.gts';

type ExtractHasManyType<Rel> = Rel extends SyncHasMany<
  // @ts-ignore
  infer X extends SomeModel
>
  ? X
  : never;
type CollectionModel<M, F extends keyof M> = ExtractHasManyType<
  Exclude<M[F], undefined>
>;

export interface ModelCollectionRelatedSignature<
  K extends ModelKey,
  M extends ModelRegistry[K],
  F extends keyof M & string,
  R extends SomeModel = CollectionModel<M, F>,
> {
  Args: {
    // Required
    model: M;
    field: F;
    // List view
    modelListView?: string;
    // Optional: a query to copy from
    baseQuery?: Query<K>;
    // Optional: specific endpoint
    endpoint?: string;
    // List view id (for query)
    listViewId?: string;
    // Disabled (don't fetch)
    disabled?: boolean;
  };
  Blocks: {
    default: [collection: Collection<R>];
  };
}

export default class ModelCollectionRelatedComponent<
  K extends ModelKey,
  M extends ModelRegistry[K],
  F extends keyof M & string,
> extends Component<ModelCollectionRelatedSignature<K, M, F>> {
  @service declare modelInformation: ModelInformationService;

  get modelName() {
    return this.modelInformation.getHasManyModelName(
      this.modelInformation.getModelName(this.args.model)!,
      this.args.field,
    ) as ModelKey;
  }

  get inverseRelationship() {
    return this.modelInformation.getInverseRelationshipName(
      this.modelInformation.getModelName(this.args.model)!,
      this.args.field,
    );
  }

  get relatedQuery() {
    // 1. Make query for modelName
    const query = new Query(this.modelName);

    // 2. Copy stuff from baseQuery
    if (this.args.baseQuery) {
      query.copyFrom(this.args.baseQuery);
      // Make sure it has the correct modelName after copying
      query.setModelName(this.modelName);
    }

    // RELATED: inverse relationship condition
    if (this.inverseRelationship) {
      query.addCondition(
        new Condition(
          dasherize(this.inverseRelationship),
          Operator.EQUALS,
          undefined,
          // @ts-ignore
          this.args.model.id,
        ),
      );
    } else {
      query.addCondition(new Condition('dummy', Operator.EQUALS, 'dummy'));
    }

    return query;
  }

  <template>
    <ModelCollection
      @modelName={{this.modelName}}
      @baseQuery={{this.relatedQuery}}
      @endpoint={{@endpoint}}
      @modelListView={{@modelListView}}
      @listViewId={{@listViewId}}
      @disabled={{@disabled}}
      as |collection|
    >
      {{yield collection}}
    </ModelCollection>
  </template>
}
