import { assert } from '@ember/debug';
import { guidFor } from '@ember/object/internals';
import Component from '@glimmer/component';
import { tracked } from 'tracked-built-ins';
import { modifier } from 'ember-modifier';
import type { WithBoundArgs } from '@glint/template';
import { hash } from '@ember/helper';
import { Collapse } from 'bootstrap.native';
import { on } from '@ember/modifier';

export interface AccordionSignature {
  Element: HTMLDivElement;
  Blocks: {
    default: [
      { item: WithBoundArgs<typeof AccordionItemComponent, 'accordion'> },
    ];
  };
}

export default class AccordionComponent extends Component<AccordionSignature> {
  element!: HTMLDivElement;

  constructor(owner: any, args: any) {
    super(owner, args);

    this.id = `${guidFor(this)}-accordion`;
  }

  id;

  accordionModifier = modifier((element: HTMLDivElement) => {
    this.element = element;
  });

  <template>
    <div
      id={{this.id}}
      class='accordion'
      ...attributes
      {{this.accordionModifier}}
    >
      {{yield (hash item=(component AccordionItemComponent accordion=this))}}
    </div>
  </template>
}

interface AccordionItemSignature {
  Args: {
    accordion: AccordionComponent;
  };
  Blocks: {
    trigger: [];
    body: [];
  };
}

class AccordionItemComponent extends Component<AccordionItemSignature> {
  @tracked visible = false;

  constructor(owner: any, args: AccordionItemSignature['Args']) {
    super(owner, args);
    assert(`Accordion is required on Accorion Item`, args.accordion);

    const guid = guidFor(this);

    this.bodyId = `${guid}-body`;
    this.headingId = `${guid}-heading`;
    this.triggerId = `${guid}-trigger`;
  }

  bodyId;
  headingId;
  triggerId;

  @tracked bsCollapse?: Collapse;

  accordionItemModifier = modifier((element: HTMLButtonElement) => {
    const bootstrapDropdown = new Collapse(element, {
      parent: this.args.accordion.element,
    });
    this.bsCollapse = bootstrapDropdown;
    element.addEventListener('shown.bs.collapse', this.showAccordionListener);
    element.addEventListener('hidden.bs.collapse', this.hideAccordionListener);

    return () => {
      this.bsCollapse = undefined;
      element.removeEventListener(
        'shown.bs.collapse',
        this.showAccordionListener,
      );
      element.removeEventListener(
        'hidden.bs.collapse',
        this.hideAccordionListener,
      );
    };
  });

  toggle = () => {
    if (this.visible) {
      this.bsCollapse?.hide();
    } else {
      this.bsCollapse?.show();
    }
  };

  showAccordionListener = () => {
    this.visible = true;
  };

  hideAccordionListener = () => {
    this.visible = false;
  };

  <template>
    <div class='accordion-item'>
      <span class='accordion-header' id={{this.headingId}}>
        <button
          id={{this.triggerId}}
          class='accordion-button{{unless this.visible " collapsed"}}'
          type='button'
          data-bs-toggle='collapse'
          data-bs-target='#{{this.bodyId}}'
          aria-expanded={{this.visible}}
          aria-controls={{this.bodyId}}
          {{this.accordionItemModifier}}
          {{on 'click' this.toggle}}
        >
          {{yield to='trigger'}}
        </button>
      </span>
      <div
        id={{this.bodyId}}
        class='accordion-collapse collapse{{if this.visible " show"}}'
        aria-labelledby={{this.headingId}}
        data-bs-parent='#{{@accordion.id}}'
      >
        <div class='accordion-body accordion-item-body'>
          {{yield to='body'}}
        </div>
      </div>
    </div>
  </template>
}
