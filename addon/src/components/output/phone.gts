import Component from '@glimmer/component';
import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';
import IconComponent from '../icon.gts';
import { on } from '@ember/modifier';
import { service } from '@ember/service';
import type PhoneIntlService from '../../services/phone-intl';
import type PhoneService from '../../services/phone';
import { isNone } from '@ember/utils';

export interface OutputPhoneOptions {
  showButton?: boolean;
}

export interface OutputPhoneArguments extends BaseOutputArguments {
  value: string | undefined;
  options?: OutputPhoneOptions;
}

export interface OutputPhoneSignature {
  Args: OutputPhoneArguments;
  Element: HTMLSpanElement;
}

export default class OutputPhoneComponent extends Component<OutputPhoneSignature> {
  @service declare phoneIntl: PhoneIntlService;
  @service declare phone: PhoneService;

  get formattedNumber(): string | undefined {
    return this.phoneIntl.formatNumber(this.args.value);
  }

  get showButton() {
    if (!this.phone.clickToDialEnabled) {
      return false;
    }

    if (this.args.options?.showButton !== undefined) {
      return this.args.options.showButton;
    }

    return !!this.args.value;
  }

  clickToDial = async () => {
    await this.phone.clickToDial.perform(this.args.value!);
  };

  <template>
    {{#let (isNone @value) as |isEmpty|}}
      <span
        class='output output-phone{{if isEmpty " output--empty"}}'
        ...attributes
      >
        {{#unless isEmpty}}
          <span class='output-phone__number'>
            {{this.formattedNumber}}
          </span>
          {{#if this.showButton}}
            <button
              type='button'
              class='btn btn-default output-phone__dial'
              {{on 'click' this.clickToDial}}
            >
              <IconComponent @name='phone' />
            </button>
          {{/if}}
        {{/unless}}
      </span>
    {{/let}}
  </template>
}
