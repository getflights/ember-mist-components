import { isNone } from '@ember/utils';
import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';
import Component from '@glimmer/component';

export interface OutputTextMaskOptions {
  mask: string;
  regex: RegExp;
}

export interface OutputTextMaskArguments extends BaseOutputArguments {
  value: string | undefined;
  options: OutputTextMaskOptions;
}

export interface OutputTextMaskSignature {
  Args: OutputTextMaskArguments;
  Element: HTMLSpanElement;
}

export default class OutputTextMaskComponent extends Component<OutputTextMaskSignature> {
  private pattern = /\{\{(\d+)\}\}/g;

  get value() {
    if (isNone(this.args.value)) {
      return undefined;
    }

    const matchedValue = this.args.value.match(this.args.options.regex);

    let maskedValue = this.args.options.mask;
    let match: RegExpExecArray | null = null;
    while ((match = this.pattern.exec(this.args.options.mask))) {
      if (match) {
        const matchedIndex = parseInt(match[1]!);
        maskedValue = maskedValue.replace(
          match[0],
          matchedValue?.[matchedIndex] ?? '',
        );
      }
    }

    return maskedValue;
  }

  <template>
    <span
      class='output output-text-mask{{if (isNone this.value) " output--empty"}}'
      ...attributes
    >
      {{this.value}}
    </span>
  </template>
}
