import type { TOC } from '@ember/component/template-only';
import { get } from '@ember/helper';
import { isNone } from '@ember/utils';
import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';
import eq from 'ember-truth-helpers/helpers/eq';
import lt from 'ember-truth-helpers/helpers/lt';
import or from 'ember-truth-helpers/helpers/or';

export interface OutputStringsOptions {}

export interface OutputStringsArguments extends BaseOutputArguments {
  value: string[] | undefined;
}

export interface OutputStringsSignature {
  Args: OutputStringsArguments;
  Element: HTMLSpanElement;
}

const OutputStringsComponent: TOC<OutputStringsSignature> = <template>
  {{#let (or (isNone @value) (lt @value.length 1)) as |isNoneOrEmpty|}}
    <span
      class='output output-strings{{if isNoneOrEmpty " output--empty"}}'
      ...attributes
    >
      {{#unless isNoneOrEmpty}}
        {{#if (eq @value.length 1)}}
          {{get @value 0}}
        {{else}}
          <ul class='output-strings__list'>
            {{#each @value as |singleValue|}}
              <li class='output-strings__list-item'>
                {{singleValue}}
              </li>
            {{/each}}
          </ul>
        {{/if}}
      {{/unless}}
    </span>
  {{/let}}
</template>;

export default OutputStringsComponent;
