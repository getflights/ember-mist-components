import Component from '@glimmer/component';
import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';
import { service } from '@ember/service';
import type AddressService from '../../services/address';
import LoadingComponent from '../loading.gts';
import type { InputSelectOptions } from '@getflights/ember-field-components/components/input/select';
import OutputSelectComponent from '@getflights/ember-field-components/components/output/select';

export interface OutputCountryCodeOptions {
  showButton?: boolean;
}

export interface OutputCountryCodeArguments extends BaseOutputArguments {
  value: string | undefined;
  options?: OutputCountryCodeOptions;
}

export interface OutputCountryCodeSignature {
  Args: OutputCountryCodeArguments;
  Element: HTMLSpanElement;
}

export default class OutputCountryCodeComponent extends Component<OutputCountryCodeSignature> {
  @service declare address: AddressService;

  get selectOptions() {
    return this.address.countrySelectOptions;
  }

  get isLoading() {
    return this.address.countrySelectOptionsRequest.isLoading;
  }

  get options(): InputSelectOptions {
    return {
      selectOptions: this.selectOptions ?? [],
    };
  }

  <template>
    {{#if this.isLoading}}
      <span class='output output-country-code' ...attributes>
        <span class='output-country-code__loading'>
          <LoadingComponent />
        </span>
      </span>
    {{else}}
      <OutputSelectComponent
        @value={{@value}}
        @options={{this.options}}
        ...attributes
      />
    {{/if}}
  </template>
}
