import Component from '@glimmer/component';
import IconComponent from '../icon.gts';
import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';
import { isNone } from '@ember/utils';

export interface OutputUrlOptions {
  showButton?: boolean;
}

export interface OutputUrlArguments extends BaseOutputArguments {
  value: string | undefined;
  options?: OutputUrlOptions;
}

export interface OutputUrlSignature {
  Args: OutputUrlArguments;
  Element: HTMLSpanElement;
}

export default class OutputUrlComponent extends Component<OutputUrlSignature> {
  get showButton() {
    if (this.args.options?.showButton !== undefined) {
      return this.args.options.showButton;
    }

    return !!this.args.value;
  }

  <template>
    <span
      class='output output-url{{if (isNone @value) " output--empty"}}'
      ...attributes
    >
      {{#unless (isNone @value)}}
        {{@value}}
        {{#if this.showButton}}
          <a
            class='output-url__button btn btn-default'
            href={{@value}}
            target='_blank'
            rel='noopener noreferrer'
          >
            <IconComponent @name='new-window' />
          </a>
        {{/if}}
      {{/unless}}
    </span>
  </template>
}
