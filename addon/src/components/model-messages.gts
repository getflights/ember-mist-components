import Component from '@glimmer/component';
import Model from '@ember-data/model';
import { service } from '@ember/service';
import type FieldInformationService from '@getflights/ember-field-components/services/field-information';

interface ModelMessagesSignature {
  Args: {
    /**
     * The Model you want to display the error messages from
     */
    model: Model;

    /**
     * If this flag is true, all the errors for every field + the errors not specific to a certain field will be displayed.
     */
    displayAll?: boolean;
  };
}

/**
 * This component displays either the errors not related to a specific field on a provided model, or all the errors on the model, depending on the flag displayAll (default=false)
 */
export default class ModelMessagesComponent extends Component<ModelMessagesSignature> {
  @service declare fieldInformation: FieldInformationService;

  get errors(): string[] {
    if (!this.args.displayAll) {
      return this.args.model.errors
        .errorsFor('base')
        .map(({ message }) => message);
    }

    return this.args.model.errors.toArray().map(({ attribute, message }) => {
      if (attribute === 'base') {
        return message;
      }
      return `${this.fieldInformation.getTranslatedFieldLabel(
        this.args.model,
        attribute,
      )}: ${message}`;
    });
  }

  <template>
    {{#each this.errors as |error|}}
      <div class='alert alert-danger' role='alert'>
        {{error}}
      </div>
    {{/each}}
  </template>
}
