import type Store from '@ember-data/store';
import { service } from '@ember/service';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';
import ListViewService from '../services/list-view.ts';
import Component from '@glimmer/component';
import type IntlService from 'ember-intl/services/intl';
import { tracked } from 'tracked-built-ins';
import UiService from '../services/ui.ts';
import type { WithBoundArgs } from '@glint/template';
import InputSelectComponent, {
  type InputSelectOptions,
} from '@getflights/ember-field-components/components/input/select';
import { trackedFunction } from 'reactiveweb/function';
import Condition from '../query/Condition.ts';
import Query from '../query/Query.ts';
import { isBlank } from '@ember/utils';
import { action } from '@ember/object';
import { LoadingSelect } from './input-field/select.gts';

export interface ListViewSelectArguments {
  grouping: string;
  valueChanged?: (id: string | undefined) => void;
}

export interface ListViewSelectSignature<YieldInput = true> {
  Args: ListViewSelectArguments;
  Blocks: {
    default?: YieldInput extends true
      ? [
          id: string | undefined,
          element: WithBoundArgs<
            typeof InputSelectComponent,
            'value' | 'valueChanged' | 'options' | 'required'
          >,
        ]
      : [id: string | undefined];
  };
  Element: HTMLElement;
}

export default class ListViewSelectComponent<
  YieldInput = true,
> extends Component<ListViewSelectSignature<YieldInput>> {
  @service declare store: Store;
  @service declare intl: IntlService;
  @service declare listView: ListViewService;
  @service declare ui: UiService;

  constructor(owner: any, args: any) {
    super(owner, args);

    this.selectedListViewId = this.listView.getActiveListViewIdForGrouping(
      this.args.grouping,
    );
  }

  protected listViewModelsForGroupingRequest = trackedFunction(
    this,
    async () => {
      const query = new Query('list-view');
      query.addCondition(new Condition('grouping', '=', this.args.grouping));
      query.addInclude('sort_orders');
      query.addInclude('sort_orders.field');
      query.setLimit(50);

      return (await query.fetch(this.store)).toArray();
    },
  );

  get selectOptionsLoading() {
    return this.listViewModelsForGroupingRequest.isLoading;
  }

  get selectOptions(): SelectOption[] {
    return (
      this.listViewModelsForGroupingRequest.value?.map((listViewModel) => {
        return {
          value: listViewModel.id,
          label: listViewModel.name,
        };
      }) ?? []
    );
  }

  @tracked selectedListViewId: string | undefined;

  @action
  setNewListView(id: string | undefined) {
    this.selectedListViewId = isBlank(id) ? undefined : id;
    this.listView.setActiveListViewIdForGrouping(
      this.args.grouping,
      this.selectedListViewId,
    );
    this.args.valueChanged?.(this.selectedListViewId);
  }

  get inputSelectOptions(): InputSelectOptions {
    return {
      selectOptions: this.selectOptions,
      placeholder: this.intl.t('label.all'),
    };
  }

  <template>
    {{#let
      (component
        InputSelectComponent
        value=this.selectedListViewId
        valueChanged=this.setNewListView
        options=this.inputSelectOptions
        required=false
      )
      as |BoundInput|
    }}
      {{#if (has-block)}}
        {{#if this.selectOptionsLoading}}
          <LoadingSelect ...attributes />
        {{else}}
          {{! @glint-ignore }}
          {{yield this.selectedListViewId BoundInput}}
        {{/if}}
      {{else}}
        {{#if this.selectOptionsLoading}}
          <LoadingSelect ...attributes />
        {{else}}
          <BoundInput class='list-view' ...attributes />
        {{/if}}
      {{/if}}
    {{/let}}
  </template>
}
