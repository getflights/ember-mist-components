import Component from '@glimmer/component';
import { service } from '@ember/service';
import ToastService from '../services/toast.ts';
import type ToastMessage from '../objects/toast-message.ts';
import { tracked } from 'tracked-built-ins';
import { guidFor } from '@ember/object/internals';
import { Toast } from 'bootstrap.native';
import { modifier } from 'ember-modifier';
import { MessageType } from '../objects/toast-message.ts';
import { on } from '@ember/modifier';
import IconComponent from './icon.gts';

export interface ToastContainerSignature {}

export default class ToastContainerComponent extends Component<ToastContainerSignature> {
  @service declare toast: ToastService;

  <template>
    {{#if this.toast.toasts}}
      <div aria-live='polite' aria-atomic='true' class='toast-container'>
        {{#each this.toast.toasts as |message|}}
          <ToastMessageComponent @toast={{message}} />
        {{/each}}
      </div>
    {{/if}}
  </template>
}

class ToastMessageComponent extends Component<{
  Args: {
    toast: ToastMessage;
  };
  Element: HTMLDivElement;
}> {
  @service('toast') declare toastService: ToastService;

  constructor(owner: any, args: any) {
    super(owner, args);

    this.toastId = `${guidFor(this)}-toast`;
  }

  @tracked bsToast?: Toast;

  toastId;

  toastMessageModifier = modifier((element: HTMLDivElement) => {
    const bootstrapToast = new Toast(element, {
      animation: true,
      autohide: false,
    });
    bootstrapToast.show();
    this.bsToast = bootstrapToast;

    return () => {
      this.bsToast?.hide();
      this.bsToast = undefined;
    };
  });

  get class(): string {
    const classes: string[] = [];
    classes.push('toast');

    if (this.args.toast.type === MessageType.SUCCESS) {
      classes.push('toast-success');
    } else if (this.args.toast.type === MessageType.ERROR) {
      classes.push('toast-error');
    } else if (this.args.toast.type === MessageType.INFO) {
      classes.push('toast-info');
    } else if (this.args.toast.type === MessageType.WARNING) {
      classes.push('toast-warning');
    }

    return classes.join(' ');
  }

  get ariaLive(): string {
    if (this.args.toast.type === MessageType.ERROR) {
      return 'assertive';
    } else {
      return 'polite';
    }
  }

  close = () => {
    this.toastService.toasts.delete(this.args.toast);
  };

  <template>
    <div
      id={{this.toastId}}
      class={{this.class}}
      role='alert'
      aria-live={{this.ariaLive}}
      aria-atomic='true'
      {{this.toastMessageModifier}}
      ...attributes
    >
      <div class='toast-header'>
        <strong class='toast-subject'>
          {{@toast.subject}}
        </strong>
        {{! <small>11 mins ago</small> }}
        <button
          type='button'
          class='btn btn-icon close'
          aria-label='Close'
          {{on 'click' this.close}}
        >
          <IconComponent @name='close' />
        </button>
      </div>
      {{#if @toast.message}}
        <div class='toast-body'>
          {{@toast.message}}
        </div>
      {{/if}}
    </div>
  </template>
}
