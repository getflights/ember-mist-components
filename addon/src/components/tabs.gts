import { assert } from '@ember/debug';
import Component from '@glimmer/component';
import { service } from '@ember/service';
import TabsService from '../services/tabs.ts';
import type { WithBoundArgs } from '@glint/template';
import Portal from 'ember-stargate/components/portal';
import { hash } from '@ember/helper';
import PortalTarget from 'ember-stargate/components/portal-target';
import { guidFor } from '@ember/object/internals';
import { on } from '@ember/modifier';

interface TabsYield {
  component: TabsComponent;
  activeTab: string;
  Body: WithBoundArgs<typeof Portal, 'target'>;
  Tab: WithBoundArgs<typeof TabComponent, 'tabs'>;
}

export interface TabsSignature {
  Args: {
    default: string;
    id: string;
    onChange?: (newTab: string) => void;
  };
  Blocks: {
    default: [tabsInfo: TabsYield];
  };
}

export default class TabsComponent extends Component<TabsSignature> {
  @service declare tabs: TabsService;

  constructor(owner: any, args: TabsSignature['Args']) {
    super(owner, args);
    assert(`Tabs requires a @default and @id`, args.default && args.id);

    this.portalName = `${guidFor(this)}-accordion`;
  }

  portalName;

  get activeTab(): string {
    return this.tabs.getSelectedTabFor(this.args.id) ?? this.args.default;
  }

  setActiveTab(activeTab: string): void {
    this.tabs.setSelectedTabFor(this.args.id, activeTab);
    this.args.onChange?.(activeTab);
  }

  <template>
    <ul class='nav nav-tabs' role='tablist'>
      {{yield
        (hash
          component=this
          activeTab=this.activeTab
          Body=(component Portal target=this.portalName)
          Tab=(component TabComponent tabs=this)
        )
      }}
    </ul>

    <PortalTarget @name={{this.portalName}} />
  </template>
}

interface TabSignature {
  Args: {
    tabs: TabsComponent;
    id: string;
    disabled?: boolean;
  };
  Blocks: {
    default: [];
  };
  Element: HTMLButtonElement;
}

class TabComponent extends Component<TabSignature> {
  constructor(owner: any, args: TabSignature['Args']) {
    super(owner, args);

    assert(`Tab required at least @id and @tabs`, args.id && args.tabs);
  }

  get isActive(): boolean {
    return this.args.tabs.activeTab === this.args.id;
  }

  setActive = () => {
    this.args.tabs.setActiveTab(this.args.id);
  };

  <template>
    <li class={{if this.isActive 'nav-item active' 'nav-item'}}>
      {{! template-lint-disable require-context-role }}
      <button
        type='button'
        role='tab'
        class='nav-link{{if @disabled " disabled"}}'
        data-bs-toggle='tab'
        aria-disabled={{@disabled}}
        aria-expanded={{this.isActive}}
        {{on 'click' this.setActive}}
        data-test-tab={{@id}}
        ...attributes
      >
        {{yield}}
      </button>
    </li>
  </template>
}
