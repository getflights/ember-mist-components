import Component from '@glimmer/component';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import GfInputVatRateComponent, {
  type GfInputVatRateOptions,
  type GfInputVatRateSignature,
} from '../gf-input/vat-rate.gts';

export default class InputFieldVatRateComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F, GfInputVatRateOptions>;
  Element: GfInputVatRateSignature<boolean>['Element'];
}> {
  get inputFieldArgs() {
    return this.args;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      {{! @glint-ignore }}
      @inputComponent={{GfInputVatRateComponent}}
      @fieldType='vat-rate'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
