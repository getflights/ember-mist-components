import Component from '@glimmer/component';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import { service } from '@ember/service';
import type ModelInformationService from '../../services/model-information';
import GfInputMultiComponent from '../gf-input/multi.gts';
import OutputDrupalImageComponent from '../output/drupal-image.gts';
import GfInputDrupalFileComponent, {
  type GfInputDrupalFileOptions,
} from '../gf-input/drupal-file.gts';
import type DrupalImage from '../../interfaces/drupal-image';
import type DrupalFile from '../../interfaces/drupal-file';
import { hash } from '@ember/helper';

interface InputDrupalImagesOptions extends GfInputDrupalFileOptions<false> {
  style?: string;
}

interface InputDrupalImagesArguments {
  value: DrupalImage[];
  valueChanged: (newValue: DrupalImage[]) => void;
  options: InputDrupalImagesOptions;
}

interface InputDrupalImagesSignature {
  Args: InputDrupalImagesArguments;
}

class InputDrupalImages extends Component<InputDrupalImagesSignature> {
  removeItem = (index: number) => {
    const newValue = [...this.args.value];
    newValue.splice(index, 1);
    this.args.valueChanged(newValue);
  };

  reorderItems = (reorderedItems: DrupalImage[]) => {
    this.args.valueChanged([...reorderedItems]);
  };

  addNewImage = (image: DrupalFile | undefined) => {
    if (image) {
      this.args.valueChanged([...this.args.value, image as DrupalImage]);
    }
  };

  get inputOptions() {
    return {
      ...this.args.options,
      multiple: false as const,
    };
  }

  valueFor = (index: number) => {
    return this.args.value[index];
  };

  <template>
    <div class='gf-input gf-input-drupal-images'>
      <GfInputMultiComponent
        @value={{@value}}
        @removeItem={{this.removeItem}}
        @reorderItems={{this.reorderItems}}
        @hideNew={{true}}
        as |index|
      >
        <div class='gf-input-drupal-images__image'>
          <OutputDrupalImageComponent
            @value={{this.valueFor index}}
            @options={{if @options.style (hash style=@options.style)}}
          />
        </div>
      </GfInputMultiComponent>

      <GfInputDrupalFileComponent
        @options={{this.inputOptions}}
        @valueChanged={{this.addNewImage}}
      />
    </div>
  </template>
}

export default class InputFieldImagesComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F>;
  Element: HTMLInputElement;
}> {
  @service declare modelInformation: ModelInformationService;

  get inputFieldArgs() {
    return {
      ...this.args,
      inputOptions: {
        ...this.args.inputOptions,
        field: this.args.field,
        // @ts-ignore
        modelName: this.modelInformation.getModelName(this.args.model),
      },
    };
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      {{! @glint-ignore }}
      @inputComponent={{InputDrupalImages}}
      @fieldType='images'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
