import Component from '@glimmer/component';
import InputDateComponent, {
  type InputDateOptions,
} from '../gf-input/date.gts';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';

export default class InputFieldDateComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F, InputDateOptions<false>>;
  Element: HTMLInputElement;
}> {
  get inputFieldArgs() {
    const args = { ...this.args };

    // Range must be false
    args.inputOptions = {
      ...args.inputOptions,
      range: false,
    };

    return args;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      @inputComponent={{InputDateComponent}}
      @fieldType='date'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
