import Component from '@glimmer/component';
import InputTimeComponent, {
  type GfInputTimeOptions,
} from '../gf-input/time.gts';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';

export default class InputFieldTimeComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F, GfInputTimeOptions>;
  Element: HTMLInputElement;
}> {
  get inputFieldArgs() {
    return this.args;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      @inputComponent={{InputTimeComponent}}
      @fieldType='time'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
