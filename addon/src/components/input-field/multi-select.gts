import Component from '@glimmer/component';
import InputMultiSelectComponent from '@getflights/ember-field-components/components/input/multi-select';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import type { InputMultiSelectOptions } from '@getflights/ember-field-components/components/input/multi-select';
import { service } from '@ember/service';
import type { IntlService } from 'ember-intl';
import { LoadingSelect } from './select.gts';
import { use } from 'ember-resources';
import FieldSelectHelperResource from '../../resources/field-select-helper.ts';

export default class InputFieldMultiSelectComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F, InputMultiSelectOptions>;
  Element: HTMLInputElement;
}> {
  @service declare intl: IntlService;

  private helperResource = use(
    this,
    FieldSelectHelperResource(() => ({
      model: this.args.model,
      field: this.args.field,
      selectOptions: this.args.inputOptions?.selectOptions,
    })),
  );

  get helper() {
    return this.helperResource.current;
  }

  get inputFieldArgs() {
    const args = {
      ...this.args,
      inputOptions: {
        placeholder: this.helper.placeholder,
        ...this.args.inputOptions,
        selectOptions: this.helper.selectOptions,
      },
    };

    if (!args.inputOptions?.selectOptions) {
      args.inputOptions.selectOptions = this.helper.asyncSelectOptions ?? [];

      if (this.helper.isError) {
        args.inputOptions.placeholder = this.intl.t(
          'input-field.select.could_not_load',
        );
      }
    }

    return args;
  }

  get inputComponent() {
    if (!this.args.inputOptions?.selectOptions && this.helper.isLoading) {
      return LoadingSelect;
    }

    return InputMultiSelectComponent;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      {{! @glint-ignore }}
      @inputComponent={{this.inputComponent}}
      @fieldType='select'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
