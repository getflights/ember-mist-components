import Component from '@glimmer/component';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import { service } from '@ember/service';
import type ModelInformationService from '../../services/model-information';
import GfInputMultiComponent from '../gf-input/multi.gts';
import InputTextComponent from '@getflights/ember-field-components/components/input/text';
import { tracked } from 'tracked-built-ins';
import { fn } from '@ember/helper';
import type { BaseOutputArguments } from '@getflights/ember-field-components/components/output/-base';

interface InputStringsArguments extends BaseOutputArguments {
  value: string[] | undefined;
  valueChanged: (newValue: string[]) => void;
}

interface InputStringsSignature {
  Args: InputStringsArguments;
}

class InputStrings extends Component<InputStringsSignature> {
  sync(newValue: string[]) {
    this.args.valueChanged(tracked(newValue));
  }

  removeItem = (index: number) => {
    const newValue = [...(this.args.value ?? [])];
    newValue.splice(index, 1);
    this.sync(newValue);
  };

  itemChanged = (index: number, value: string) => {
    this.args.value![index] = value;
  };

  reorderItems = (reorderedItems: string[]) => {
    this.sync(reorderedItems);
  };

  addNewItem = () => {
    this.sync([...(this.args.value ?? []), '']);
  };

  valueFor = (index: number) => {
    return this.args.value?.[index];
  };

  <template>
    <div class='gf-input gf-input-strings'>
      <GfInputMultiComponent
        @value={{@value}}
        @addNewItem={{this.addNewItem}}
        @removeItem={{this.removeItem}}
        @reorderItems={{this.reorderItems}}
        @hideNew={{false}}
        as |index|
      >
        <InputTextComponent
          @value={{this.valueFor index}}
          @valueChanged={{fn this.itemChanged index}}
          class='form-control'
        />
      </GfInputMultiComponent>
    </div>
  </template>
}

export default class InputFieldStringsComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F>;
  Element: HTMLInputElement;
}> {
  @service declare modelInformation: ModelInformationService;

  get inputFieldArgs() {
    return {
      ...this.args,
      inputOptions: {
        ...this.args.inputOptions,
        field: this.args.field,
        // @ts-ignore
        modelName: this.modelInformation.getModelName(this.args.model),
      },
    };
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      {{! @glint-ignore }}
      @inputComponent={{InputStrings}}
      @fieldType='strings'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
