import Component from '@glimmer/component';
import InputSelectComponent from '@getflights/ember-field-components/components/input/select';
import InputFieldTemplate, {
  type InputFieldArguments,
} from '@getflights/ember-field-components/components/input-field/-base';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import Model from '@ember-data/model';
import type { InputSelectOptions } from '@getflights/ember-field-components/components/input/select';
import { service } from '@ember/service';
import type ModelInformationService from '../../services/model-information';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';
import type SelectOptionGroup from '@getflights/ember-field-components/interfaces/select-option-group';
import { cached } from '@glimmer/tracking';
import { isGroup } from '@getflights/ember-field-components/interfaces/select-option-group';

export interface InputDependentSelectOptions extends InputSelectOptions {
  dependentField: string;
  dependencies: Map<string, string[]>;
}

export default class InputFieldDependentSelectComponent<
  O extends SomeModel,
  F extends FieldOf<O>,
> extends Component<{
  Args: InputFieldArguments<O, F, InputDependentSelectOptions>;
  Element: HTMLInputElement;
}> {
  @service declare modelInformation: ModelInformationService;

  @cached
  get modelName() {
    return this.modelInformation.getModelName(
      this.args.model as unknown as Model,
    )!;
  }

  get placeholder() {
    return this.modelInformation.getTranslatedSelectNoneLabel(
      this.modelName,
      this.args.field,
    );
  }

  get selectOptions() {
    const selectOptions: (SelectOption | SelectOptionGroup)[] = [
      ...this.args.inputOptions!.selectOptions,
    ];

    const dependencyValue =
      this.args.model[this.args.inputOptions!.dependentField];

    const allowedSelectOptions = selectOptions.filter((option) => {
      if (!isGroup(option)) {
        return (
          this.args
            .inputOptions!.dependencies.get(dependencyValue)
            ?.includes(option.value) ?? true
        );
      }

      return true;
    });

    return allowedSelectOptions.map((option) => {
      if (!isGroup(option) && !option.label) {
        return {
          ...option,
          label: this.modelInformation.getTranslatedSelectOptionLabel(
            this.modelName,
            this.args.field,
            option.value,
          ),
        };
      }

      return option;
    });
  }

  get inputFieldArgs() {
    const args = {
      ...this.args,
      inputOptions: {
        placeholder: this.placeholder,
        ...this.args.inputOptions,
        selectOptions: this.selectOptions,
      },
    };

    return args;
  }

  <template>
    <InputFieldTemplate
      @inputFieldArgs={{this.inputFieldArgs}}
      {{! @glint-ignore }}
      @inputComponent={{InputSelectComponent}}
      @fieldType='select'
      {{! @glint-ignore }}
      ...attributes
    />
  </template>
}
