import Component from '@glimmer/component';
import {
  type Placement,
  type Strategy,
  type OffsetOptions,
  type InlineOptions,
  type ShiftOptions,
  type ArrowOptions,
  type SizeOptions,
  type FlipOptions,
  type AutoPlacementOptions,
  type HideOptions,
  autoUpdate,
  offset,
  inline,
  shift,
  size,
  flip,
  autoPlacement,
  arrow,
  hide,
  computePosition,
  type Middleware,
} from '@floating-ui/dom';
import { modifier } from 'ember-modifier';
import { tracked } from 'tracked-built-ins';
import type { TOC } from '@ember/component/template-only';
import type { ComponentLike, ModifierLike } from '@glint/template';

export type FloatingUiOptions = {
  placement?: Placement | undefined;
  strategy?: Strategy | undefined;
  /**
   * Placement modifier: `offset` modifies the placement to add distance or margin between the reference and floating element.
   */
  offset?: OffsetOptions | undefined;
  /**
   * Placement modifier: `inline` positions the floating element relative to individual client rects rather than the bounding box for better precision.
   */
  inline?: boolean | InlineOptions | undefined;
  /**
   * Visibility Optimizer: `shift` prevents the floating element from overflowing a clipping container by shifting it to stay in view.
   */
  shift?: boolean | ShiftOptions | undefined;
  /**
   * Visibility Optimizer: `size` resizes the floating element, for example so it will not overflow a clipping container, or to match the width of the reference element.
   */
  size?: SizeOptions | undefined;
  /**
   * Visibility Optimizer: `flip` prevents the floating element from overflowing a clipping container by flipping it to the opposite placement to stay in view.
   * This is al alternative to `autoPlacement` and only one of either can be used. When providing both, we will use only `autoPlacement`.
   */
  flip?: boolean | FlipOptions | undefined;
  /**
   * Visibility Optimizer: `autoPlacement` automatically chooses a placement for you using a “most space” strategy.
   * This is al alternative to `flip` and only one of either can be used. When providing both, we will use only `autoPlacement`.
   */
  autoPlacement?: boolean | AutoPlacementOptions | undefined;
  /**
   * `arrow` adds an arrow to the floating element at the correct position.
   */
  arrow?: boolean | Omit<ArrowOptions, 'element'> | undefined;
  /**
   * `hide` hides the floating element in applicable situations when it no longer appears attached to its reference element due to different clipping contexts.
   */
  hide?: boolean | HideOptions | undefined;
};

function optionsOrEmptyObject<O extends object>(
  value: O | any,
): O | Record<string, never> {
  if (typeof value === 'object') {
    return value;
  }

  return {};
}

export interface FloatingSignature {
  Args: {
    /**
     * Type of floating component.
     * Popover: on click (default)
     * Tooltip: on hover, focus...
     */
    type?: 'popover' | 'tooltip';
    disabled?: boolean;
    options?: Omit<FloatingUiOptions, 'flip' | 'autoPlacement'> &
      (
        | Pick<FloatingUiOptions, 'flip'>
        | Pick<FloatingUiOptions, 'autoPlacement'>
      );
    becameVisible?: () => void;
    becameHidden?: () => void;
  };
  Blocks: {
    default: [
      referenceModifier: ModifierLike<{ Element: HTMLElement }>,
      FloatingElement: ComponentLike<Omit<FloatingElementSignature, 'Args'>>,
      toggle: (newState?: boolean) => void,
    ];
  };
  Element: HTMLDivElement;
}

export default abstract class FloatingComponent extends Component<FloatingSignature> {
  get options(): Partial<FloatingUiOptions> {
    const userOptions = this.args.options ?? {};

    return {
      /**
       * Defaults
       */
      placement: 'bottom-start',
      shift: true,
      flip: 'autoPlacement' in userOptions ? false : true,
      arrow: true,
      /**
       * Floating tooltip should be at least the width of the button
       */
      size: {
        apply({ rects, elements }) {
          Object.assign(elements.floating.style, {
            'min-width': `${rects.reference.width}px`,
          });
        },
      },
      // User options
      ...userOptions,
    };
  }

  /**
   * Modifiers
   */
  floatingElement?: HTMLDivElement;

  /**
   * This modifier is called whenever the floating element gets inserted (a.k.a. becomes visible).
   */
  floatingElementModifier = modifier((floatingElement: HTMLDivElement) => {
    if (!this.referenceElement) {
      return;
    }

    this.floatingElement = floatingElement;

    const cleanup = autoUpdate(
      this.referenceElement,
      this.floatingElement,
      this.updatePosition,
    );

    this.args.becameVisible?.();

    const documentClickHandler = (e: MouseEvent) => {
      const isInside = Boolean(
        e.target === floatingElement ||
          floatingElement.contains(e.target as HTMLElement),
      );

      // Handle click outside
      if (!isInside) {
        this.toggleFloatingElement(false);
      }
    };

    if (this.isPopover) {
      document.addEventListener('click', documentClickHandler);
    }

    return () => {
      document.removeEventListener('click', documentClickHandler);
      cleanup();
      this.args.becameHidden?.();
    };
  });

  referenceElement?: HTMLElement;

  referenceModifier = modifier((referenceElement: HTMLElement) => {
    this.referenceElement = referenceElement;

    referenceElement.classList.add('floating__reference');

    const hasDisabledAttribute = referenceElement.hasAttribute('disabled');
    if (this.args.disabled && !hasDisabledAttribute) {
      referenceElement.setAttribute('disabled', '');
    }

    const handlers: [string, (e: any) => void][] = this.isPopover
      ? [
          [
            'click',
            (e: MouseEvent) => {
              // Don't propagate to window event listener
              e.stopPropagation();
              this.toggleFloatingElement();
            },
          ],
        ]
      : [
          ['pointerenter', () => this.toggleFloatingElement(true)],
          ['pointerleave', () => this.toggleFloatingElement(false)],
          ['focus', () => this.toggleFloatingElement(true)],
          ['blur', () => this.toggleFloatingElement(false)],
        ];

    handlers.forEach(([event, listener]) => {
      referenceElement.addEventListener(event, listener);
    });

    return () => {
      referenceElement.classList.remove('floating__reference');
      if (!hasDisabledAttribute) {
        referenceElement.removeAttribute('disabled');
      }

      handlers.forEach(([event, listener]) => {
        referenceElement.removeEventListener(event, listener);
      });
    };
  });

  @tracked floatingElementVisible = false;

  toggleFloatingElement = (
    newState: boolean = !this.floatingElementVisible,
  ) => {
    if (newState === this.floatingElementVisible) {
      // Already visible or hidden
      return;
    }

    if (this.args.disabled) {
      if (!this.floatingElementVisible) {
        return;
      } else {
        // Always force it to hide when it is disabled
        newState = false;
      }
    }

    this.floatingElementVisible = newState;
  };

  private updatePosition = () => {
    if (!this.floatingElementVisible) {
      return;
    }

    if (!this.referenceElement || !this.floatingElement) {
      // Error: invalid floating ui component: no reference element found
      return;
    }

    const middleware: Middleware[] = [];

    if (this.options.offset) {
      middleware.push(offset(this.options.offset));
    }

    if (this.options.inline) {
      middleware.push(inline(optionsOrEmptyObject(this.options.inline)));
    }

    if (this.options.shift) {
      middleware.push(shift(optionsOrEmptyObject(this.options.shift)));
    }

    if (this.options.size) {
      middleware.push(size(this.options.size));
    }

    if (this.options.autoPlacement) {
      middleware.push(
        autoPlacement(optionsOrEmptyObject(this.options.autoPlacement)),
      );
    } else if (this.options.flip) {
      middleware.push(flip(optionsOrEmptyObject(this.options.flip)));
    }

    if (this.options.autoPlacement) {
      middleware.push(flip(optionsOrEmptyObject(this.options.autoPlacement)));
    }

    if (this.options.arrow && this.arrowElement) {
      middleware.push(
        arrow({
          ...optionsOrEmptyObject(this.options.arrow),
          element: this.arrowElement,
        }),
      );
    }

    if (this.options.hide) {
      middleware.push(hide(optionsOrEmptyObject(this.options.hide)));
    }

    computePosition(this.referenceElement, this.floatingElement, {
      placement: this.options.placement,
      middleware,
    }).then(({ x, y, placement, middlewareData }) => {
      Object.assign(this.floatingElement!.style, {
        left: `${x}px`,
        top: `${y}px`,
      });

      // ARROW
      if (middlewareData.arrow) {
        const { x: arrowX, y: arrowY } = middlewareData.arrow;

        const staticSide = {
          top: 'bottom',
          right: 'left',
          bottom: 'top',
          left: 'right',
        }[placement.split('-')[0]!]!;

        Object.assign(this.arrowElement!.style, {
          left: arrowX != null ? `${arrowX}px` : '',
          top: arrowY != null ? `${arrowY}px` : '',
          right: '',
          bottom: '',
          [staticSide]: '-4px',
        });
      }

      // HIDE
      if (middlewareData.hide) {
        Object.assign(this.floatingElement!.style, {
          visibility: middlewareData.hide.referenceHidden
            ? 'hidden'
            : 'visible',
        });
      }
    });
  };

  get isPopover() {
    return !this.args.type || this.args.type === 'popover';
  }

  get isTooltip() {
    return this.args.type === 'tooltip';
  }

  arrowElement?: HTMLElement;

  arrowModifier = modifier((arrowElement: HTMLDivElement) => {
    if (this.options.arrow) {
      this.arrowElement = arrowElement;
    } else {
      // Remove from DOM
      arrowElement.remove();
    }
  });

  get shouldRenderArrow() {
    return !!this.options.arrow;
  }

  <template>
    {{#let
      (component
        FloatingElement
        modifier=this.floatingElementModifier
        visible=this.floatingElementVisible
        toggle=this.toggleFloatingElement
        shouldRenderArrow=this.shouldRenderArrow
        arrowModifier=this.arrowModifier
      )
      as |element|
    }}
      {{yield this.referenceModifier element this.toggleFloatingElement}}
    {{/let}}
  </template>
}

interface FloatingElementSignature {
  Args: {
    modifier: ModifierLike<{ Element: HTMLDivElement }>;
    visible: boolean;
    toggle: () => void;
    shouldRenderArrow: boolean;
    arrowModifier: ModifierLike<{
      Element: HTMLDivElement;
    }>;
  };
  Blocks: {
    default: [() => void];
  };
  Element: HTMLDivElement;
}

const FloatingElement: TOC<FloatingElementSignature> = <template>
  {{#if @visible}}
    <div
      class='floating__content'
      ...attributes
      {{@modifier}}
      data-test-content
    >
      {{yield @toggle}}
      {{#if @shouldRenderArrow}}
        <div class='floating__arrow' {{@arrowModifier}} data-test-arrow></div>
      {{/if}}
    </div>
  {{/if}}
</template>;
