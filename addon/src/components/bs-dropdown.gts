import Component from '@glimmer/component';
import { tracked } from 'tracked-built-ins';
import type { ComponentLike, WithBoundArgs } from '@glint/template';
import { hash } from '@ember/helper';
import type { TOC } from '@ember/component/template-only';
import { modifier } from 'ember-modifier';
import { Dropdown } from 'bootstrap.native';

interface BsDropdownToggleSignature {
  Args: {
    expanded: boolean;
    register: (button: HTMLButtonElement) => void;
    unregister: () => void;
  };
  Element: HTMLButtonElement;
  Blocks: {
    default: [];
  };
}

class BsDropdownToggleComponent extends Component<BsDropdownToggleSignature> {
  buttonModifier = modifier((button: HTMLButtonElement) => {
    this.args.register(button);

    return () => {
      this.args.unregister();
    };
  });

  <template>
    <button
      class='dropdown-toggle'
      type='button'
      data-bs-toggle='dropdown'
      aria-expanded={{@expanded}}
      ...attributes
      {{this.buttonModifier}}
    >{{yield}}</button>
  </template>
}

interface BsDropdownMenuSignature {
  Element: HTMLUListElement;
  Blocks: {
    default: [{ item: ComponentLike<BsDropdownMenuItemSignature> }];
  };
}

const BsDropdownMenuComponent: TOC<BsDropdownMenuSignature> = <template>
  <ul class='dropdown-menu' ...attributes>{{yield
      (hash item=(component BsDropdownMenuItemComponent))
    }}</ul>
</template>;

interface BsDropdownMenuItemSignature {
  Element: HTMLAnchorElement;
  Blocks: {
    default: [];
  };
}

const BsDropdownMenuItemComponent: TOC<BsDropdownMenuItemSignature> = <template>
  <li><a class='dropdown-item' href='#' ...attributes>{{yield}}</a></li>
</template>;

export interface BsDropdownSignature {
  Blocks: {
    default: [
      {
        toggle: WithBoundArgs<
          typeof BsDropdownToggleComponent,
          'expanded' | 'register' | 'unregister'
        >;
        menu: ComponentLike<BsDropdownMenuSignature>;
      },
    ];
  };
  Element: HTMLDivElement;
}

export default class BsDropdownComponent extends Component<BsDropdownSignature> {
  @tracked expanded = false;

  @tracked bsnDropdown?: Dropdown;

  becameVisible = () => {
    this.expanded = true;
  };

  becameHidden = () => {
    this.expanded = false;
  };

  registerButton = (button: HTMLButtonElement) => {
    this.bsnDropdown = new Dropdown(button);

    this.bsnDropdown.parentElement.addEventListener(
      'shown.bs.dropdown',
      this.becameVisible,
    );
    this.bsnDropdown.parentElement.addEventListener(
      'hidden.bs.dropdown',
      this.becameHidden,
    );
  };

  unregisterButton = () => {
    this.bsnDropdown?.parentElement.removeEventListener(
      'shown.bs.dropdown',
      this.becameVisible,
    );
    this.bsnDropdown?.parentElement.removeEventListener(
      'hidden.bs.dropdown',
      this.becameHidden,
    );

    this.bsnDropdown?.dispose();
    this.bsnDropdown = undefined;
  };

  <template>
    <div class='dropdown' ...attributes>{{yield
        (hash
          toggle=(component
            BsDropdownToggleComponent
            expanded=this.expanded
            register=this.registerButton
            unregister=this.unregisterButton
          )
          menu=(component BsDropdownMenuComponent)
        )
      }}</div>
  </template>
}
