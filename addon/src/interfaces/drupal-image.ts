import type DrupalFile from './drupal-file.ts';

export default interface DrupalImage extends DrupalFile {
  width: string;
  height: string;
  alt?: string;
  title?: string;
}
