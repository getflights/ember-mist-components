import type DrupalImage from './drupal-image';
import DrupalModel from '../models/drupal-model.ts';

export enum Permission {
  NDC = 'NDC',
  PRIVATE_FARES = 'Private Fares',
  TICKET_BOOKING = 'Ticket Booking',
  DEVELOPER = 'Developer',
  REQUIRE_TKT_APPROVAL = 'Require Ticketing Approval',
}

export default interface UserModelInterface extends DrupalModel {
  name: string;
  status: boolean;
  firstName: string;
  lastName: string;
  mail: string;
  userPicture?: DrupalImage;
  unseenNotifications: number;
  notificationsViewed?: Date;

  setNotificationsViewed(): void;
  setAllNotificationsRead(): void;
  hasPermission(permission: string): boolean;
}
