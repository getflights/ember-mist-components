import type { Operator } from '../query/Condition.ts';

export default interface BelongsToFilterInterface {
  field: string;
  operator: Operator | undefined;
  value: string | number | boolean;
}
