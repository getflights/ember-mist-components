import Model, { type SyncHasMany } from '@ember-data/model';
import type MetaModelModelInterface from './meta-model.ts';
import type FieldModelInterface from './field.ts';
import type ConditionModelInterface from './condition.ts';
import type OrderModelInterface from './order.ts';

export default interface ListViewModelInterface extends Model {
  name: string;
  grouping: string;
  rows?: number;

  model: MetaModelModelInterface;
  columns: SyncHasMany<FieldModelInterface>;

  conditionLogic?: string;
  conditions: SyncHasMany<ConditionModelInterface>;

  sortOrders: SyncHasMany<OrderModelInterface>;
}
