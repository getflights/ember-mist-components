import Model from '@ember-data/model';

export default interface FieldModelInterface extends Model {
  fieldType: string;
  name: string;
  // apiName: string;
  cardinality: number;
  label: string;
  constraints: string[];
  defaultValue?: any;
  selectOptions?: {
    [key: string]: string;
  };
}
