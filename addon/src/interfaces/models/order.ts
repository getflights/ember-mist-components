import Model from '@ember-data/model';
import type FieldModelInterface from './field.ts';

export default interface OrderModelInterface extends Model {
  direction: 'ASC' | 'DESC';
  // name: string;
  // status: boolean;

  field: FieldModelInterface;
}
