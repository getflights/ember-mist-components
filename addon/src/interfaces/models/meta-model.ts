import Model from '@ember-data/model';

export default interface MetaModelModelInterface extends Model {
  entity: string;
  bundle: string;
  label?: string;
}
