import { camelize, dasherize } from '@ember/string';

/**
 * Convert a (dash-er-ized) object to a camelCased object
 */
export const camelizeObject = (
  object: object,
  options?: {
    recursive?: boolean;
    replaceNullWithUndefined?: boolean;
  },
) => {
  options = Object.assign(
    { recursive: false, replaceNullWithUndefined: false },
    options,
  );

  return Object.entries(object).reduce(
    (result: any, [key, value]) => {
      // Don't camelize array keys
      const camelizedKey = Array.isArray(object) ? key : camelize(key);

      if (options?.recursive) {
        if (value !== null && typeof value === 'object') {
          // If the value is also an object, also camelize it
          value = camelizeObject(value, options);
        }
      }

      if (value === null && options?.replaceNullWithUndefined) {
        value = undefined;
      }

      result[camelizedKey] = value;

      return result;
    },
    // Keep arrays an array
    Array.isArray(object) ? [] : {},
  );
};

/**
 * Convert a (camelCased) object to a dash-er-ized object
 */
export const dasherizeObject = (
  object: object,
  options?: { recursive?: boolean },
) => {
  options = Object.assign({ recursive: false }, options);

  return Object.entries(object).reduce(
    (result: any, [key, value]) => {
      // Don't dasherize array keys
      const dasherizedKey = Array.isArray(object) ? key : dasherize(key);

      if (options?.recursive) {
        if (value !== null && typeof value === 'object') {
          // If the value is also an object, also camelize it
          value = dasherizeObject(value, options);
        }
      }

      result[dasherizedKey] = value;

      return result;
    },
    // Keep arrays an array
    Array.isArray(object) ? [] : {},
  );
};
