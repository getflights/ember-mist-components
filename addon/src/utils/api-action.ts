import { getOwner } from '@ember/application';
import HttpService from '../services/http.ts';
import Model from '@ember-data/model';

export default function apiAction(path: string) {
  return function runInstanceOp(this: Model, payload?: any): Promise<Response> {
    const http = getOwner(this)!.lookup(`service:http`) as HttpService;

    const url = http.getActionEndpoint(this, path);
    const headers = http.getModelHeaders(this);

    if (payload) {
      payload = JSON.stringify(payload);
    }

    return http.fetch(url, 'POST', payload, undefined, undefined, headers);
  };
}
