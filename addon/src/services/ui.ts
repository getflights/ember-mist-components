import Service from '@ember/service';
import { tracked, TrackedMap } from 'tracked-built-ins';
import type { ComponentLike } from '@glint/template';
import IconButtonMore from '../components/-private/icon/button-more.gts';
import IconArrowDown from '../components/-private/icon/arrow-down.gts';
import IconArrowLeft from '../components/-private/icon/arrow-left.gts';
import IconArrowRight from '../components/-private/icon/arrow-right.gts';
import IconArrowUp from '../components/-private/icon/arrow-up.gts';
import IconChevronDown from '../components/-private/icon/chevron-down.gts';
import IconChevronLeft from '../components/-private/icon/chevron-left.gts';
import IconChevronRight from '../components/-private/icon/chevron-right.gts';
import IconChevronUp from '../components/-private/icon/chevron-up.gts';
import IconClose from '../components/-private/icon/close.gts';
import IconDownload from '../components/-private/icon/download.gts';
import IconDraggable from '../components/-private/icon/draggable.gts';
import IconPaperclip from '../components/-private/icon/paperclip.gts';
import IconPlus from '../components/-private/icon/plus.gts';
import IconRefresh from '../components/-private/icon/refresh.gts';
import IconRemove from '../components/-private/icon/remove.gts';
import IconSearch from '../components/-private/icon/search.gts';
import { getOwner } from '@ember/application';

export default class UiService extends Service {
  constructor(...args: any[]) {
    super(...args);

    this.addIconComponent('arrow-down', IconArrowDown);
    this.addIconComponent('arrow-left', IconArrowLeft);
    this.addIconComponent('arrow-right', IconArrowRight);
    this.addIconComponent('arrow-up', IconArrowUp);
    this.addIconComponent('button-more', IconButtonMore);
    this.addIconComponent('chevron-down', IconChevronDown);
    this.addIconComponent('chevron-left', IconChevronLeft);
    this.addIconComponent('chevron-right', IconChevronRight);
    this.addIconComponent('chevron-up', IconChevronUp);
    this.addIconComponent('close', IconClose);
    this.addIconComponent('download', IconDownload);
    this.addIconComponent('draggable', IconDraggable);
    this.addIconComponent('paperclip', IconPaperclip);
    this.addIconComponent('plus', IconPlus);
    this.addIconComponent('refresh', IconRefresh);
    this.addIconComponent('remove', IconRemove);
    this.addIconComponent('search', IconSearch);
  }

  /**
   * Loading
   */
  @tracked loadingComponent?: ComponentLike<{ Element: HTMLElement }>;

  /**
   * Icons
   */
  private iconComponents = new TrackedMap<
    string,
    ComponentLike<{ Element: HTMLElement }>
  >();

  addIconComponent = (
    name: string,
    iconComponent: ComponentLike<{ Element: HTMLElement }>,
  ) => {
    // TODO REMOVE Backwards compatibility with <IconName />
    getOwner(this)?.register(`component:icon-${name}`, iconComponent);
    this.iconComponents.set(name, iconComponent);
  };

  iconFor = (name: string) => {
    return this.iconComponents.get(name)!;
  };
}
