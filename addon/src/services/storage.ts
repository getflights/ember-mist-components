import Service from '@ember/service';
import { use } from 'ember-resources';
import type { Reactive } from 'ember-resources';
import LocalStorageResource, {
  type TrackedLocalStorageItem,
} from '../resources/local-storage.ts';

export const STORAGE_PREFIX = 'es';

export default class StorageService extends Service {
  private _prefix(key: string) {
    return `${STORAGE_PREFIX}__${key}`;
  }

  private trackedKeys = new Map<string, Reactive<TrackedLocalStorageItem>>();

  public track(key: string) {
    key = this._prefix(key);

    if (this.trackedKeys.has(key)) {
      return this.trackedKeys.get(key)!;
    }

    const resource = use(this, LocalStorageResource(key));
    this.trackedKeys.set(key, resource);
    return resource;
  }

  /**
   * Retrieves a value from localStorage (untracked)
   * @param key The key to retrieve
   * @returns The value (JSON parsed) or undefined
   */
  public retrieve(key: string) {
    const value = localStorage.getItem(this._prefix(key));

    if (value) {
      return JSON.parse(value);
    }

    return;
  }

  public persist(key: string, value: any) {
    localStorage.setItem(this._prefix(key), JSON.stringify(value));

    if (this.trackedKeys.has(key)) {
      this.trackedKeys.get(key)!.current.set(value);
    }
  }

  public clear() {
    const regexp = new RegExp(`^(${STORAGE_PREFIX}__)`);

    const keysToDelete = [];

    // Clear all prefixed items
    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i);

      if (!key) {
        continue;
      }

      // don't nuke *everything* in localStorage... just keys that match our pattern
      if (key.match(regexp)) {
        keysToDelete.push(key);
      }
    }

    keysToDelete.forEach((key: string) => {
      localStorage.removeItem(key);
      if (this.trackedKeys.has(key)) {
        this.trackedKeys.get(key)!.current.clear();
      }
    });
  }

  public remove(key: string) {
    localStorage.removeItem(this._prefix(key));

    if (this.trackedKeys.has(key)) {
      this.trackedKeys.get(key)!.current.clear();
    }
  }
}
