import { action } from '@ember/object';
import Service from '@ember/service';
import type ModelRegistry from 'ember-data/types/registries/model';

interface TokenInfo {
  id: string;
  token: string;
}

type ModelName = keyof ModelRegistry;

export default class ModelAuthStoreService extends Service {
  private modelTokenMap = new Map<ModelName, TokenInfo>();

  @action
  setModelToken(modelName: ModelName, id: string, token: string) {
    this.modelTokenMap.set(modelName, { id, token });
  }

  @action
  hasModelToken(modelName: ModelName) {
    return this.modelTokenMap.has(modelName);
  }

  @action
  getModelToken(modelName: ModelName) {
    return this.modelTokenMap.get(modelName);
  }

  @action
  clearModelToken(modelName: ModelName) {
    return this.modelTokenMap.delete(modelName);
  }

  @action
  authHeadersFor(modelName: ModelName) {
    const tokenInfo = this.modelTokenMap.get(modelName);

    if (tokenInfo) {
      return {
        'X-Mist-Auth-Id': tokenInfo.id,
        'X-Mist-Auth-Token': tokenInfo.token,
      };
    }

    return undefined;
  }
}
