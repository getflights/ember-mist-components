import Service from '@ember/service';
import type Store from '@ember-data/store';
import Model from '@ember-data/model';
import { service } from '@ember/service';
import type ModelInformationService from './model-information.ts';
import type ModelRegistry from 'ember-data/types/registries/model';

interface RouterService {
  transitionTo(route: string, id?: string): void;
}

export default class EntityRouterService extends Service {
  @service declare store: Store;
  @service declare router: RouterService;
  @service declare modelInformation: ModelInformationService;

  /**
   * @param model Transition to the view route for the provided model
   */
  transitionToView(model: Model) {
    this.transitionToModelRoute(model, 'view');
  }

  /**
   * @param model Transition to the edit route for the provided model
   */
  transitionToEdit(model: Model) {
    this.transitionToModelRoute(model, 'edit');
  }

  /**
   * @param model Transition to the delete route for the provided model
   */
  transitionToDelete(model: Model) {
    this.transitionToModelRoute(model, 'delete');
  }

  /**
   * @param modelName The modelname to transition to the create route
   */
  transitionToCreate(modelName: keyof ModelRegistry) {
    const modelInstance = this.store.createRecord(modelName);
    this.transitionToModelRoute(modelInstance, 'new');
    this.store.unloadRecord(modelInstance);
  }

  /**
   * @param modelName The modelname to transition to the index route
   */
  transitionToList(modelName: keyof ModelRegistry) {
    const modelInstance = this.store.createRecord(modelName);
    this.transitionToModelRoute(modelInstance, 'index');
    this.store.unloadRecord(modelInstance);
  }

  /**
   * Transition to a modelroute
   * @param model The model to get the modelroute from
   * @param route The route within the model route to navigate to
   */
  transitionToModelRoute(model: Model, route?: string | undefined) {
    const baseRoute =
      'baseRoute' in model && model.baseRoute
        ? (model.baseRoute as string)
        : this.modelInformation.getModelName(model)!;
    let newRoute = baseRoute;
    if (route) {
      newRoute += `.${route}`;
    }

    if (model.id) {
      this.router.transitionTo(newRoute, model.id);
    } else {
      this.router.transitionTo(newRoute);
    }
  }
}
