import ValidationService from '@getflights/ember-attribute-validations/services/validation';
import type { ValidationFunction } from '@getflights/ember-attribute-validations/decorators/validation';
import { maxLength } from '@getflights/ember-attribute-validations/validations/max-length';
import { number } from '@getflights/ember-attribute-validations/validations/number';
import type { AttributeMeta } from '@getflights/ember-attribute-validations/services/validation';
import { address } from '../validations/address.ts';
import Model from '@ember-data/model';

/**
 * Here we extend the Validator Service, and add Default validations for different types
 * This is specific to the mist platform logic, and should be manually chosen per application
 * By explicitly exporting this version of the ValidatorService
 * Compared to the one in the ember-attribute-validations addon, which is exported by default
 */
export default class MistValidationService extends ValidationService {
  validationsForAttribute(_model: Model, attribute: AttributeMeta) {
    const extraValidations: ValidationFunction[] = [];

    // const fieldOptions =
    //   Reflect.getMetadata(
    //     METADATA_KEY,
    //     model, // target
    //     attribute.name, // propertyKey
    //   ) ?? {};

    // const options = Object.assign({}, attribute.options, fieldOptions);

    switch (attribute.type) {
      case 'string':
        extraValidations.push(maxLength(255));
        break;
      case 'number':
        extraValidations.push(number());
        break;
      case 'address':
        extraValidations.push(address());
        break;
    }

    return extraValidations;
  }
}
