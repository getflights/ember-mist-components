import Service from '@ember/service';
import { service } from '@ember/service';
import { getOwner } from '@ember/application';
import qs from 'qs';
import type SessionService from 'ember-simple-auth/services/session';
import JSONAPIAdapter from '@ember-data/adapter/json-api';
import { cached } from '@glimmer/tracking';
import type ModelInformationService from './model-information.ts';
import type Store from '@ember-data/store';
import Model from '@ember-data/model';
import ModelAuthStoreService from './model-auth-store.ts';
import CampaignSessionService from './campaign-session.ts';

export default class HttpService extends Service {
  @service declare campaignSession: CampaignSessionService;
  @service declare session: SessionService;
  @service declare modelInformation: ModelInformationService;
  @service declare store: Store;
  @service declare modelAuthStore: ModelAuthStoreService;

  async fetch(
    path: string,
    method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'OPTIONS' | 'DELETE' = 'GET',
    body?:
      | string
      | Blob
      | ArrayBufferView
      | ArrayBuffer
      | FormData
      | URLSearchParams
      | ReadableStream<Uint8Array>
      | null
      | undefined,
    queryParams?: any,
    abortController?: AbortController,
    headers?: any,
  ): Promise<Response> {
    const requestHeaders = { ...this.headers, ...headers };
    const options: RequestInit = {
      headers: requestHeaders,
      method: method,
      body: body,
      signal: abortController?.signal,
    };

    const endpoint = queryParams
      ? `${path}?${qs.stringify(queryParams)}`
      : path;
    return new Promise((resolve, reject) => {
      return fetch(endpoint, options)
        .then((response: Response) => {
          if (response.ok) {
            resolve(response);
          } else {
            this._handleErrorResponse(response, options);
            reject(response);
          }
        })
        .catch((reason: Response) => {
          this._handleErrorResponse(reason, options);
          reject(reason);
        });
    });
  }

  _handleErrorResponse(response: Response, request: RequestInit) {
    const applicationAdapter = getOwner(this).lookup(
      'adapter:application',
    ) as JSONAPIAdapter;

    if (applicationAdapter && 'handleResponse' in applicationAdapter) {
      applicationAdapter.handleResponse(
        response.status,
        response.headers?.values ?? {},
        response.text,
        request,
      );
    }
  }

  /**
   * Returns an endpoint for a model action
   */
  getActionEndpoint(model: Model, action: string): string {
    const modelName = this.modelInformation.getModelName(model);
    // @ts-ignore
    const adapter = this.store.adapterFor(modelName);
    // @ts-ignore
    const baseURL = adapter.buildURL(
      modelName,
      // @ts-ignore
      model.id,
      // @ts-ignore
      model._createSnapshot(),
    );
    let url = baseURL;

    if (action) {
      if (baseURL.charAt(baseURL.length - 1) === '/') {
        url = `${baseURL}${action}`;
      } else {
        url = `${baseURL}/${action}`;
      }
    }

    return url;
  }

  /**
   * Returns an endpoint for a model action
   */
  getModelHeaders(model: Model) {
    // Option A: get the adapter, use those headers
    // Option B: write the headers yourself, but this would result in the same code as mentioned in the adapter

    const modelName = this.modelInformation.getModelName(model);
    // @ts-ignore
    const adapter = this.store.adapterFor(modelName) as JSONAPIAdapter;
    return adapter.headers;
  }

  /**
   * We set the host from the config
   */
  @cached
  get host() {
    // @ts-ignore
    const config = getOwner(this).resolveRegistration('config:environment');
    return config.apiHost;
  }

  /**
   * Get the endpoint from the config
   */
  @cached
  get endpoint() {
    // @ts-ignore
    const config = getOwner(this).resolveRegistration('config:environment');
    return config.apiEndpoint;
  }

  /**
   * We set the authorization header from the session service
   */
  get headers() {
    const headers: any = {};

    if (this.session.isAuthenticated && this.session.data) {
      headers['Authorization'] =
        `Bearer ${this.session.data.authenticated.access_token}`;
    } else if (this.campaignSession.data?.access_token) {
      headers['Authorization'] =
        `Bearer ${this.campaignSession.data.access_token}`;
    }

    return headers;
  }
}
