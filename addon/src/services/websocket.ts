import type ApplicationInstance from '@ember/application/instance';
import BaseWebsocketService, { BaseWebsocketEvent } from './base-websocket.ts';
import { service } from '@ember/service';
import type SessionService from 'ember-simple-auth/services/session';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export enum AuthMessageType {
  AUTHENTICATED = 'authenticated',
  UNAUTHENTICATED = 'unauthenticated',
}
/**
 * Status of the main websocket service
 */
export enum Status {
  OFFLINE = 'OFFLINE',
  CONNECTING = 'CONNECTING',
  ONLINE = 'ONLINE',
  AUTHENTICATED = 'AUTHENTICATED',
}

/**
 * Type of event emitted by main websocket service
 */
export enum WebsocketEvent {
  OPEN,
  CLOSE,
  AUTHENTICATED,
  UNAUTHENTICATED,
}

type MessageCallback = (message: any) => void;
type EventCallback = () => any;

/**
 * Main websocket, responsible for sending most messages.
 */
export default class WebsocketService extends BaseWebsocketService {
  @service declare session: SessionService;

  @tracked status = Status.OFFLINE;

  suspendConnectionWhenIdle = false;
  autoReconnect = true;

  constructor(owner: ApplicationInstance) {
    const config = owner.factoryFor('config:environment')!.class as any;

    super(owner, { url: config.websocketHost });

    // Automatic websocket suspension
    if (config['ember-mist-components']?.suspendWebsocketWhenIdle) {
      // Do suspend initially
      this.suspendConnectionWhenIdle = true;
      // Do not suspend after authenticating
      this.addEventSubscription(WebsocketEvent.AUTHENTICATED, () => {
        this.suspendConnectionWhenIdle = false;
      });
      // Do suspend after unauthenticating
      this.addEventSubscription(WebsocketEvent.UNAUTHENTICATED, () => {
        this.suspendConnectionWhenIdle = true;
      });
    }

    // 1. We need to authenticate over the websocket, whenever the socket has opened.
    this.addWebsocketSubscription(BaseWebsocketEvent.OPEN, this.wsOpened);
    this.addWebsocketSubscription(
      BaseWebsocketEvent.MESSAGE,
      this.wsMessageReceived,
    );
    this.addWebsocketSubscription(BaseWebsocketEvent.CLOSE, this.wsClosed);
    this.addMessageSubscription(
      AuthMessageType.UNAUTHENTICATED,
      this.wsUnauthenticated,
    );
  }

  //#region MESSAGE SUBSCRIPTIONS
  private messageSubscriptions = new Map<string, Set<MessageCallback>>();

  @action
  addMessageSubscription(type: string, callback: MessageCallback) {
    if (!this.messageSubscriptions.has(type)) {
      this.messageSubscriptions.set(type, new Set());
    }

    this.messageSubscriptions.get(type)!.add(callback);
  }

  @action
  removeMessageSubscription(type: string, callback: MessageCallback) {
    if (this.messageSubscriptions.has(type)) {
      this.messageSubscriptions.get(type)!.delete(callback);
    }
  }

  private emitMessage(type: string, message: any) {
    this.messageSubscriptions.get(type)?.forEach((subscription) => {
      subscription(message);
    });
  }
  //#endregion

  //#region EVENT SUBSCRIPTIONS
  private eventSubscriptions = new Map<WebsocketEvent, Set<EventCallback>>();

  @action
  addEventSubscription(event: WebsocketEvent, callback: EventCallback) {
    if (!this.eventSubscriptions.has(event)) {
      this.eventSubscriptions.set(event, new Set());
    }

    this.eventSubscriptions.get(event)!.add(callback);
  }

  @action
  removeEventSubscription(event: WebsocketEvent, callback: EventCallback) {
    if (this.eventSubscriptions.has(event)) {
      this.eventSubscriptions.get(event)!.delete(callback);
    }
  }

  private emitEvent(event: WebsocketEvent) {
    this.eventSubscriptions.get(event)?.forEach((subscription) => {
      subscription();
    });
  }
  //#endregion

  @action
  private wsMessageReceived(ev: MessageEvent) {
    const message = JSON.parse(ev.data);

    if (!message.meta || !message.meta.type) {
      return;
    }

    this.emitMessage(message.meta.type, message);
  }

  @action
  private wsOpened() {
    this.status = Status.ONLINE;
    this.emitEvent(WebsocketEvent.OPEN);
    this.authenticate();
  }

  @action
  private wsClosed() {
    this.emitEvent(WebsocketEvent.CLOSE);
    this.status = Status.OFFLINE;
  }

  //#region AUTHENTICATION
  @action
  sessionAuthenticated() {
    if (this.status !== Status.AUTHENTICATED) {
      this.authenticate();
    }
  }

  @action
  sessionInvalidated() {
    if (this.status === Status.AUTHENTICATED) {
      this.unauthenticate();
    }
  }

  @action
  wsUnauthenticated() {
    if (this.session.isAuthenticated) {
      this.session.invalidate();
    }

    this.status = Status.ONLINE;
    this.emitEvent(WebsocketEvent.UNAUTHENTICATED);
  }

  @action
  authenticate() {
    if (!this.session.isAuthenticated) {
      return;
    }

    const message = {
      meta: {
        type: 'authenticate',
      },
      data: {
        type: 'authentication-message',
        attributes: {
          type: 'bearer',
          authorization: this.session.data!.authenticated.access_token,
        },
      },
    };

    // Message listener
    const onAuthenticated = (_message: any) => {
      this.status = Status.AUTHENTICATED;
      this.emitEvent(WebsocketEvent.AUTHENTICATED);
      this.removeMessageSubscription(
        AuthMessageType.AUTHENTICATED,
        onAuthenticated,
      );
    };

    this.addMessageSubscription(AuthMessageType.AUTHENTICATED, onAuthenticated);

    // Send message
    this.sendMessage(message);
  }

  @action
  unauthenticate() {
    const message = {
      meta: {
        type: 'unauthenticate',
      },
    };

    // Send message
    this.sendMessage(message);
  }
  //#endregion
}
