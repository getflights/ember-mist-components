import Service from '@ember/service';
import Model from '@ember-data/model';
import type ModelInformationService from './model-information';
import { service } from '@ember/service';
import { isBlank } from '@ember/utils';
import type StorageService from '../services/storage';
import type Store from '@ember-data/store';
import type ModelRegistry from 'ember-data/types/registries/model';
import { cached } from '@glimmer/tracking';
import { getOwner } from '@ember/application';

export interface RecentlyViewedRecord {
  type: keyof ModelRegistry;
  name: string;
  id: string;
  externalReference?: string;
}

const RECENTLY_VIEWED_LENGTH = 10;

export default class RecentlyViewedService extends Service {
  @service declare storage: StorageService;
  @service declare modelInformation: ModelInformationService;
  @service declare store: Store;

  constructor(args: any) {
    super(args);

    this.recordsStorageItem = this.storage.track(
      'recentlyViewedRecords',
    ).current;
  }

  /**
   * Remove a record from recently viewed
   * @param type The type of model you want to remove
   * @param id THe ID of the model you want to remove from recently viewed
   */
  removeRecentlyViewed = (type: string, id: string) => {
    const records = this.records.filter((record) => {
      return record.id !== id || record.type !== type;
    });

    this.recordsStorageItem.set(records);
  };

  /**
   * Add a model to the recently viewed records
   * @param model The model you want to add
   */
  addRecentlyViewed = (model: Model) => {
    if (!isBlank(model)) {
      const newRecentlyViewedRecord = {
        type: this.modelInformation.getModelName(model),
        // @ts-ignore
        name: model.name,
        id: model.id,
        // @ts-ignore
        externalReference: model.externalReference,
      };

      const newRecords = [
        newRecentlyViewedRecord,
        ...this.records
          .slice(0, RECENTLY_VIEWED_LENGTH - 1)
          .filter((record) => {
            return (
              record.id !== newRecentlyViewedRecord.id ||
              record.type !== newRecentlyViewedRecord.type
            );
          }),
      ];

      this.recordsStorageItem.set(newRecords);
    }
  };

  private recordsStorageItem;

  /**
   * Returns the Recently viewed records currently in local storage, if nothing is found an empty array is returned
   */
  @cached
  get records(): RecentlyViewedRecord[] {
    const records = (this.recordsStorageItem.value ??
      []) as RecentlyViewedRecord[];

    // Only return records that have a view route
    return records.filter((record) => {
      return getOwner(this)!.lookup(`route:${record.type}.view`);
    });
  }
}
