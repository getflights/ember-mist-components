import Service, { service } from '@ember/service';
import StorageService from './storage.ts';
import type ModelRegistry from 'ember-data/types/registries/model';
import type Store from '@ember-data/store';
import type ListViewInterface from '../interfaces/list-view.ts';
import type { ModelClassWithListViews } from '../interfaces/list-view.ts';
import type ListViewModelInterface from '../interfaces/models/list-view.ts';
import { assert } from '@ember/debug';
import Query from '../query/Query.ts';
import { Direction } from '../query/Order.ts';
import Condition, { Operator } from '../query/Condition.ts';

const SELECTIONS_STORAGE_KEY = 'listViewSelections';

export default class ListViewService extends Service {
  @service declare storage: StorageService;
  @service declare store: Store;

  //#region Model List Views
  getModelListView(
    modelName: keyof ModelRegistry,
    key = 'default',
  ): ListViewInterface {
    const listViews = this.getModelListViews(modelName);
    const listView = listViews?.[key];

    assert(
      `You tried to access the model list view "${key}" on "${modelName}" but it does not exist. Please define it using the @modelListView decorator.`,
      listView,
    );

    return {
      model: modelName,
      ...listView,
    };
  }

  private getModelListViews(modelName: keyof ModelRegistry) {
    const modelClass = this.store.modelFor(modelName);

    const hasListViews = (
      modelClass: any,
    ): modelClass is ModelClassWithListViews => {
      return (
        'settings' in modelClass &&
        modelClass.settings &&
        'listViews' in modelClass.settings &&
        typeof modelClass.settings.listViews === 'object'
      );
    };

    if (hasListViews(modelClass)) {
      return modelClass?.settings?.listViews;
    }
  }
  //#endregion

  //#region API List Views
  async getListViewById(id: string) {
    const localRecord = this.store.peekRecord(
      'list-view',
      id,
    ) as ListViewModelInterface | null;

    if (localRecord !== null) {
      return this.listViewModelToModelListView(localRecord);
    }

    const query = new Query('list-view');
    query.addInclude('sort_orders');
    query.addInclude('sort_orders.field');
    query.addCondition(new Condition('id', Operator.EQUALS, id));

    const remoteRecord = await query.fetchRecord(this.store);

    if (remoteRecord) {
      return this.listViewModelToModelListView(remoteRecord);
    }
  }

  private listViewModelToModelListView(
    listViewModel: ListViewModelInterface,
  ): ListViewInterface {
    const columns = listViewModel
      .hasMany('columns')
      .ids()
      .map((fieldId: string) => {
        const fieldArray = fieldId.toString().split('.');
        fieldArray.shift();
        return fieldArray.join('.');
      });

    const firstSortOrder = listViewModel.sortOrders.firstObject;
    const sortOrder: ListViewInterface['sortOrder'] | undefined = firstSortOrder
      ? {
          field: firstSortOrder.field.name,
          dir:
            firstSortOrder.direction.toUpperCase() == 'DESC'
              ? Direction.DESC
              : Direction.ASC,
        }
      : undefined;

    const modelListView: ListViewInterface = {
      model: listViewModel.belongsTo('model').id() as keyof ModelRegistry,
      rows: listViewModel.rows,
      columns,
      sortOrder,
    };

    const sortOrderModel = listViewModel.sortOrders.firstObject;

    if (sortOrderModel) {
      modelListView.sortOrder = {
        field: sortOrderModel.field.name,
        dir: sortOrderModel.direction,
      };
    }

    return modelListView;
  }

  setActiveListViewIdForGrouping(grouping: string, id: string | undefined) {
    const newSelections = {
      ...this.listViewSelections,
    };

    if (id === undefined) {
      delete newSelections[grouping];
    } else {
      newSelections[grouping] = id;
    }

    this.storage.persist(SELECTIONS_STORAGE_KEY, newSelections);
  }

  getActiveListViewIdForGrouping(grouping: string) {
    return this.listViewSelections?.[grouping];
  }

  get listViewSelections():
    | {
        [grouping: string]: string;
      }
    | undefined {
    return this.storage.retrieve(SELECTIONS_STORAGE_KEY);
  }
  //#endregion
}
