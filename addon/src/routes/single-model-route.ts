import ResetModelRoute from './reset-model-route.ts';
import RecentlyViewedService from '../services/recently-viewed.ts';
import type ModelInformationService from '../services/model-information.ts';
import { service } from '@ember/service';
import { isBlank } from '@ember/utils';
import { action } from '@ember/object';
import type Transition from '@ember/routing/transition';
import type SessionService from 'ember-simple-auth/services/session';
import type Store from '@ember-data/store';
import ChangeTrackerModel from '../models/change-tracker-model.ts';
import type ModelRegistry from 'ember-data/types/registries/model';

export interface SingleModelRouteModelParams {
  id: string;
}

export default abstract class SingleModelRoute extends ResetModelRoute {
  @service declare modelInformation: ModelInformationService;
  @service declare recentlyViewed: RecentlyViewedService;
  @service declare session: SessionService;
  @service declare store: Store;

  abstract modelName: keyof ModelRegistry;
  defaultIncludes: string[] = [];
  includeLimits?: Map<string, number>;

  async beforeModel(transition: Transition) {
    this.session.requireAuthentication(transition, 'login');
    return super.beforeModel(transition);
  }

  /**
   * The model hook with functionality for single entities
   * @param params The router params
   */
  async model(params: SingleModelRouteModelParams) {
    const modelDefaultIncludes = this.modelInformation.getDefaultIncludes(
      this.modelName,
    );
    const routeDefaultIncludes = this.defaultIncludes;

    // Lets merge the different includes
    let includes: string[] = [];
    if (!isBlank(modelDefaultIncludes) && modelDefaultIncludes.length > 0) {
      includes = includes.concat(modelDefaultIncludes);
    }

    if (!isBlank(routeDefaultIncludes) && routeDefaultIncludes.length > 0) {
      includes = includes.concat(routeDefaultIncludes);
    }

    // and filter the doubles
    const uniqueIncludes = includes.filter((elem, index, self) => {
      return index == self.indexOf(elem);
    });

    // And now construct the options hash
    const options: any = {
      reload: true, // Make sure all includes are requested from the API.
    };

    if (uniqueIncludes.length > 0) {
      options['include'] = uniqueIncludes.join(',');
    }

    if (this.includeLimits && this.includeLimits.size > 0) {
      if (!options.adapterOptions) {
        options['adapterOptions'] = {};
      }

      options.adapterOptions.includeLimits = {};

      this.includeLimits.forEach((limit, relationshipName) => {
        options.adapterOptions.includeLimits[relationshipName] = limit;
      });
    }

    // @ts-ignore
    return this.store.findRecord(this.modelName, params['id'], options);
  }

  async afterModel(model: ChangeTrackerModel, transition: Transition) {
    await super.afterModel(model, transition);
    model.startTrack();
  }
  /**
   * If an error is triggered on the transition, we remove the Model from the Recently viewed
   */
  @action
  error(error: any, transition: Transition) {
    if (!isBlank(this.modelName)) {
      if (
        transition.to &&
        transition.to.params &&
        Object.prototype.hasOwnProperty.call(transition.to.params, 'id')
      ) {
        // @ts-ignore
        const id = transition.to.params.id;

        if (id) {
          this.recentlyViewed.removeRecentlyViewed(this.modelName, id);
        }
      }
    }

    if (error?.errors?.[0]?.status === '404') {
      this.transitionTo('error');
      return false;
    }

    return true;
  }
}
