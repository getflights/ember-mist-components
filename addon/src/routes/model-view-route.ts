import SingleModelRoute from './single-model-route.ts';
import type RecentlyViewedService from '../services/recently-viewed.ts';
import EntityCacheService from '../services/entity-cache.ts';
import Model from '@ember-data/model';
import { service } from '@ember/service';
import type Transition from '@ember/routing/transition';
import ChangeTrackerModel from '../models/change-tracker-model.ts';

export default abstract class ModelViewRoute extends SingleModelRoute {
  @service declare entityCache: EntityCacheService;
  @service declare recentlyViewed: RecentlyViewedService;

  async afterModel(model: Model, transition: Transition) {
    await super.afterModel(<ChangeTrackerModel>model, transition);

    this.entityCache.clearReturnToModel();
    this.recentlyViewed.addRecentlyViewed(model);
  }
}
