import ResetModelRoute from './reset-model-route.ts';
import type Store from '@ember-data/store';
import EntityCacheService from '../services/entity-cache.ts';
import { service } from '@ember/service';
import type Transition from '@ember/routing/transition';
import type SessionService from 'ember-simple-auth/services/session';
import Model from '@ember-data/model';
import type ModelRegistry from 'ember-data/types/registries/model';

export default abstract class ModelNewRoute extends ResetModelRoute {
  @service declare store: Store;
  @service declare entityCache: EntityCacheService;
  @service declare session: SessionService;

  abstract modelName: keyof ModelRegistry;

  async beforeModel(transition: Transition) {
    this.session.requireAuthentication(transition, 'login');
    return super.beforeModel(transition);
  }

  async model(): Promise<Model> {
    const cachedModel = this.entityCache.cachedModel;

    if (!cachedModel) {
      return this.store.createRecord(this.modelName as keyof ModelRegistry);
    } else {
      this.entityCache.clearCachedModel();
      return cachedModel;
    }
  }
}
