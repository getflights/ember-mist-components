import Route from '@ember/routing/route';
import type Controller from '@ember/controller';
import type Transition from '@ember/routing/transition';

export default class ResetModelRoute extends Route {
  resetController(
    controller: Controller,
    isExiting: boolean,
    transition: Transition,
  ) {
    super.resetController(controller, isExiting, transition);
    // @ts-ignore
    controller.model.rollback();
  }

  activate(transition: Transition) {
    super.activate(transition);
    window.scrollTo(0, 0);
  }
}
