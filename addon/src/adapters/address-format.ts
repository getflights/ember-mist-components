import JSONApiAdapter from '@ember-data/adapter/json-api';
import { service } from '@ember/service';
import StorageService from '../services/storage.ts';
import AddressService from '../services/address.ts';
import RSVP from 'rsvp';
import HttpService from '../services/http.ts';

type FindRecordParams = Parameters<JSONApiAdapter['findRecord']>;

const FORMAT_PREFIX = 'addressFormat';

export default abstract class AddressFormatAdapter extends JSONApiAdapter {
  @service declare address: AddressService;
  @service declare http: HttpService;
  @service declare storage: StorageService;

  /**
    The `findRecord()` method is invoked when the store is asked for a record that
    has not previously been loaded. In response to `findRecord()` being called, you
    should query your persistence layer for a record with the given ID. The `findRecord`
    method should return a promise that will resolve to a JavaScript object that will be
    normalized by the serializer.
  */
  findRecord(
    _store: FindRecordParams[0],
    _type: FindRecordParams[1],
    id: FindRecordParams[2],
    _snapshot: FindRecordParams[3],
  ): RSVP.Promise<unknown> {
    return new RSVP.Promise(async (resolve, reject) => {
      const storageKey = `${FORMAT_PREFIX}${id}`;

      let addressFormat = this.address.shouldCache
        ? await this.storage.retrieve(storageKey)
        : undefined;

      if (!addressFormat) {
        await this.http
          .fetch(`${this.http.endpoint}address/format/${id}`)
          .then(async (response) => {
            addressFormat = await response.json();
          })
          .catch((error: any) => {
            reject(error);
          });

        if (this.address.shouldCache) {
          this.storage.persist(storageKey, addressFormat);
        }
      }

      resolve(addressFormat);
    });
  }
}
