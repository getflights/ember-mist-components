/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable ember/no-mixins */
import { EmbeddedRecordsMixin } from '@ember-data/serializer/rest';
import JSONAPISerializer from '@ember-data/serializer/json-api';
import type Store from '@ember-data/store';
import type MistModel from '../models/mist-model.ts';
import keepOnlyChanged from '../mixins/keep-only-changed.ts';
import type { Snapshot } from '@ember-data/store';
import type { ModelSchema } from 'ember-data';
import { dasherize } from '@ember/string';
import type ModelRegistry from 'ember-data/types/registries/model';
// @ts-ignore
import { recordIdentifierFor } from '@ember-data/store';
import type Model from '@ember-data/model';

const isMistModel = (model: any): model is MistModel => {
  return 'hasDirtyEmbeddedRelationship' in model;
};

const attrsHasSerializeRecords = (attrs?: any) => {
  return (
    attrs &&
    // and it says the relationship should be serialized
    (attrs.serialize === 'records' || attrs.embedded === 'always')
  );
};

/**
 * This is an extension for the JSON API serializer, where relationships marked as embedded will be serialized
 * into the response when creating/updating a record.
 *
 * When doing an update, embedded records will be unloaded from the store after the save.
 * But the response will have the embedded records included in the response.
 *
 * However, when creating a record with embedded relationships, related records will not be unloaded
 * - The problem is that we do not know the instance of the model doing the save, so we cant do the rollback
 * This must be handled by the application
 *  one way you could do this, is mark the relationship as rollback: true,
 *  this will delete unsaved records from the store when doing a transition
 */
export default class JSONAPIEmbeddedSerializer extends JSONAPISerializer.extend(
  EmbeddedRecordsMixin,
  keepOnlyChanged,
) {
  isEmbeddedRecordsMixinCompatible = true;
  attrs: any;

  /**
   * Override the return value for `keepRelationship` from the keepOnlyChanged mixin for all embedded relationships.
   */
  keepRelationship(record: any, key: string) {
    if (attrsHasSerializeRecords(this.attrs?.[key])) {
      return (
        record.get('isNew') ||
        !isMistModel(record) ||
        record.hasDirtyEmbeddedRelationship(key)
      );
    }

    return super.keepRelationship(record, key);
  }

  /**
   * Adding a `lid` to the serialized resource
   * (!) This will only add a `lid` to the parent, not to the embedded relationships. Their serializer also needs this code!
   */
  serialize(snapshot: Snapshot, options: {}) {
    const serialized = super.serialize(snapshot, options);

    // @ts-expect-error types of super.serialize() and snapshot are wrong / outdated (checked 4.6 docs / source code, data exists)
    serialized.data.lid = snapshot.identifier.lid;

    return serialized;
  }

  /**
   * Add `lid` to the result of `normalize` so Ember Data can use it to match the record
   */
  normalize(typeClass: ModelSchema, hash: any) {
    const normalized = super.normalize(typeClass, hash);

    if (hash.lid) {
      // @ts-expect-error super.normalize has no proper return type (checked 4.6 docs / source code, data exists)
      normalized.data.lid = hash.lid;
    }

    return normalized;
  }

  normalizeUpdateRecordResponse(
    store: Store,
    primaryModelClass: any,
    payload: any,
    id: string,
    ...args: [string] // requestType
  ) {
    if (this.attrs) {
      const relationshipsByName = primaryModelClass.relationshipsByName;

      for (const relationshipName in this.attrs) {
        if (attrsHasSerializeRecords(this.attrs[relationshipName])) {
          const newRelationshipValue =
            payload.data.relationships?.[dasherize(relationshipName)];

          if (newRelationshipValue) {
            const relationship = relationshipsByName.get(relationshipName);
            const modelName =
              primaryModelClass.modelName as keyof ModelRegistry;
            const existingRecord = store.peekRecord(modelName, id);

            if (relationship.kind === 'hasMany') {
              const oldRecords: Model[] = existingRecord
                .hasMany(relationshipName)
                .value()
                .slice();
              const newIdentifiers: {
                id: string;
                type: string;
                lid: string;
              }[] = newRelationshipValue.data;

              for (const oldRecord of oldRecords) {
                const { id, lid } = recordIdentifierFor(oldRecord);

                if (
                  newIdentifiers.some(
                    (identifier) =>
                      identifier.id !== id && identifier.lid !== lid,
                  )
                ) {
                  store.unloadRecord(oldRecord);
                }
              }
            }
          }
        }
      }
    }

    return super.normalizeUpdateRecordResponse(
      store,
      primaryModelClass,
      payload,
      id,
      ...args,
    );
  }

  normalizeDeleteRecordResponse(
    store: Store,
    primaryModelClass: any,
    payload: object,
    id: string,
    requestType: string,
  ) {
    if (this.attrs) {
      const relationshipsByName = primaryModelClass.relationshipsByName;

      for (const relationshipName in this.attrs) {
        if (attrsHasSerializeRecords(this.attrs[relationshipName])) {
          const relationship = relationshipsByName.get(relationshipName);
          const modelName = primaryModelClass.modelName as keyof ModelRegistry;
          const existingRecord = store.peekRecord(modelName, id);

          if (relationship.kind === 'hasMany') {
            const existingIds = existingRecord.hasMany(relationshipName).ids();

            for (const id of existingIds) {
              const oldRecord = store.peekRecord(relationship.type, id);
              store.unloadRecord(oldRecord);
            }
          }
        }
      }
    }

    return super.normalizeDeleteRecordResponse(
      store,
      primaryModelClass,
      payload,
      id,
      requestType,
    );
  }
}
