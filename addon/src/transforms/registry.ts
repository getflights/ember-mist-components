import type AddressTransform from './address';
import type DateTransform from './date';
import type DatetimeTransform from './datetime';
import type FileTransform from './file';
import type FilesTransform from './files';
import type ImageTransform from './image';
import type ImagesTransform from './images';
import type JsonTransform from './json';
import type LongtextTransform from './longtext';
import type ObjectTransform from './object';
import type RichtextTransform from './richtext';
import type StringsTransform from './strings';

export default interface MistTransformRegistry {
  address: AddressTransform;
  autonumber: ObjectTransform;
  date: DateTransform;
  datetime: DatetimeTransform;
  file: FileTransform;
  files: FilesTransform;
  image: ImageTransform;
  images: ImagesTransform;
  json: JsonTransform;
  longtext: LongtextTransform;
  object: ObjectTransform;
  richtext: RichtextTransform;
  strings: StringsTransform;
}
