import { setOwner } from '@ember/application';
import type Owner from '@ember/owner';
import type { FieldOf, SomeModel } from '@getflights/ember-field-components';
import type DynamicSelectOptionService from '../services/dynamic-select-options';
import { service } from '@ember/service';
import type ModelInformationService from '../services/model-information';
import type SelectOption from '@getflights/ember-field-components/interfaces/select-option';
import type SelectOptionGroup from '@getflights/ember-field-components/interfaces/select-option-group';
import { cached } from '@glimmer/tracking';
import { trackedFunction } from 'reactiveweb/function';
import Model from '@ember-data/model';
import { isGroup } from '@getflights/ember-field-components/interfaces/select-option-group';
import { resource, resourceFactory } from 'ember-resources';

export class FieldSelectHelper<O extends SomeModel, F extends FieldOf<O>> {
  @service declare dynamicSelectOptions: DynamicSelectOptionService;
  @service declare modelInformation: ModelInformationService;

  constructor(
    owner: Owner,
    model: O,
    field: F,
    initialSelectOptions?: (SelectOption | SelectOptionGroup)[],
  ) {
    setOwner(this, owner);

    this.model = model;
    this.field = field;
    this.initialSelectOptions = initialSelectOptions;
  }

  model: O;
  field: F;
  initialSelectOptions?: (SelectOption | SelectOptionGroup)[];

  @cached
  get modelName() {
    return this.modelInformation.getModelName(this.model as unknown as Model)!;
  }

  private asyncSelectOptionsRequest = trackedFunction(this, async () => {
    if (this.model instanceof Model) {
      return await this.dynamicSelectOptions.getSelectOptions.perform(
        this.modelName as string,
        this.field,
      );
    } else {
      throw new Error('Not an Ember Data Model');
    }
  });

  get asyncSelectOptions() {
    return this.asyncSelectOptionsRequest.value;
  }

  get isLoading() {
    return this.asyncSelectOptionsRequest.isLoading;
  }

  get isError() {
    return this.asyncSelectOptionsRequest.isError;
  }

  get placeholder() {
    return this.modelInformation.getTranslatedSelectNoneLabel(
      this.modelName,
      this.field,
    );
  }

  get selectOptions() {
    if (!this.initialSelectOptions) {
      return undefined;
    }

    const selectOptions: (SelectOption | SelectOptionGroup)[] = [
      ...this.initialSelectOptions,
    ];

    return selectOptions.map((option) => {
      if (!isGroup(option) && !option.label) {
        return {
          ...option,
          label: this.modelInformation.getTranslatedSelectOptionLabel(
            this.modelName,
            this.field,
            option.value,
          ),
        };
      }

      return option;
    });
  }
}

const FieldSelectHelperResource = resourceFactory(
  <O extends SomeModel, F extends FieldOf<O>>(
    optionsFn: () => {
      model: O;
      field: F;
      selectOptions: (SelectOption | SelectOptionGroup)[] | undefined;
    },
  ) => {
    let helper: FieldSelectHelper<O, F> | undefined = undefined;

    return resource(({ owner }) => {
      const { model, field, selectOptions } = optionsFn();

      if (!helper) {
        helper = new FieldSelectHelper(owner, model, field, selectOptions);
      } else {
        helper.model = model;
        helper.field = field;
        helper.initialSelectOptions = selectOptions;
      }

      return helper;
    });
  },
);

export default FieldSelectHelperResource;
