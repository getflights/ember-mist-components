import Ember from 'ember';
import { version, name } from '../package.json';

export const VERSION = version ?? 'unknown';
export const NAME = name;

export function registerLibrary() {
  // @ts-ignore
  Ember.libraries.register(NAME, VERSION);
}
