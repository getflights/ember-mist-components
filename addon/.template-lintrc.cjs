'use strict';

module.exports = {
  extends: 'recommended',
  rules: {
    'no-array-prototype-extensions': false,
  }
};
